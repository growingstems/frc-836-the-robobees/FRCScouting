<?php

/*
 * Copyright 2016 Daniel Logan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
define('INCLUDE_CHECK', true);
include('scouting-header.php');

function checkVersion($client_version, $server_version) {
	$cver = substr(trim($client_version), 0, strrchr(trim($client_version), '.'));
	$sver = substr(trim($server_version), 0, strrchr(trim($server_version), '.'));
	return strcasecmp($cver, $sver) == 0;
}

function genJSON($sql_result, $tablename) {
	$json = '"' . $tablename . '":[';

	$firstrow = true;
	while($row = mysqli_fetch_array($sql_result, 1)) {

		if ($firstrow == false) {
			$json .= ",";
		}
		$firstrow = false;
		$i = 0;
		$json .= "{";
		$firstcell = true;
		foreach($row as $cell) {
			if ($firstcell == false) {
				$json .= ",";
			}
			$firstcell = false;
			// Escaping illegal characters
			$cell = str_replace("\\", "\\\\", $cell);
			$cell = str_replace("\"", "\\\"", $cell);
			$cell = str_replace("/", "\\/", $cell);
			$cell = str_replace("\n", "\\n", $cell);
			$cell = str_replace("\r", "\\r", $cell);
			$cell = str_replace("\t", "\\t", $cell);
			$col_name = mysqli_field_name($sql_result, $i);
			$col_type = mysqli_fetch_field_direct($sql_result, $i);


			$json .= '"' . $col_name . '":' ;

			//echo $col_name . ": " . $col_type->type . "\n";
			if ($col_type->type == 7) { //timestamp
				$json .= strtotime($cell);
			}
			elseif ($col_type->type == 1
					|| $col_type->type == 2
					|| $col_type->type == 3
					|| $col_type->type == 4
					|| $col_type->type == 5
					|| $col_type->type == 8
					|| $col_type->type == 9
					|| $col_type->type == 246 ) { //is numeric
				$json .= $cell;
			} else {
				$json .= '"' . $cell . '"';
			}
			$i++;
		}
		$json .= "}";
	}

	$json .= "]";

	return $json;
}

if ($_POST['type'] == 'passConfirm' && $_POST['password'] === $pass) {
	header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
	header('Content-Type: text/plain; charset=utf-8');
	echo 'success';
}
elseif ($_POST['type'] == 'versioncheck') {
	header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
	header('Content-Type: text/plain; charset=utf-8');
	echo $ver;
}

elseif ($_POST['password'] === $pass) {
	header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');

	$client_version = $_POST['version'];
	$verMatch = checkVersion($client_version, $ver);

	if ($_POST['type'] == 'fullsync' || $_POST['type'] == 'sync') {
		header('Content-Type: application/json; charset=utf-8');
		//syncronization request. if it's a fullsync, then send all data, otherwise use the timestamp (in unix time)
		if ($_POST['type'] == 'fullsync') {
			$suffix = ';';
		} else {
			$suffix = ' WHERE timestamp >= FROM_UNIXTIME(' . $_POST['timestamp'] . ');';
		}

		$json = '{"timestamp" : ' . strtotime(date("Y-m-d H:i:s")) . ',';
		$json .= '"version" : "' . $ver . '",';

		//degrade_lu
		$query = "SELECT * FROM degrade_lu" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "degrade_lu") . ",";
		mysqli_free_result($result);

		//drive_lu
		$query = "SELECT * FROM drive_lu" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "drive_lu") . ",";
		mysqli_free_result($result);

		//event_lu
		$query = "SELECT * FROM event_lu" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "event_lu") . ",";
		mysqli_free_result($result);

		//fact_match_data_2025
		$query = "SELECT * FROM fact_match_data_2025" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "fact_match_data_2025") . ",";
		mysqli_free_result($result);

		//game_info
		$query = "SELECT * FROM game_info" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "game_info") . ",";
		mysqli_free_result($result);

		//notes_options
		$query = "SELECT * FROM notes_options" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "notes_options") . ",";
		mysqli_free_result($result);

		//position_lu
		$query = "SELECT * FROM position_lu" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "position_lu") . ",";
		mysqli_free_result($result);

		//robot_cg_lu
		$query = "SELECT * FROM robot_cg_lu" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "robot_cg_lu") . ",";
		mysqli_free_result($result);

		//robot_lu
		$query = "SELECT * FROM robot_lu" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "robot_lu") . ",";
		mysqli_free_result($result);

		//scout_pit_data_2025
		$query = "SELECT * FROM scout_pit_data_2025" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "scout_pit_data_2025") . ",";
		mysqli_free_result($result);

		//superscout_data_2025
		$query = "SELECT * FROM superscout_data_2025" . $suffix;
		$result = mysqli_query($link,$query);
		$json .= genJSON($result, "superscout_data_2025") . "}";
		mysqli_free_result($result);

		$resp = $json;
	}
	else if ($verMatch == false) {
		header('Content-Type: text/plain; charset=utf-8');
		$resp = 'Version Mismatch, server using version ' . $ver;
	}
	else if ($_POST['type'] == 'match') {
		header('Content-Type: text/plain; charset=utf-8');
		$event_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['event_id']) ? $_POST['event_id'] : '0')));
		$team_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['team_id']) ? $_POST['team_id'] : '0')));
		$match_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['match_id']) ? $_POST['match_id'] : '0')));
		$practice_match = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['practice_match']) ? $_POST['practice_match'] : '0')));
		$position_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['position_id']) ? $_POST['position_id'] : '0')));
		$auto_barge_zone_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_barge_zone_id']) ? $_POST['auto_barge_zone_id'] : '0')));
		$auto_first_face_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_first_face_id']) ? $_POST['auto_first_face_id'] : '0')));
		$auto_ground_face_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_ground_face_id']) ? $_POST['auto_ground_face_id'] : '0')));
		$auto_leave = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_leave']) ? $_POST['auto_leave'] : '0')));
		$auto_player_center_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_center_l1_score']) ? $_POST['auto_player_center_l1_score'] : '0')));
		$auto_player_center_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_center_l2_score']) ? $_POST['auto_player_center_l2_score'] : '0')));
		$auto_player_center_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_center_l3_score']) ? $_POST['auto_player_center_l3_score'] : '0')));
		$auto_player_center_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_center_l4_score']) ? $_POST['auto_player_center_l4_score'] : '0')));
		$auto_player_center_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_center_l1_miss']) ? $_POST['auto_player_center_l1_miss'] : '0')));
		$auto_player_center_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_center_l2_miss']) ? $_POST['auto_player_center_l2_miss'] : '0')));
		$auto_player_center_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_center_l3_miss']) ? $_POST['auto_player_center_l3_miss'] : '0')));
		$auto_player_center_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_center_l4_miss']) ? $_POST['auto_player_center_l4_miss'] : '0')));
		$auto_player_left_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_left_l1_score']) ? $_POST['auto_player_left_l1_score'] : '0')));
		$auto_player_left_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_left_l2_score']) ? $_POST['auto_player_left_l2_score'] : '0')));
		$auto_player_left_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_left_l3_score']) ? $_POST['auto_player_left_l3_score'] : '0')));
		$auto_player_left_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_left_l4_score']) ? $_POST['auto_player_left_l4_score'] : '0')));
		$auto_player_left_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_left_l1_miss']) ? $_POST['auto_player_left_l1_miss'] : '0')));
		$auto_player_left_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_left_l2_miss']) ? $_POST['auto_player_left_l2_miss'] : '0')));
		$auto_player_left_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_left_l3_miss']) ? $_POST['auto_player_left_l3_miss'] : '0')));
		$auto_player_left_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_left_l4_miss']) ? $_POST['auto_player_left_l4_miss'] : '0')));
		$auto_player_right_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_right_l1_score']) ? $_POST['auto_player_right_l1_score'] : '0')));
		$auto_player_right_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_right_l2_score']) ? $_POST['auto_player_right_l2_score'] : '0')));
		$auto_player_right_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_right_l3_score']) ? $_POST['auto_player_right_l3_score'] : '0')));
		$auto_player_right_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_right_l4_score']) ? $_POST['auto_player_right_l4_score'] : '0')));
		$auto_player_right_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_right_l1_miss']) ? $_POST['auto_player_right_l1_miss'] : '0')));
		$auto_player_right_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_right_l2_miss']) ? $_POST['auto_player_right_l2_miss'] : '0')));
		$auto_player_right_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_right_l3_miss']) ? $_POST['auto_player_right_l3_miss'] : '0')));
		$auto_player_right_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_player_right_l4_miss']) ? $_POST['auto_player_right_l4_miss'] : '0')));
		$auto_far_center_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_center_l1_score']) ? $_POST['auto_far_center_l1_score'] : '0')));
		$auto_far_center_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_center_l2_score']) ? $_POST['auto_far_center_l2_score'] : '0')));
		$auto_far_center_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_center_l3_score']) ? $_POST['auto_far_center_l3_score'] : '0')));
		$auto_far_center_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_center_l4_score']) ? $_POST['auto_far_center_l4_score'] : '0')));
		$auto_far_center_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_center_l1_miss']) ? $_POST['auto_far_center_l1_miss'] : '0')));
		$auto_far_center_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_center_l2_miss']) ? $_POST['auto_far_center_l2_miss'] : '0')));
		$auto_far_center_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_center_l3_miss']) ? $_POST['auto_far_center_l3_miss'] : '0')));
		$auto_far_center_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_center_l4_miss']) ? $_POST['auto_far_center_l4_miss'] : '0')));
		$auto_far_left_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_left_l1_score']) ? $_POST['auto_far_left_l1_score'] : '0')));
		$auto_far_left_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_left_l2_score']) ? $_POST['auto_far_left_l2_score'] : '0')));
		$auto_far_left_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_left_l3_score']) ? $_POST['auto_far_left_l3_score'] : '0')));
		$auto_far_left_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_left_l4_score']) ? $_POST['auto_far_left_l4_score'] : '0')));
		$auto_far_left_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_left_l1_miss']) ? $_POST['auto_far_left_l1_miss'] : '0')));
		$auto_far_left_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_left_l2_miss']) ? $_POST['auto_far_left_l2_miss'] : '0')));
		$auto_far_left_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_left_l3_miss']) ? $_POST['auto_far_left_l3_miss'] : '0')));
		$auto_far_left_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_left_l4_miss']) ? $_POST['auto_far_left_l4_miss'] : '0')));
		$auto_far_right_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_right_l1_score']) ? $_POST['auto_far_right_l1_score'] : '0')));
		$auto_far_right_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_right_l2_score']) ? $_POST['auto_far_right_l2_score'] : '0')));
		$auto_far_right_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_right_l3_score']) ? $_POST['auto_far_right_l3_score'] : '0')));
		$auto_far_right_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_right_l4_score']) ? $_POST['auto_far_right_l4_score'] : '0')));
		$auto_far_right_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_right_l1_miss']) ? $_POST['auto_far_right_l1_miss'] : '0')));
		$auto_far_right_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_right_l2_miss']) ? $_POST['auto_far_right_l2_miss'] : '0')));
		$auto_far_right_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_right_l3_miss']) ? $_POST['auto_far_right_l3_miss'] : '0')));
		$auto_far_right_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_far_right_l4_miss']) ? $_POST['auto_far_right_l4_miss'] : '0')));
		$auto_processor_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_processor_score']) ? $_POST['auto_processor_score'] : '0')));
		$auto_processor_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_processor_miss']) ? $_POST['auto_processor_miss'] : '0')));
		$auto_net_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_net_score']) ? $_POST['auto_net_score'] : '0')));
		$auto_net_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_net_miss']) ? $_POST['auto_net_miss'] : '0')));
		$auto_l2_algae_removed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_l2_algae_removed']) ? $_POST['auto_l2_algae_removed'] : '0')));
		$auto_l3_algae_removed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_l3_algae_removed']) ? $_POST['auto_l3_algae_removed'] : '0')));
		$auto_coral_intake_ground_left = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_coral_intake_ground_left']) ? $_POST['auto_coral_intake_ground_left'] : '0')));
		$auto_coral_intake_ground_center = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_coral_intake_ground_center']) ? $_POST['auto_coral_intake_ground_center'] : '0')));
		$auto_coral_intake_ground_right = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_coral_intake_ground_right']) ? $_POST['auto_coral_intake_ground_right'] : '0')));
		$auto_algae_intake_ground_left = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_algae_intake_ground_left']) ? $_POST['auto_algae_intake_ground_left'] : '0')));
		$auto_algae_intake_ground_center = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_algae_intake_ground_center']) ? $_POST['auto_algae_intake_ground_center'] : '0')));
		$auto_algae_intake_ground_right = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_algae_intake_ground_right']) ? $_POST['auto_algae_intake_ground_right'] : '0')));
		$auto_algae_discarded = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_algae_discarded']) ? $_POST['auto_algae_discarded'] : '0')));
		$auto_feeder_station_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_feeder_station_id']) ? $_POST['auto_feeder_station_id'] : '0')));
		$auto_stop = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['auto_stop']) ? $_POST['auto_stop'] : '0')));
		$player_center_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_center_l1_score']) ? $_POST['player_center_l1_score'] : '0')));
		$player_center_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_center_l2_score']) ? $_POST['player_center_l2_score'] : '0')));
		$player_center_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_center_l3_score']) ? $_POST['player_center_l3_score'] : '0')));
		$player_center_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_center_l4_score']) ? $_POST['player_center_l4_score'] : '0')));
		$player_center_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_center_l1_miss']) ? $_POST['player_center_l1_miss'] : '0')));
		$player_center_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_center_l2_miss']) ? $_POST['player_center_l2_miss'] : '0')));
		$player_center_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_center_l3_miss']) ? $_POST['player_center_l3_miss'] : '0')));
		$player_center_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_center_l4_miss']) ? $_POST['player_center_l4_miss'] : '0')));
		$player_left_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_left_l1_score']) ? $_POST['player_left_l1_score'] : '0')));
		$player_left_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_left_l2_score']) ? $_POST['player_left_l2_score'] : '0')));
		$player_left_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_left_l3_score']) ? $_POST['player_left_l3_score'] : '0')));
		$player_left_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_left_l4_score']) ? $_POST['player_left_l4_score'] : '0')));
		$player_left_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_left_l1_miss']) ? $_POST['player_left_l1_miss'] : '0')));
		$player_left_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_left_l2_miss']) ? $_POST['player_left_l2_miss'] : '0')));
		$player_left_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_left_l3_miss']) ? $_POST['player_left_l3_miss'] : '0')));
		$player_left_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_left_l4_miss']) ? $_POST['player_left_l4_miss'] : '0')));
		$player_right_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_right_l1_score']) ? $_POST['player_right_l1_score'] : '0')));
		$player_right_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_right_l2_score']) ? $_POST['player_right_l2_score'] : '0')));
		$player_right_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_right_l3_score']) ? $_POST['player_right_l3_score'] : '0')));
		$player_right_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_right_l4_score']) ? $_POST['player_right_l4_score'] : '0')));
		$player_right_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_right_l1_miss']) ? $_POST['player_right_l1_miss'] : '0')));
		$player_right_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_right_l2_miss']) ? $_POST['player_right_l2_miss'] : '0')));
		$player_right_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_right_l3_miss']) ? $_POST['player_right_l3_miss'] : '0')));
		$player_right_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['player_right_l4_miss']) ? $_POST['player_right_l4_miss'] : '0')));
		$far_center_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_center_l1_score']) ? $_POST['far_center_l1_score'] : '0')));
		$far_center_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_center_l2_score']) ? $_POST['far_center_l2_score'] : '0')));
		$far_center_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_center_l3_score']) ? $_POST['far_center_l3_score'] : '0')));
		$far_center_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_center_l4_score']) ? $_POST['far_center_l4_score'] : '0')));
		$far_center_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_center_l1_miss']) ? $_POST['far_center_l1_miss'] : '0')));
		$far_center_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_center_l2_miss']) ? $_POST['far_center_l2_miss'] : '0')));
		$far_center_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_center_l3_miss']) ? $_POST['far_center_l3_miss'] : '0')));
		$far_center_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_center_l4_miss']) ? $_POST['far_center_l4_miss'] : '0')));
		$far_left_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_left_l1_score']) ? $_POST['far_left_l1_score'] : '0')));
		$far_left_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_left_l2_score']) ? $_POST['far_left_l2_score'] : '0')));
		$far_left_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_left_l3_score']) ? $_POST['far_left_l3_score'] : '0')));
		$far_left_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_left_l4_score']) ? $_POST['far_left_l4_score'] : '0')));
		$far_left_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_left_l1_miss']) ? $_POST['far_left_l1_miss'] : '0')));
		$far_left_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_left_l2_miss']) ? $_POST['far_left_l2_miss'] : '0')));
		$far_left_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_left_l3_miss']) ? $_POST['far_left_l3_miss'] : '0')));
		$far_left_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_left_l4_miss']) ? $_POST['far_left_l4_miss'] : '0')));
		$far_right_l1_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_right_l1_score']) ? $_POST['far_right_l1_score'] : '0')));
		$far_right_l2_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_right_l2_score']) ? $_POST['far_right_l2_score'] : '0')));
		$far_right_l3_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_right_l3_score']) ? $_POST['far_right_l3_score'] : '0')));
		$far_right_l4_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_right_l4_score']) ? $_POST['far_right_l4_score'] : '0')));
		$far_right_l1_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_right_l1_miss']) ? $_POST['far_right_l1_miss'] : '0')));
		$far_right_l2_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_right_l2_miss']) ? $_POST['far_right_l2_miss'] : '0')));
		$far_right_l3_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_right_l3_miss']) ? $_POST['far_right_l3_miss'] : '0')));
		$far_right_l4_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['far_right_l4_miss']) ? $_POST['far_right_l4_miss'] : '0')));
		$processor_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['processor_score']) ? $_POST['processor_score'] : '0')));
		$processor_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['processor_miss']) ? $_POST['processor_miss'] : '0')));
		$net_score = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['net_score']) ? $_POST['net_score'] : '0')));
		$net_miss = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['net_miss']) ? $_POST['net_miss'] : '0')));
		$l2_algae_removed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['l2_algae_removed']) ? $_POST['l2_algae_removed'] : '0')));
		$l3_algae_removed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['l3_algae_removed']) ? $_POST['l3_algae_removed'] : '0')));
		$controlled_algae_removal = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['controlled_algae_removal']) ? $_POST['controlled_algae_removal'] : '0')));
		$can_launch_algae_over_barge = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_launch_algae_over_barge']) ? $_POST['can_launch_algae_over_barge'] : '0')));
		$can_propel_algae_through_barge = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_propel_algae_through_barge']) ? $_POST['can_propel_algae_through_barge'] : '0')));
		$can_place_algae_precisely = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_place_algae_precisely']) ? $_POST['can_place_algae_precisely'] : '0')));
		$scores_allowed_while_def = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['scores_allowed_while_def']) ? $_POST['scores_allowed_while_def'] : '0')));
		$algae_stolen_from_opp = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['algae_stolen_from_opp']) ? $_POST['algae_stolen_from_opp'] : '0')));
		$algae_stolen_by_opp = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['algae_stolen_by_opp']) ? $_POST['algae_stolen_by_opp'] : '0')));
		$deep_climb_success = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['deep_climb_success']) ? $_POST['deep_climb_success'] : '0')));
		$deep_climb_unsuccessful_attempt = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['deep_climb_unsuccessful_attempt']) ? $_POST['deep_climb_unsuccessful_attempt'] : '0')));
		$shallow_climb_success = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['shallow_climb_success']) ? $_POST['shallow_climb_success'] : '0')));
		$shallow_climb_unsuccessful_attempt = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['shallow_climb_unsuccessful_attempt']) ? $_POST['shallow_climb_unsuccessful_attempt'] : '0')));
		$barge_park = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['barge_park']) ? $_POST['barge_park'] : '0')));
		$buddy_climb = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['buddy_climb']) ? $_POST['buddy_climb'] : '0')));
		$degraded_performance = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['degraded_performance']) ? $_POST['degraded_performance'] : '0')));
		$degrade_causes_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['degrade_causes_id']) ? $_POST['degrade_causes_id'] : '0')));
		$causes_penalties = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['causes_penalties']) ? $_POST['causes_penalties'] : '0')));
		$yellow_card = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['yellow_card']) ? $_POST['yellow_card'] : '0')));
		$red_card = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['red_card']) ? $_POST['red_card'] : '0')));
		$no_show = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['no_show']) ? $_POST['no_show'] : '0')));
		$no_show_w_rep = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['no_show_w_rep']) ? $_POST['no_show_w_rep'] : '0')));
		$notes = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['notes']) ? $_POST['notes'] : '0')));

		$result = mysqli_query($link,"SELECT id FROM fact_match_data_2025 WHERE event_id=" . $event_id . " AND match_id=" . $match_id . " AND team_id=" . $team_id . " AND practice_match=" . $practice_match);
		$row = mysqli_fetch_array($result);
		$match_row_id = $row["id"];
		if (mysqli_num_rows($result) == 0) {
			$query = "INSERT INTO fact_match_data_2025(event_id,team_id,match_id,practice_match,position_id,auto_barge_zone_id,auto_first_face_id,auto_ground_face_id,auto_leave,auto_player_center_l1_score,auto_player_center_l2_score,auto_player_center_l3_score,auto_player_center_l4_score,auto_player_center_l1_miss,auto_player_center_l2_miss,auto_player_center_l3_miss,auto_player_center_l4_miss,auto_player_left_l1_score,auto_player_left_l2_score,auto_player_left_l3_score,auto_player_left_l4_score,auto_player_left_l1_miss,auto_player_left_l2_miss,auto_player_left_l3_miss,auto_player_left_l4_miss,auto_player_right_l1_score,auto_player_right_l2_score,auto_player_right_l3_score,auto_player_right_l4_score,auto_player_right_l1_miss,auto_player_right_l2_miss,auto_player_right_l3_miss,auto_player_right_l4_miss,auto_far_center_l1_score,auto_far_center_l2_score,auto_far_center_l3_score,auto_far_center_l4_score,auto_far_center_l1_miss,auto_far_center_l2_miss,auto_far_center_l3_miss,auto_far_center_l4_miss,auto_far_left_l1_score,auto_far_left_l2_score,auto_far_left_l3_score,auto_far_left_l4_score,auto_far_left_l1_miss,auto_far_left_l2_miss,auto_far_left_l3_miss,auto_far_left_l4_miss,auto_far_right_l1_score,auto_far_right_l2_score,auto_far_right_l3_score,auto_far_right_l4_score,auto_far_right_l1_miss,auto_far_right_l2_miss,auto_far_right_l3_miss,auto_far_right_l4_miss,auto_processor_score,auto_processor_miss,auto_net_score,auto_net_miss,auto_l2_algae_removed,auto_l3_algae_removed,auto_coral_intake_ground_left,auto_coral_intake_ground_center,auto_coral_intake_ground_right,auto_algae_intake_ground_left,auto_algae_intake_ground_center,auto_algae_intake_ground_right,auto_algae_discarded,auto_feeder_station_id,auto_stop,player_center_l1_score,player_center_l2_score,player_center_l3_score,player_center_l4_score,player_center_l1_miss,player_center_l2_miss,player_center_l3_miss,player_center_l4_miss,player_left_l1_score,player_left_l2_score,player_left_l3_score,player_left_l4_score,player_left_l1_miss,player_left_l2_miss,player_left_l3_miss,player_left_l4_miss,player_right_l1_score,player_right_l2_score,player_right_l3_score,player_right_l4_score,player_right_l1_miss,player_right_l2_miss,player_right_l3_miss,player_right_l4_miss,far_center_l1_score,far_center_l2_score,far_center_l3_score,far_center_l4_score,far_center_l1_miss,far_center_l2_miss,far_center_l3_miss,far_center_l4_miss,far_left_l1_score,far_left_l2_score,far_left_l3_score,far_left_l4_score,far_left_l1_miss,far_left_l2_miss,far_left_l3_miss,far_left_l4_miss,far_right_l1_score,far_right_l2_score,far_right_l3_score,far_right_l4_score,far_right_l1_miss,far_right_l2_miss,far_right_l3_miss,far_right_l4_miss,processor_score,processor_miss,net_score,net_miss,l2_algae_removed,l3_algae_removed,controlled_algae_removal,can_launch_algae_over_barge,can_propel_algae_through_barge,can_place_algae_precisely,scores_allowed_while_def,algae_stolen_from_opp,algae_stolen_by_opp,deep_climb_success,deep_climb_unsuccessful_attempt,shallow_climb_success,shallow_climb_unsuccessful_attempt,barge_park,buddy_climb,degraded_performance,degrade_causes_id,causes_penalties,yellow_card,red_card,no_show,no_show_w_rep,notes,invalid) VALUES("
				. $event_id . ","
				. $team_id . ","
				. $match_id . ","
				. $practice_match . ","
				. $position_id . ","
				. $auto_barge_zone_id . ","
				. $auto_first_face_id . ","
				. $auto_ground_face_id . ","
				. $auto_leave . ","
				. $auto_player_center_l1_score . ","
				. $auto_player_center_l2_score . ","
				. $auto_player_center_l3_score . ","
				. $auto_player_center_l4_score . ","
				. $auto_player_center_l1_miss . ","
				. $auto_player_center_l2_miss . ","
				. $auto_player_center_l3_miss . ","
				. $auto_player_center_l4_miss . ","
				. $auto_player_left_l1_score . ","
				. $auto_player_left_l2_score . ","
				. $auto_player_left_l3_score . ","
				. $auto_player_left_l4_score . ","
				. $auto_player_left_l1_miss . ","
				. $auto_player_left_l2_miss . ","
				. $auto_player_left_l3_miss . ","
				. $auto_player_left_l4_miss . ","
				. $auto_player_right_l1_score . ","
				. $auto_player_right_l2_score . ","
				. $auto_player_right_l3_score . ","
				. $auto_player_right_l4_score . ","
				. $auto_player_right_l1_miss . ","
				. $auto_player_right_l2_miss . ","
				. $auto_player_right_l3_miss . ","
				. $auto_player_right_l4_miss . ","
				. $auto_far_center_l1_score . ","
				. $auto_far_center_l2_score . ","
				. $auto_far_center_l3_score . ","
				. $auto_far_center_l4_score . ","
				. $auto_far_center_l1_miss . ","
				. $auto_far_center_l2_miss . ","
				. $auto_far_center_l3_miss . ","
				. $auto_far_center_l4_miss . ","
				. $auto_far_left_l1_score . ","
				. $auto_far_left_l2_score . ","
				. $auto_far_left_l3_score . ","
				. $auto_far_left_l4_score . ","
				. $auto_far_left_l1_miss . ","
				. $auto_far_left_l2_miss . ","
				. $auto_far_left_l3_miss . ","
				. $auto_far_left_l4_miss . ","
				. $auto_far_right_l1_score . ","
				. $auto_far_right_l2_score . ","
				. $auto_far_right_l3_score . ","
				. $auto_far_right_l4_score . ","
				. $auto_far_right_l1_miss . ","
				. $auto_far_right_l2_miss . ","
				. $auto_far_right_l3_miss . ","
				. $auto_far_right_l4_miss . ","
				. $auto_processor_score . ","
				. $auto_processor_miss . ","
				. $auto_net_score . ","
				. $auto_net_miss . ","
				. $auto_l2_algae_removed . ","
				. $auto_l3_algae_removed . ","
				. $auto_coral_intake_ground_left . ","
				. $auto_coral_intake_ground_center . ","
				. $auto_coral_intake_ground_right . ","
				. $auto_algae_intake_ground_left . ","
				. $auto_algae_intake_ground_center . ","
				. $auto_algae_intake_ground_right . ","
				. $auto_algae_discarded . ","
				. $auto_feeder_station_id . ","
				. $auto_stop . ","
				. $player_center_l1_score . ","
				. $player_center_l2_score . ","
				. $player_center_l3_score . ","
				. $player_center_l4_score . ","
				. $player_center_l1_miss . ","
				. $player_center_l2_miss . ","
				. $player_center_l3_miss . ","
				. $player_center_l4_miss . ","
				. $player_left_l1_score . ","
				. $player_left_l2_score . ","
				. $player_left_l3_score . ","
				. $player_left_l4_score . ","
				. $player_left_l1_miss . ","
				. $player_left_l2_miss . ","
				. $player_left_l3_miss . ","
				. $player_left_l4_miss . ","
				. $player_right_l1_score . ","
				. $player_right_l2_score . ","
				. $player_right_l3_score . ","
				. $player_right_l4_score . ","
				. $player_right_l1_miss . ","
				. $player_right_l2_miss . ","
				. $player_right_l3_miss . ","
				. $player_right_l4_miss . ","
				. $far_center_l1_score . ","
				. $far_center_l2_score . ","
				. $far_center_l3_score . ","
				. $far_center_l4_score . ","
				. $far_center_l1_miss . ","
				. $far_center_l2_miss . ","
				. $far_center_l3_miss . ","
				. $far_center_l4_miss . ","
				. $far_left_l1_score . ","
				. $far_left_l2_score . ","
				. $far_left_l3_score . ","
				. $far_left_l4_score . ","
				. $far_left_l1_miss . ","
				. $far_left_l2_miss . ","
				. $far_left_l3_miss . ","
				. $far_left_l4_miss . ","
				. $far_right_l1_score . ","
				. $far_right_l2_score . ","
				. $far_right_l3_score . ","
				. $far_right_l4_score . ","
				. $far_right_l1_miss . ","
				. $far_right_l2_miss . ","
				. $far_right_l3_miss . ","
				. $far_right_l4_miss . ","
				. $processor_score . ","
				. $processor_miss . ","
				. $net_score . ","
				. $net_miss . ","
				. $l2_algae_removed . ","
				. $l3_algae_removed . ","
				. $controlled_algae_removal . ","
				. $can_launch_algae_over_barge . ","
				. $can_propel_algae_through_barge . ","
				. $can_place_algae_precisely . ","
				. $scores_allowed_while_def . ","
				. $algae_stolen_from_opp . ","
				. $algae_stolen_by_opp . ","
				. $deep_climb_success . ","
				. $deep_climb_unsuccessful_attempt . ","
				. $shallow_climb_success . ","
				. $shallow_climb_unsuccessful_attempt . ","
				. $barge_park . ","
				. $buddy_climb . ","
				. $degraded_performance . ","
				. $degrade_causes_id . ","
				. $causes_penalties . ","
				. $yellow_card . ","
				. $red_card . ","
				. $no_show . ","
				. $no_show_w_rep . ","
				. "'" . $notes . "',"
				. "0);";
			$success = mysqli_query($link,$query);
		}
		else {
			$query = "UPDATE fact_match_data_2025 SET "
				. "event_id=" . $event_id . ","
				. "team_id=" . $team_id . ","
				. "match_id=" . $match_id . ","
				. "practice_match=" . $practice_match . ","
				. "position_id=" . $position_id . ","
				. "auto_barge_zone_id=" . $auto_barge_zone_id . ","
				. "auto_first_face_id=" . $auto_first_face_id . ","
				. "auto_ground_face_id=" . $auto_ground_face_id . ","
				. "auto_leave=" . $auto_leave . ","
				. "auto_player_center_l1_score=" . $auto_player_center_l1_score . ","
				. "auto_player_center_l2_score=" . $auto_player_center_l2_score . ","
				. "auto_player_center_l3_score=" . $auto_player_center_l3_score . ","
				. "auto_player_center_l4_score=" . $auto_player_center_l4_score . ","
				. "auto_player_center_l1_miss=" . $auto_player_center_l1_miss . ","
				. "auto_player_center_l2_miss=" . $auto_player_center_l2_miss . ","
				. "auto_player_center_l3_miss=" . $auto_player_center_l3_miss . ","
				. "auto_player_center_l4_miss=" . $auto_player_center_l4_miss . ","
				. "auto_player_left_l1_score=" . $auto_player_left_l1_score . ","
				. "auto_player_left_l2_score=" . $auto_player_left_l2_score . ","
				. "auto_player_left_l3_score=" . $auto_player_left_l3_score . ","
				. "auto_player_left_l4_score=" . $auto_player_left_l4_score . ","
				. "auto_player_left_l1_miss=" . $auto_player_left_l1_miss . ","
				. "auto_player_left_l2_miss=" . $auto_player_left_l2_miss . ","
				. "auto_player_left_l3_miss=" . $auto_player_left_l3_miss . ","
				. "auto_player_left_l4_miss=" . $auto_player_left_l4_miss . ","
				. "auto_player_right_l1_score=" . $auto_player_right_l1_score . ","
				. "auto_player_right_l2_score=" . $auto_player_right_l2_score . ","
				. "auto_player_right_l3_score=" . $auto_player_right_l3_score . ","
				. "auto_player_right_l4_score=" . $auto_player_right_l4_score . ","
				. "auto_player_right_l1_miss=" . $auto_player_right_l1_miss . ","
				. "auto_player_right_l2_miss=" . $auto_player_right_l2_miss . ","
				. "auto_player_right_l3_miss=" . $auto_player_right_l3_miss . ","
				. "auto_player_right_l4_miss=" . $auto_player_right_l4_miss . ","
				. "auto_far_center_l1_score=" . $auto_far_center_l1_score . ","
				. "auto_far_center_l2_score=" . $auto_far_center_l2_score . ","
				. "auto_far_center_l3_score=" . $auto_far_center_l3_score . ","
				. "auto_far_center_l4_score=" . $auto_far_center_l4_score . ","
				. "auto_far_center_l1_miss=" . $auto_far_center_l1_miss . ","
				. "auto_far_center_l2_miss=" . $auto_far_center_l2_miss . ","
				. "auto_far_center_l3_miss=" . $auto_far_center_l3_miss . ","
				. "auto_far_center_l4_miss=" . $auto_far_center_l4_miss . ","
				. "auto_far_left_l1_score=" . $auto_far_left_l1_score . ","
				. "auto_far_left_l2_score=" . $auto_far_left_l2_score . ","
				. "auto_far_left_l3_score=" . $auto_far_left_l3_score . ","
				. "auto_far_left_l4_score=" . $auto_far_left_l4_score . ","
				. "auto_far_left_l1_miss=" . $auto_far_left_l1_miss . ","
				. "auto_far_left_l2_miss=" . $auto_far_left_l2_miss . ","
				. "auto_far_left_l3_miss=" . $auto_far_left_l3_miss . ","
				. "auto_far_left_l4_miss=" . $auto_far_left_l4_miss . ","
				. "auto_far_right_l1_score=" . $auto_far_right_l1_score . ","
				. "auto_far_right_l2_score=" . $auto_far_right_l2_score . ","
				. "auto_far_right_l3_score=" . $auto_far_right_l3_score . ","
				. "auto_far_right_l4_score=" . $auto_far_right_l4_score . ","
				. "auto_far_right_l1_miss=" . $auto_far_right_l1_miss . ","
				. "auto_far_right_l2_miss=" . $auto_far_right_l2_miss . ","
				. "auto_far_right_l3_miss=" . $auto_far_right_l3_miss . ","
				. "auto_far_right_l4_miss=" . $auto_far_right_l4_miss . ","
				. "auto_processor_score=" . $auto_processor_score . ","
				. "auto_processor_miss=" . $auto_processor_miss . ","
				. "auto_net_score=" . $auto_net_score . ","
				. "auto_net_miss=" . $auto_net_miss . ","
				. "auto_l2_algae_removed=" . $auto_l2_algae_removed . ","
				. "auto_l3_algae_removed=" . $auto_l3_algae_removed . ","
				. "auto_coral_intake_ground_left=" . $auto_coral_intake_ground_left . ","
				. "auto_coral_intake_ground_center=" . $auto_coral_intake_ground_center . ","
				. "auto_coral_intake_ground_right=" . $auto_coral_intake_ground_right . ","
				. "auto_algae_intake_ground_left=" . $auto_algae_intake_ground_left . ","
				. "auto_algae_intake_ground_center=" . $auto_algae_intake_ground_center . ","
				. "auto_algae_intake_ground_right=" . $auto_algae_intake_ground_right . ","
				. "auto_algae_discarded=" . $auto_algae_discarded . ","
				. "auto_feeder_station_id=" . $auto_feeder_station_id . ","
				. "auto_stop=" . $auto_stop . ","
				. "player_center_l1_score=" . $player_center_l1_score . ","
				. "player_center_l2_score=" . $player_center_l2_score . ","
				. "player_center_l3_score=" . $player_center_l3_score . ","
				. "player_center_l4_score=" . $player_center_l4_score . ","
				. "player_center_l1_miss=" . $player_center_l1_miss . ","
				. "player_center_l2_miss=" . $player_center_l2_miss . ","
				. "player_center_l3_miss=" . $player_center_l3_miss . ","
				. "player_center_l4_miss=" . $player_center_l4_miss . ","
				. "player_left_l1_score=" . $player_left_l1_score . ","
				. "player_left_l2_score=" . $player_left_l2_score . ","
				. "player_left_l3_score=" . $player_left_l3_score . ","
				. "player_left_l4_score=" . $player_left_l4_score . ","
				. "player_left_l1_miss=" . $player_left_l1_miss . ","
				. "player_left_l2_miss=" . $player_left_l2_miss . ","
				. "player_left_l3_miss=" . $player_left_l3_miss . ","
				. "player_left_l4_miss=" . $player_left_l4_miss . ","
				. "player_right_l1_score=" . $player_right_l1_score . ","
				. "player_right_l2_score=" . $player_right_l2_score . ","
				. "player_right_l3_score=" . $player_right_l3_score . ","
				. "player_right_l4_score=" . $player_right_l4_score . ","
				. "player_right_l1_miss=" . $player_right_l1_miss . ","
				. "player_right_l2_miss=" . $player_right_l2_miss . ","
				. "player_right_l3_miss=" . $player_right_l3_miss . ","
				. "player_right_l4_miss=" . $player_right_l4_miss . ","
				. "far_center_l1_score=" . $far_center_l1_score . ","
				. "far_center_l2_score=" . $far_center_l2_score . ","
				. "far_center_l3_score=" . $far_center_l3_score . ","
				. "far_center_l4_score=" . $far_center_l4_score . ","
				. "far_center_l1_miss=" . $far_center_l1_miss . ","
				. "far_center_l2_miss=" . $far_center_l2_miss . ","
				. "far_center_l3_miss=" . $far_center_l3_miss . ","
				. "far_center_l4_miss=" . $far_center_l4_miss . ","
				. "far_left_l1_score=" . $far_left_l1_score . ","
				. "far_left_l2_score=" . $far_left_l2_score . ","
				. "far_left_l3_score=" . $far_left_l3_score . ","
				. "far_left_l4_score=" . $far_left_l4_score . ","
				. "far_left_l1_miss=" . $far_left_l1_miss . ","
				. "far_left_l2_miss=" . $far_left_l2_miss . ","
				. "far_left_l3_miss=" . $far_left_l3_miss . ","
				. "far_left_l4_miss=" . $far_left_l4_miss . ","
				. "far_right_l1_score=" . $far_right_l1_score . ","
				. "far_right_l2_score=" . $far_right_l2_score . ","
				. "far_right_l3_score=" . $far_right_l3_score . ","
				. "far_right_l4_score=" . $far_right_l4_score . ","
				. "far_right_l1_miss=" . $far_right_l1_miss . ","
				. "far_right_l2_miss=" . $far_right_l2_miss . ","
				. "far_right_l3_miss=" . $far_right_l3_miss . ","
				. "far_right_l4_miss=" . $far_right_l4_miss . ","
				. "processor_score=" . $processor_score . ","
				. "processor_miss=" . $processor_miss . ","
				. "net_score=" . $net_score . ","
				. "net_miss=" . $net_miss . ","
				. "l2_algae_removed=" . $l2_algae_removed . ","
				. "l3_algae_removed=" . $l3_algae_removed . ","
				. "controlled_algae_removal=" . $controlled_algae_removal . ","
				. "can_launch_algae_over_barge=" . $can_launch_algae_over_barge . ","
				. "can_propel_algae_through_barge=" . $can_propel_algae_through_barge . ","
				. "can_place_algae_precisely=" . $can_place_algae_precisely . ","
				. "scores_allowed_while_def=" . $scores_allowed_while_def . ","
				. "algae_stolen_from_opp=" . $algae_stolen_from_opp . ","
				. "algae_stolen_by_opp=" . $algae_stolen_by_opp . ","
				. "deep_climb_success=" . $deep_climb_success . ","
				. "deep_climb_unsuccessful_attempt=" . $deep_climb_unsuccessful_attempt . ","
				. "shallow_climb_success=" . $shallow_climb_success . ","
				. "shallow_climb_unsuccessful_attempt=" . $shallow_climb_unsuccessful_attempt . ","
				. "barge_park=" . $barge_park . ","
				. "buddy_climb=" . $buddy_climb . ","
				. "degraded_performance=" . $degraded_performance . ","
				. "degrade_causes_id=" . $degrade_causes_id . ","
				. "causes_penalties=" . $causes_penalties . ","
				. "yellow_card=" . $yellow_card . ","
				. "red_card=" . $red_card . ","
				. "no_show=" . $no_show . ","
				. "no_show_w_rep=" . $no_show_w_rep . ","
				. "notes='" . $notes . "',"
				. "invalid=0"
				. " WHERE id=" . $match_row_id;

			$success = mysqli_query($link,$query);
		}
		if ($success) {
			$result = mysqli_query($link,"SELECT id, timestamp FROM fact_match_data_2025 WHERE event_id=" . $event_id . " AND match_id=" . $match_id . " AND team_id=" . $team_id . " AND practice_match=" . $practice_match);
			$row = mysqli_fetch_array($result);
			$resp = $row["id"] . "," . strtotime($row["timestamp"]);
		} else {
			$resp = 'Database Query Failed : \n' . $query;
		}
	}
	else if ($_POST['type'] == 'superscout') {
		header('Content-Type: text/plain; charset=utf-8');
		$event_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['event_id']) ? $_POST['event_id'] : '0')));
		$team_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['team_id']) ? $_POST['team_id'] : '0')));
		$match_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['match_id']) ? $_POST['match_id'] : '0')));
		$practice_match = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['practice_match']) ? $_POST['practice_match'] : '0')));
		$position_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['position_id']) ? $_POST['position_id'] : '0')));
		$robot_drive_speed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['robot_drive_speed']) ? $_POST['robot_drive_speed'] : '0')));
		$time_to_opp_algae = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['time_to_opp_algae']) ? $_POST['time_to_opp_algae'] : '0')));
		$coral_intake_speed_ground = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['coral_intake_speed_ground']) ? $_POST['coral_intake_speed_ground'] : '0')));
		$coral_intake_speed_station = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['coral_intake_speed_station']) ? $_POST['coral_intake_speed_station'] : '0')));
		$algae_intake_speed_ground = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['algae_intake_speed_ground']) ? $_POST['algae_intake_speed_ground'] : '0')));
		$algae_intake_speed_reef = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['algae_intake_speed_reef']) ? $_POST['algae_intake_speed_reef'] : '0')));
		$opportunistic_defense = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['opportunistic_defense']) ? $_POST['opportunistic_defense'] : '0')));
		$algae_removal_speed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['algae_removal_speed']) ? $_POST['algae_removal_speed'] : '0')));
		$net_scoring_speed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['net_scoring_speed']) ? $_POST['net_scoring_speed'] : '0')));
		$processor_scoring_speed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['processor_scoring_speed']) ? $_POST['processor_scoring_speed'] : '0')));
		$l1_scoring_speed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['l1_scoring_speed']) ? $_POST['l1_scoring_speed'] : '0')));
		$l2_l3_scoring_speed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['l2_l3_scoring_speed']) ? $_POST['l2_l3_scoring_speed'] : '0')));
		$l4_scoring_speed = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['l4_scoring_speed']) ? $_POST['l4_scoring_speed'] : '0')));
		$l2_l3_difference = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['l2_l3_difference']) ? $_POST['l2_l3_difference'] : '0')));
		$easily_defended = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['easily_defended']) ? $_POST['easily_defended'] : '0')));
		$tippy = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['tippy']) ? $_POST['tippy'] : '0')));
		$notes = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['notes']) ? $_POST['notes'] : '0')));

		$result = mysqli_query($link,"SELECT id FROM superscout_data_2025 WHERE event_id=" . $event_id . " AND match_id=" . $match_id . " AND team_id=" . $team_id . " AND practice_match=" . $practice_match);
		$row = mysqli_fetch_array($result);
		$match_row_id = $row["id"];
		if (mysqli_num_rows($result) == 0) {
			$query = "INSERT INTO superscout_data_2025(event_id,team_id,match_id,practice_match,position_id,robot_drive_speed,time_to_opp_algae,coral_intake_speed_ground,coral_intake_speed_station,algae_intake_speed_ground,algae_intake_speed_reef,opportunistic_defense,algae_removal_speed,net_scoring_speed,processor_scoring_speed,l1_scoring_speed,l2_l3_scoring_speed,l4_scoring_speed,l2_l3_difference,easily_defended,tippy,notes,invalid) VALUES("
				. $event_id . ","
				. $team_id . ","
				. $match_id . ","
				. $practice_match . ","
				. $position_id . ","
				. $robot_drive_speed . ","
				. $time_to_opp_algae . ","
				. $coral_intake_speed_ground . ","
				. $coral_intake_speed_station . ","
				. $algae_intake_speed_ground . ","
				. $algae_intake_speed_reef . ","
				. $opportunistic_defense . ","
				. $algae_removal_speed . ","
				. $net_scoring_speed . ","
				. $processor_scoring_speed . ","
				. $l1_scoring_speed . ","
				. $l2_l3_scoring_speed . ","
				. $l4_scoring_speed . ","
				. $l2_l3_difference . ","
				. $easily_defended . ","
				. $tippy . ","
				. "'" . $notes . "',"
				. "0);";
			$success = mysqli_query($link,$query);
		}
		else {
			$query = "UPDATE superscout_data_2025 SET "
				. "event_id=" . $event_id . ","
				. "team_id=" . $team_id . ","
				. "match_id=" . $match_id . ","
				. "practice_match=" . $practice_match . ","
				. "position_id=" . $position_id . ","
				. "robot_drive_speed=" . $robot_drive_speed . ","
				. "time_to_opp_algae=" . $time_to_opp_algae . ","
				. "coral_intake_speed_ground=" . $coral_intake_speed_ground . ","
				. "coral_intake_speed_station=" . $coral_intake_speed_station . ","
				. "algae_intake_speed_ground=" . $algae_intake_speed_ground . ","
				. "algae_intake_speed_reef=" . $algae_intake_speed_reef . ","
				. "opportunistic_defense=" . $opportunistic_defense . ","
				. "algae_removal_speed=" . $algae_removal_speed . ","
				. "net_scoring_speed=" . $net_scoring_speed . ","
				. "processor_scoring_speed=" . $processor_scoring_speed . ","
				. "l1_scoring_speed=" . $l1_scoring_speed . ","
				. "l2_l3_scoring_speed=" . $l2_l3_scoring_speed . ","
				. "l4_scoring_speed=" . $l4_scoring_speed . ","
				. "l2_l3_difference=" . $l2_l3_difference . ","
				. "easily_defended=" . $easily_defended . ","
				. "tippy=" . $tippy . ","
				. "notes='" . $notes . "',"
				. "invalid=0"
				. " WHERE id=" . $match_row_id;

			$success = mysqli_query($link,$query);
		}
		if ($success) {
			$result = mysqli_query($link,"SELECT id, timestamp FROM superscout_data_2025 WHERE event_id=" . $event_id . " AND match_id=" . $match_id . " AND team_id=" . $team_id . " AND practice_match=" . $practice_match);
			$row = mysqli_fetch_array($result);
			$resp = $row["id"] . "," . strtotime($row["timestamp"]);
		} else {
			$resp = 'Database Query Failed : \n' . $query;
		}
	}
	else if ($_POST['type'] == 'pits') {
		header('Content-Type: text/plain; charset=utf-8');
		$event_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['event_id']) ? $_POST['event_id'] : '0')));
		$team_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['team_id']) ? $_POST['team_id'] : '0')));
		$pass_number = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['pass_number']) ? $_POST['pass_number'] : '0')));
		$robot_inspection_weight_lbs = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['robot_inspection_weight_lbs']) ? $_POST['robot_inspection_weight_lbs'] : '0')));
		$robot_wheel_width_in = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['robot_wheel_width_in']) ? $_POST['robot_wheel_width_in'] : '0')));
		$drive_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['drive_id']) ? $_POST['drive_id'] : '0')));
		$robot_cg_id = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['robot_cg_id']) ? $_POST['robot_cg_id'] : '0')));
		$can_score_proc = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_score_proc']) ? $_POST['can_score_proc'] : '0')));
		$can_reef_intake_algae = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_reef_intake_algae']) ? $_POST['can_reef_intake_algae'] : '0')));
		$can_ground_intake_algae = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_ground_intake_algae']) ? $_POST['can_ground_intake_algae'] : '0')));
		$can_ground_intake_coral = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_ground_intake_coral']) ? $_POST['can_ground_intake_coral'] : '0')));
		$can_station_intake_coral = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_station_intake_coral']) ? $_POST['can_station_intake_coral'] : '0')));
		$can_remove_algae_l2 = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_remove_algae_l2']) ? $_POST['can_remove_algae_l2'] : '0')));
		$can_remove_algae_l3 = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_remove_algae_l3']) ? $_POST['can_remove_algae_l3'] : '0')));
		$can_carry_both_simul = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_carry_both_simul']) ? $_POST['can_carry_both_simul'] : '0')));
		$can_score_on_intake_side = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_score_on_intake_side']) ? $_POST['can_score_on_intake_side'] : '0')));
		$can_score_opp_intake_side = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_score_opp_intake_side']) ? $_POST['can_score_opp_intake_side'] : '0')));
		$can_score_perp_intake_side = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_score_perp_intake_side']) ? $_POST['can_score_perp_intake_side'] : '0')));
		$has_coral_tunnel = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['has_coral_tunnel']) ? $_POST['has_coral_tunnel'] : '0')));
		$can_manip_coral_angle = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_manip_coral_angle']) ? $_POST['can_manip_coral_angle'] : '0')));
		$can_extend_arm_in_motion = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['can_extend_arm_in_motion']) ? $_POST['can_extend_arm_in_motion'] : '0')));
		$coral_scoring_side_width_in = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['coral_scoring_side_width_in']) ? $_POST['coral_scoring_side_width_in'] : '0')));
		$coral_intake_side_width_in = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['coral_intake_side_width_in']) ? $_POST['coral_intake_side_width_in'] : '0')));
		$notes = mysqli_real_escape_string($link,stripslashes(trim(isset($_POST['notes']) ? $_POST['notes'] : '0')));

		$result = mysqli_query($link,"SELECT id FROM scout_pit_data_2025 WHERE team_id=" . $team_id);
		$row = mysqli_fetch_array($result);
		$match_row_id = $row["id"];
		if (mysqli_num_rows($result) == 0) {
			$query = "INSERT INTO scout_pit_data_2025(event_id,team_id,pass_number,robot_inspection_weight_lbs,robot_wheel_width_in,drive_id,robot_cg_id,can_score_proc,can_reef_intake_algae,can_ground_intake_algae,can_ground_intake_coral,can_station_intake_coral,can_remove_algae_l2,can_remove_algae_l3,can_carry_both_simul,can_score_on_intake_side,can_score_opp_intake_side,can_score_perp_intake_side,has_coral_tunnel,can_manip_coral_angle,can_extend_arm_in_motion,coral_scoring_side_width_in,coral_intake_side_width_in,notes,invalid) VALUES("
				. $event_id . ","
				. $team_id . ","
				. $pass_number . ","
				. $robot_inspection_weight_lbs . ","
				. $robot_wheel_width_in . ","
				. $drive_id . ","
				. $robot_cg_id . ","
				. $can_score_proc . ","
				. $can_reef_intake_algae . ","
				. $can_ground_intake_algae . ","
				. $can_ground_intake_coral . ","
				. $can_station_intake_coral . ","
				. $can_remove_algae_l2 . ","
				. $can_remove_algae_l3 . ","
				. $can_carry_both_simul . ","
				. $can_score_on_intake_side . ","
				. $can_score_opp_intake_side . ","
				. $can_score_perp_intake_side . ","
				. $has_coral_tunnel . ","
				. $can_manip_coral_angle . ","
				. $can_extend_arm_in_motion . ","
				. $coral_scoring_side_width_in . ","
				. $coral_intake_side_width_in . ","
				. "'" . $notes . "',"
				. "0);";
			$success = mysqli_query($link,$query);
		}
		else {
			$query = "UPDATE scout_pit_data_2025 SET "
				. "event_id=" . $event_id . ","
				. "team_id=" . $team_id . ","
				. "pass_number=" . $pass_number . ","
				. "robot_inspection_weight_lbs=" . $robot_inspection_weight_lbs . ","
				. "robot_wheel_width_in=" . $robot_wheel_width_in . ","
				. "drive_id=" . $drive_id . ","
				. "robot_cg_id=" . $robot_cg_id . ","
				. "can_score_proc=" . $can_score_proc . ","
				. "can_reef_intake_algae=" . $can_reef_intake_algae . ","
				. "can_ground_intake_algae=" . $can_ground_intake_algae . ","
				. "can_ground_intake_coral=" . $can_ground_intake_coral . ","
				. "can_station_intake_coral=" . $can_station_intake_coral . ","
				. "can_remove_algae_l2=" . $can_remove_algae_l2 . ","
				. "can_remove_algae_l3=" . $can_remove_algae_l3 . ","
				. "can_carry_both_simul=" . $can_carry_both_simul . ","
				. "can_score_on_intake_side=" . $can_score_on_intake_side . ","
				. "can_score_opp_intake_side=" . $can_score_opp_intake_side . ","
				. "can_score_perp_intake_side=" . $can_score_perp_intake_side . ","
				. "has_coral_tunnel=" . $has_coral_tunnel . ","
				. "can_manip_coral_angle=" . $can_manip_coral_angle . ","
				. "can_extend_arm_in_motion=" . $can_extend_arm_in_motion . ","
				. "coral_scoring_side_width_in=" . $coral_scoring_side_width_in . ","
				. "coral_intake_side_width_in=" . $coral_intake_side_width_in . ","
				. "notes='" . $notes . "',"
				. "invalid=0"
				. " WHERE id=" . $match_row_id;

			$success = mysqli_query($link,$query);
		}
		if ($success) {
			$result = mysqli_query($link,"SELECT id, timestamp FROM scout_pit_data_2025 WHERE team_id=" . $team_id);
			$row = mysqli_fetch_array($result);
			$resp = $row["id"] . "," . strtotime($row["timestamp"]);
		} else {
			$resp = 'Database Query Failed : \n' . $query;
		}
	}

	else {
		header('Content-Type: text/plain; charset=utf-8');
		$resp = 'invalid submission type';
	}

	echo $resp;
}
