package org.growingstems.scouting

import android.view.View
import android.widget.Spinner

class OnIncrementListener(private val spinner: Spinner, private val increment: Int) : View.OnClickListener {
    override fun onClick(v: View) {
        spinner.setSelection(spinner.selectedItemPosition + increment)
    }
}
