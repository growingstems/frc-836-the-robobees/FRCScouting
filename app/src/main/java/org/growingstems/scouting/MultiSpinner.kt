package org.growingstems.scouting

import android.content.Context
import android.content.DialogInterface
import android.content.res.Resources.Theme
import android.util.AttributeSet
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatSpinner
import java.util.Arrays

class MultiSpinner : AppCompatSpinner {

    interface OnItemsSelectedListener {
        fun onSelected(view: MultiSpinner, indices: Collection<Int>, strings: Collection<String>)
    }

    private val itemAdapter: ArrayAdapter<String> by lazy {
        ArrayAdapter(
            context,
            android.R.layout.simple_spinner_item
        )
    }
    private var listener: OnItemsSelectedListener? = null
    var items: Array<String> = emptyArray()
        private set

    private var lastSelection: BooleanArray = booleanArrayOf()
    var selection: BooleanArray = booleanArrayOf()
        private set

    var title: String = ""
    var hint: String = ""

    var selectAllEnabled: Boolean = true

    constructor(context: Context) : super(context) {
        super.setAdapter(itemAdapter)
    }

    constructor(context: Context, mode: Int) : super(context, mode) {
        super.setAdapter(itemAdapter)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        super.setAdapter(itemAdapter)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        super.setAdapter(itemAdapter)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, mode: Int) : super(
        context,
        attrs,
        defStyleAttr,
        mode
    ) {
        super.setAdapter(itemAdapter)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        mode: Int,
        popupTheme: Theme?
    ) : super(context, attrs, defStyleAttr, mode, popupTheme) {
        super.setAdapter(itemAdapter)
    }

    fun setListener(listener: OnItemsSelectedListener) {
        this.listener = listener
    }

    override fun performClick(): Boolean {
        if (items.size > 0) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMultiChoiceItems(
                items,
                selection
            ) { _: DialogInterface, i: Int, b: Boolean ->
                if (i < selection.size) {
                    selection[i] = b
                    updateAdapter()
                } else {
                    throw IllegalArgumentException("Selected Index is out of bounds")
                }
            }

            if (selectAllEnabled) {
                builder.setNeutralButton(R.string.select_all) { _: DialogInterface, _: Int ->
                    selectAll()
                }
            }

            builder.setPositiveButton(R.string.ok) { _: DialogInterface, _: Int ->
                listener?.onSelected(this, getSelectedIndices(), getSelectedStrings())
            }

            builder.setNegativeButton(R.string.cancel) { _: DialogInterface, _: Int ->
                lastSelection.copyInto(selection)
                updateAdapter()
            }

            selection.copyInto(lastSelection)
            builder.show()
            return true
        }
        return super.performClick()
    }

    override fun setAdapter(adapter: SpinnerAdapter?) {
        //Do nothing
    }

    fun selectAll() {
        Arrays.fill(selection, true)
        updateAdapter()
    }

    fun getSelectedStrings(): Collection<String> {
        val ret = ArrayList<String>()
        items.forEachIndexed { i, v ->
            if (selection[i]) {
                ret.add(v)
            }
        }
        return ret
    }

    fun getSelectedIndices(): Collection<Int> {
        val ret = ArrayList<Int>()
        selection.forEachIndexed { i, v ->
            if (v) {
                ret.add(i)
            }
        }
        return ret
    }

    fun setSelectedIndices(indices: Collection<Int>) {
        selection.fill(false)
        for (i in indices) {
            selection[i] = true
        }
        updateAdapter()
    }

    fun setItems(items: Collection<String>) {
        setItems(items.toTypedArray())
    }

    fun setItems(items: Array<String>) {
        this.items = items
        selection = BooleanArray(items.size) { _: Int -> false }
        lastSelection = BooleanArray(items.size) { _: Int -> false }
        updateAdapter()
    }

    private fun updateAdapter() {
        itemAdapter.clear()
        itemAdapter.add(getAdapterString())
    }

    private fun getAdapterString(): String {
        val builder = StringBuilder()
        var foundFirst = false
        items.forEachIndexed{ i, v ->
            if (selection[i]) {
                if (foundFirst) {
                    builder.append(", ")
                }
                foundFirst = true
                builder.append(v)
            }
        }
        return if (foundFirst) {
            builder.toString()
        } else {
            hint
        }
    }
}
