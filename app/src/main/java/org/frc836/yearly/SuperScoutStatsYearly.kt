/*
 * Copyright 2023 Daniel Logan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.frc836.yearly

import org.frc836.database.SuperScoutStats

/*
stats.practice_match
 stats.tippy
stats.notes
*/

object SuperScoutStatsYearly {

    @JvmStatic
    fun copyData(from: SuperScoutStats, to: SuperScoutStats) {
        to.algae_intake_speed_ground = from.algae_intake_speed_ground
        to.algae_intake_speed_reef = from.algae_intake_speed_reef
        to.algae_removal_speed = from.algae_removal_speed
        to.coral_intake_speed_station = from.coral_intake_speed_station
        to.coral_intake_speed_ground = from.coral_intake_speed_ground

        to.easily_defended = from.easily_defended
        to.l1_scoring_speed = from.l1_scoring_speed
        to.l2_l3_difference = from.l2_l3_difference
        to.l2_l3_scoring_speed = from.l2_l3_scoring_speed
        to.l4_scoring_speed = from.l4_scoring_speed
        to.net_scoring_speed = from.net_scoring_speed
        to.opportunistic_defense = from.opportunistic_defense
        to.processor_scoring_speed = from.processor_scoring_speed
        to.time_to_opp_algae = from.time_to_opp_algae
        to.robot_drive_speed = from.robot_drive_speed

        to.tippy = from.tippy
        to.notes = from.notes
    }

    @JvmStatic
    fun clearContents(stats: SuperScoutStats) {
        stats.algae_intake_speed_ground = 0
        stats.algae_intake_speed_reef = 0
        stats.algae_removal_speed = 0
        stats.coral_intake_speed_station = 0
        stats.coral_intake_speed_ground = 0

        stats.easily_defended = false
        stats.l1_scoring_speed = 0
        stats.l2_l3_difference = false
        stats.l2_l3_scoring_speed = 0
        stats.l4_scoring_speed = 0
        stats.net_scoring_speed = 0
        stats.opportunistic_defense = 0
        stats.processor_scoring_speed = 0
        stats.time_to_opp_algae = 0.0
        stats.robot_drive_speed = 0

        stats.tippy = 0
        stats.notes = ""
    }
}
