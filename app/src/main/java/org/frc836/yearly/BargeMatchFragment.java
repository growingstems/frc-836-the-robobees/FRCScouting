/*
 * Copyright 2016 Daniel Logan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.frc836.yearly;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;

import org.frc836.database.MatchStatsStruct;
import org.growingstems.scouting.MatchFragment;
import org.growingstems.scouting.Prefs;
import org.growingstems.scouting.R;


public class BargeMatchFragment extends MatchFragment {

    private final MatchStatsStruct tempData = new MatchStatsStruct();

    private Button deepB;
    private Button shallowB;
    private Button parkB;
    private Button buddyB;

    private int yellow;
    private int green;
    private final PorterDuff.Mode filterMode = PorterDuff.Mode.MULTIPLY;

    public BargeMatchFragment() {
        // Required empty public constructor
    }

    public static BargeMatchFragment newInstance() {
        return new BargeMatchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_match_barge, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        getBaseGUIRefs(view);
        setDisplayed(true);
        setListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData(tempData);
    }

    public void onPause() {
        super.onPause();
        saveData(tempData);
    }

    @Override
    public void saveData(@NonNull MatchStatsStruct data) {
        if (getView() == null || !getDisplayed())
            return;
        MatchStatsYearly.copyBarge(tempData, data);
    }

    @Override
    public void loadData(@NonNull MatchStatsStruct data) {
        tempData.copyFrom(data);
        View view = getView();
        if (view == null || !getDisplayed())
            return;
        updateSide(view);
        //setListeners();

        Activity act = getActivity();
        if (act == null)
            return;
        setDeep(data);
        setShallow(data);
        setPark(data);
        setBuddy(data);

    }

    private void updateSide(View view) {
        Activity act = getActivity();
        String pos;
        if (act instanceof MatchActivity)
            pos = ((MatchActivity) act).getPosition();
        else
            pos = Prefs.getPosition(act, "Red 1");
        if (pos.contains("Blue")) {
            //TODO set barge image color
        } else {
            //TODO set barge image color
        }
    }

    private void getBaseGUIRefs(View view) {
        green = ResourcesCompat.getColor(getResources(), R.color.halfgreen, null);
        yellow = ResourcesCompat.getColor(getResources(), R.color.halfyellow, null);
        deepB = view.findViewById(R.id.button_barge_deep);
        shallowB = view.findViewById(R.id.button_barge_shallow);
        parkB = view.findViewById(R.id.button_barge_park);
        buddyB = view.findViewById(R.id.button_barge_buddyclimb);
    }

    private void setListeners() {

        Activity act = getActivity();
        if (act == null)
            return;
        deepB.setOnClickListener(v -> {
            if (tempData.deep_climb_success) {
                tempData.deep_climb_success = false;
                tempData.deep_climb_unsuccessful_attempt = true;
            } else if (tempData.deep_climb_unsuccessful_attempt) {
                tempData.deep_climb_unsuccessful_attempt = false;
            } else {
                tempData.deep_climb_success = true;
            }
            setDeep(tempData);
        });
        shallowB.setOnClickListener(v -> {
            if (tempData.shallow_climb_success) {
                tempData.shallow_climb_success = false;
                tempData.shallow_climb_unsuccessful_attempt = true;
            } else if (tempData.shallow_climb_unsuccessful_attempt) {
                tempData.shallow_climb_unsuccessful_attempt = false;
            } else {
                tempData.shallow_climb_success = true;
            }
            setShallow(tempData);
        });
        parkB.setOnClickListener(v -> {
            tempData.barge_park = !tempData.barge_park;
            setPark(tempData);
        });
        buddyB.setOnClickListener(v -> {
            tempData.buddy_climb = !tempData.buddy_climb;
            setBuddy(tempData);
        });

    }

    private void setDeep(MatchStatsStruct stats) {
        if (stats.deep_climb_success) {
            deepB.getBackground().setColorFilter(green, filterMode);
        } else if (stats.deep_climb_unsuccessful_attempt) {
            deepB.getBackground().setColorFilter(yellow, filterMode);
        } else {
            deepB.getBackground().clearColorFilter();
        }
    }

    private void setShallow(MatchStatsStruct stats) {
        if (stats.shallow_climb_success) {
            shallowB.getBackground().setColorFilter(green, filterMode);
        } else if (stats.shallow_climb_unsuccessful_attempt) {
            shallowB.getBackground().setColorFilter(yellow, filterMode);
        } else {
            shallowB.getBackground().clearColorFilter();
        }
    }

    private void setPark(MatchStatsStruct stats) {
        if (stats.barge_park) {
            parkB.getBackground().setColorFilter(green, filterMode);
        } else {
            parkB.getBackground().clearColorFilter();
        }
    }

    private void setBuddy(MatchStatsStruct stats) {
        if (stats.buddy_climb) {
            buddyB.getBackground().setColorFilter(green, filterMode);
        } else {
            buddyB.getBackground().clearColorFilter();
        }
    }

}
