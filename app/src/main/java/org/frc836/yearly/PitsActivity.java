package org.frc836.yearly;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;

import org.frc836.database.DBActivity;
import org.frc836.database.PitStats;
import org.growingstems.scouting.Prefs;
import org.growingstems.scouting.R;

import java.util.ArrayList;
import java.util.List;

public class PitsActivity extends DBActivity {

    private String HELPMESSAGE;
    private static final String defaultEvent = "CHS District Oxon Hill MD Event";

    private PitStats stats;

    private AutoCompleteTextView teamT;
    private TextView passNumT;
    private TextView teamInfoT;

    private EditText weightT;
    private EditText wheelwidthT;
    private EditText intakeSideWidthT;
    private EditText scoringSideWidthT;
    private Spinner driveTypeS;
    private ArrayAdapter<String> driveTypeAdapter;
    private Spinner cgS;
    private ArrayAdapter<String> cgAdapter;

    private CheckBox coralgroundAcqC;
    private CheckBox coralStationAcqC;
    private CheckBox algaeGroundAcqC;
    private CheckBox algaeReefAcqC;
    private CheckBox l2AlgaeRemoveC;
    private CheckBox l3AlgaeRemoveC;

    private CheckBox intakeScoreC;
    private CheckBox oppScoreC;
    private CheckBox perpScoreC;

    private CheckBox procScoreC;
    private CheckBox gpControlC;
    private CheckBox coralTunnelC;
    private CheckBox coralAngleC;
    private CheckBox extendMotionC;

    private EditText commentsT;

    private final Handler timer = new Handler(Looper.getMainLooper());
    private static final int DELAY = 500;

    private final List<TeamNumTask> tasks = new ArrayList<>(3);
    private final ActivityResultLauncher<Intent> resultForPrefs = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pits);

        HELPMESSAGE = """
            Enter the requested information about each team.

            When a team number is entered, the last time that data was collected about this team
            for this event and the configured pass will be shown""";

        stats = new PitStats();
        teamT = findViewById(R.id.pits_teamT);
        teamT.setThreshold(1);
        passNumT = findViewById(R.id.pits_passT);
        //TODO set pass number
        passNumT.setText("0");
        teamInfoT = findViewById(R.id.pits_teamInfo);

        weightT = findViewById(R.id.robot_weightT);
        wheelwidthT = findViewById(R.id.robot_wheelWidthT);
        intakeSideWidthT = findViewById(R.id.robot_intakeSideWidthT);
        scoringSideWidthT = findViewById(R.id.robot_scoringSideWidthT);
        driveTypeS = findViewById(R.id.pitDriveS);
        driveTypeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        driveTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cgS = findViewById(R.id.pitCgS);
        cgAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        cgAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        coralgroundAcqC = findViewById(R.id.coralGroundIntakeC);
        coralStationAcqC = findViewById(R.id.coralStationIntakeC);
        algaeGroundAcqC = findViewById(R.id.algaeGroundIntakeC);
        algaeReefAcqC = findViewById(R.id.algaeReefIntakeC);
        l2AlgaeRemoveC = findViewById(R.id.algaeL2RemovalC);
        l3AlgaeRemoveC = findViewById(R.id.algaeL3RemovalC);

        intakeScoreC = findViewById(R.id.intakeSideC);
        oppScoreC = findViewById(R.id.oppositeIntakeC);
        perpScoreC = findViewById(R.id.perpendicularSideC);

        procScoreC = findViewById(R.id.pitCanScoreProc);
        gpControlC = findViewById(R.id.carriesBothC);
        coralTunnelC = findViewById(R.id.coralTunnelC);
        coralAngleC = findViewById(R.id.coralAngleManipC);
        extendMotionC = findViewById(R.id.armExtendMotionC);

        commentsT = findViewById(R.id.pits_commentsT);
        Button submitB = findViewById(R.id.pits_submitB);
        teamT.addTextChangedListener(new teamTextListener());
        submitB.setOnClickListener(new SubmitListener());

        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                onBack();
            }
        });
    }

    public void onResume() {
        super.onResume();

        List<String> drives = db.getDriveList();
        if (drives != null) {
            driveTypeAdapter.clear();
            driveTypeAdapter.addAll(drives);
            driveTypeS.setAdapter(driveTypeAdapter);
        }

        List<String> cg = db.getCgList();
        if (cg != null) {
            cgAdapter.clear();
            cgAdapter.addAll(cg);
            cgS.setAdapter(cgAdapter);
        }

        for (TeamNumTask task : tasks) {
            timer.removeCallbacks(task);
        }
        tasks.clear();
        setTeamList(db.getTeamsWithData());
    }

    public void onBack() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Cancel Data Entry?\nChanges will not be saved.")
            .setCancelable(false)
            .setPositiveButton("Yes",
                (dialog, id) -> PitsActivity.this.finish())
            .setNegativeButton("No",
                (dialog, id) -> dialog.cancel());
        builder.show();
    }

    @NonNull
    @Override
    public String getHelpMessage() {
        return HELPMESSAGE;
    }

    @NonNull
    @Override
    public ActivityResultLauncher<Intent> getResultForPrefs() {
        return resultForPrefs;
    }

    private class SubmitListener implements View.OnClickListener {

        public void onClick(View v) {
            submit();
        }

    }

    protected void submit() {
        String tstr; // to avoid re-calc for string -> int check operations

        tstr = teamT.getText().toString().trim();
        if (!tstr.isEmpty())
            stats.team_id = Integer.parseInt(tstr);
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage(
                    "No team number entered, please enter a team number")
                .setCancelable(true)
                .setPositiveButton("OK",
                    (dialog, which) -> {
                        dialog.cancel();
                        teamT.requestFocus();
                    });
            builder.show();
            return;
        }

        stats.event_id = Prefs.getEvent(this, defaultEvent);
        //TODO pass num
        stats.pass_number = 0;

        tstr = weightT.getText().toString().trim();
        if (!tstr.isEmpty())
            stats.robot_inspection_weight_lbs = Integer.parseInt(tstr);
        else
            stats.robot_inspection_weight_lbs = 0;

        tstr = wheelwidthT.getText().toString().trim();
        if (!tstr.isEmpty())
            stats.robot_wheel_width_in = Integer.parseInt(tstr);
        else
            stats.robot_wheel_width_in = 0;

        tstr = intakeSideWidthT.getText().toString().trim();
        if (!tstr.isEmpty())
            stats.coral_intake_side_width_in = Integer.parseInt(tstr);
        else
            stats.coral_intake_side_width_in = 0;

        tstr = scoringSideWidthT.getText().toString().trim();
        if (!tstr.isEmpty())
            stats.coral_scoring_side_width_in = Integer.parseInt(tstr);
        else
            stats.coral_scoring_side_width_in = 0;

        stats.drive_id = driveTypeS.getSelectedItem().toString();
        stats.robot_cg_id = cgS.getSelectedItem().toString();

        stats.can_ground_intake_coral = coralgroundAcqC.isChecked();
        stats.can_station_intake_coral = coralStationAcqC.isChecked();
        stats.can_ground_intake_algae = algaeGroundAcqC.isChecked();
        stats.can_reef_intake_algae = algaeReefAcqC.isChecked();
        stats.can_remove_algae_l2 = l2AlgaeRemoveC.isChecked();
        stats.can_remove_algae_l3 = l3AlgaeRemoveC.isChecked();

        stats.can_score_on_intake_side = intakeScoreC.isChecked();
        stats.can_score_opp_intake_side = oppScoreC.isChecked();
        stats.can_score_perp_intake_side = perpScoreC.isChecked();

        stats.can_score_proc = procScoreC.isChecked();
        stats.can_carry_both_simul = gpControlC.isChecked();
        stats.has_coral_tunnel = coralTunnelC.isChecked();
        stats.can_manip_coral_angle = coralAngleC.isChecked();
        stats.can_extend_arm_in_motion = extendMotionC.isChecked();

        stats.notes = commentsT.getText().toString();

        if (db.submitPits(stats))
            clear();
        else
            Toast.makeText(getApplicationContext(), "Error in local database",
                Toast.LENGTH_LONG).show();

        setTeamList(db.getTeamsWithData(Prefs.getEvent(this, defaultEvent)));
    }

    protected void clear() {
        clearData();
        teamT.setText("");
    }

    protected void clearData() {
        teamInfoT.setText("");
        commentsT.setText("");

        weightT.setText("");
        wheelwidthT.setText("");
        intakeSideWidthT.setText("");
        scoringSideWidthT.setText("");
        driveTypeS.setSelection(0);
        cgS.setSelection(0);

        coralgroundAcqC.setChecked(false);
        coralStationAcqC.setChecked(false);
        algaeGroundAcqC.setChecked(false);
        algaeReefAcqC.setChecked(false);
        l2AlgaeRemoveC.setChecked(false);
        l3AlgaeRemoveC.setChecked(false);

        intakeScoreC.setChecked(false);
        oppScoreC.setChecked(false);
        perpScoreC.setChecked(false);

        procScoreC.setChecked(false);
        gpControlC.setChecked(false);
        coralTunnelC.setChecked(false);
        coralAngleC.setChecked(false);
        extendMotionC.setChecked(false);
    }

    private void clearDataDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PitsActivity.this);
        builder.setMessage("You had already entered data. Clear form?")
            .setCancelable(false)
            .setPositiveButton("Yes",
                (dialog, id) -> {
                    teamInfoT.setText("");
                    clearData();
                })
            .setNegativeButton("No",
                (dialog, id) -> dialog.cancel());
        builder.show();
    }

    private class teamTextListener implements TextWatcher {

        public void afterTextChanged(Editable s) {
            if (s.length() > 0) {
                TeamNumTask task = new TeamNumTask();
                tasks.add(task);
                task.teamNum = Integer.parseInt(s.toString());
                timer.postDelayed(task, DELAY);
            } else {
                if (!dataClear()) {
                    clearDataDialog();
                } else
                    clearData();
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }

    }

    private PitStats teamLoad = new PitStats();
    private String dateLoad = "";

    private void setTeam(int teamNum) {
        //TODO pass numbers
        Pair<String, PitStats> pitInfo = db.getTeamPitInfo(String.valueOf(teamNum), Prefs.getEvent(this, defaultEvent), 0);
        String date = pitInfo.first;
        PitStats stats = pitInfo.second;
        if (!date.isEmpty()) {
            if (dataClear()) {
                teamInfoT.setText(getString(R.string.last_updated, date.trim()));
                populateData(stats);
            } else {
                teamLoad = stats;
                dateLoad = date;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(
                        "Data for this team exists. Overwrite current form?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                        (dialog, id) -> {
                            teamInfoT.setText(getString(R.string.last_updated, dateLoad.trim()));
                            populateData(teamLoad);
                        })
                    .setNegativeButton("No",
                        (dialog, id) -> dialog.cancel());
                builder.show();
            }
        } else {
            if (dataClear()) {
                teamInfoT.setText("");
                clearData();
            } else
                clearDataDialog();
        }
    }

    private class TeamNumTask implements Runnable {
        int teamNum;

        public void run() {
            if (!teamT.getText().toString().isEmpty()
                && Integer.parseInt(teamT.getText().toString()) == teamNum) {
                setTeam(teamNum);
            }
        }
    }

    public void populateData(PitStats stats) {

        commentsT.setText(stats.notes);

        weightT.setText(String.valueOf(stats.robot_inspection_weight_lbs));
        wheelwidthT.setText(String.valueOf(stats.robot_wheel_width_in));
        intakeSideWidthT.setText(String.valueOf(stats.coral_intake_side_width_in));
        scoringSideWidthT.setText(String.valueOf(stats.coral_scoring_side_width_in));

        driveTypeS.setSelection(driveTypeAdapter.getPosition(stats.drive_id));
        cgS.setSelection(cgAdapter.getPosition(stats.robot_cg_id));

        coralgroundAcqC.setChecked(stats.can_ground_intake_coral);
        coralStationAcqC.setChecked(stats.can_station_intake_coral);
        algaeGroundAcqC.setChecked(stats.can_ground_intake_algae);
        algaeReefAcqC.setChecked(stats.can_reef_intake_algae);
        l2AlgaeRemoveC.setChecked(stats.can_remove_algae_l2);
        l3AlgaeRemoveC.setChecked(stats.can_remove_algae_l3);

        intakeScoreC.setChecked(stats.can_score_on_intake_side);
        oppScoreC.setChecked(stats.can_score_opp_intake_side);
        perpScoreC.setChecked(stats.can_score_perp_intake_side);

        procScoreC.setChecked(stats.can_score_proc);
        gpControlC.setChecked(stats.can_carry_both_simul);
        coralTunnelC.setChecked(stats.has_coral_tunnel);
        coralAngleC.setChecked(stats.can_manip_coral_angle);
        extendMotionC.setChecked(stats.can_extend_arm_in_motion);

    }

    private boolean dataClear() {
        return !(!commentsT.getText().toString().isEmpty()
            || !weightT.getText().toString().isEmpty()
            || !intakeSideWidthT.getText().toString().isEmpty()
            || !scoringSideWidthT.getText().toString().isEmpty()
            || driveTypeS.getSelectedItemPosition() != 0
            || cgS.getSelectedItemPosition() != 0
            || coralgroundAcqC.isChecked()
            || coralStationAcqC.isChecked()
            || algaeGroundAcqC.isChecked()
            || algaeReefAcqC.isChecked()
            || l2AlgaeRemoveC.isChecked()
            || l3AlgaeRemoveC.isChecked()
            || intakeScoreC.isChecked()
            || oppScoreC.isChecked()
            || perpScoreC.isChecked()
            || procScoreC.isChecked()
            || gpControlC.isChecked()
            || coralTunnelC.isChecked()
            || coralAngleC.isChecked()
            || extendMotionC.isChecked()
        );
    }

    private void setTeamList(List<String> teams) {
        if (teams == null || teams.isEmpty())
            return;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
            android.R.layout.simple_dropdown_item_1line, teams);

        teamT.setAdapter(adapter);
    }
}
