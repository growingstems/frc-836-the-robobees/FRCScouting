/*
 * Copyright 2015 Daniel Logan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.frc836.yearly;


import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import org.frc836.database.DBActivity;
import org.frc836.database.MatchStatsStruct;
import org.growingstems.scouting.MainMenuSelection;
import org.growingstems.scouting.MatchFragment;
import org.growingstems.scouting.Prefs;
import org.growingstems.scouting.R;

import java.util.List;


public class MatchActivity extends DBActivity {

    public static final int NUM_SCREENS = 6;
    public static final int PRE_SCREEN = 0;
    public static final int AUTO_SCREEN = 1;
    public static final int TELE_SCREEN = 2;
    public static final int DEF_SCREEN = 3;
    public static final int BARGE_SCREEN = 4;
    public static final int END_SCREEN = 5;
    private MatchViewAdapter mMatchViewAdapter;

    private ViewPager2 mViewPager;

    private String HELPMESSAGE;

    private MatchStatsStruct teamData;

    private EditText teamText;
    private EditText matchT;
    private TextView posT;

    private Button lastB;
    private Button nextB;
    private Button centerB;

    private int mCurrentPage;
    private boolean readOnly = false;
    private String event = null;
    private boolean prac = false;
    private String position = null;

    private boolean defPage = false;

    private final Handler timer = new Handler();
    private static final int DELAY = 16000;

    private final ActivityResultLauncher<Intent> resultForPrefs = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
    });

    private final Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            AlertDialog.Builder builder = new AlertDialog.Builder(MatchActivity.this);
            builder.setMessage("Continue to Tele-Op?")
                .setCancelable(true)
                .setPositiveButton("Yes",
                    (dialog, id) -> {
                        if (mCurrentPage == AUTO_SCREEN)
                            onNext(nextB);
                    })
                .setNegativeButton("No",
                    (dialog, id) -> {
                        timer.removeCallbacks(mUpdateTimeTask);
                        if (!readOnly && mCurrentPage == AUTO_SCREEN)
                            timer.postDelayed(mUpdateTimeTask, DELAY);
                        dialog.cancel();
                    });
            builder.show();
        }
    };

    private static final String defaultEvent = "CHS District Oxon Hill MD Event";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.match);

        //TODO update Help Message
        HELPMESSAGE = "Record Match Data here.\n" +
            "Input where game pieces are gathered during the autonomous period\n" +
            "along with speaker, amp, dropped game pieces and fouls during both periods.\n" +
            "On the Stage screen, record scores and misses for the Trap, as well as which level was achieved/attempted" +
            "On the end-game screen yellow/red flags\n" +
            "If the robot played a lot of defense, check the \"Defense Bot\" button" +
            "If the robot was a no-show, select the appropriate button for if a rep was present or not";

        getGUIRefs();

        Intent intent = getIntent();
        teamText.setText(intent.getStringExtra("team"));
        matchT.setText(intent.getStringExtra("match"));
        readOnly = intent.getBooleanExtra("readOnly", false);
        event = intent.getStringExtra("event");
        prac = intent.getBooleanExtra("practice", false);
        position = intent.getStringExtra("position");

        mMatchViewAdapter = new MatchViewAdapter(getSupportFragmentManager(), getLifecycle());
        mCurrentPage = PRE_SCREEN;

        mViewPager = findViewById(R.id.matchPager);
        mViewPager.setAdapter(mMatchViewAdapter);
        mViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                pageSelected(position);
            }
        });

        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                onBack(null);
            }
        });

        if (!readOnly)
            posT.setOnClickListener(new positionClickListener());

        loadData();
        loadPreMatch();
    }

    protected void onResume() {
        super.onResume();

        teamData.event_id = event == null ? Prefs.getEvent(getApplicationContext(), defaultEvent) : event;

        teamData.practice_match = readOnly ? prac : Prefs.getPracticeMatch(getApplicationContext(), false);

        updatePosition();

        if (mCurrentPage == AUTO_SCREEN && !readOnly) {
            timer.removeCallbacks(mUpdateTimeTask);
            timer.postDelayed(mUpdateTimeTask, DELAY);
        }
    }

    protected void onPause() {
        super.onPause();
        timer.removeCallbacks(mUpdateTimeTask);
    }

    public List<String> getNotesOptions() {
        return db.getNotesOptions();
    }

    public List<String> getTeamNotes() {
        return db.getNotesForTeam(teamData.team_id);
    }

    private void updatePosition() {
        String pos = position == null ? Prefs.getPosition(getApplicationContext(), "Red 1") : position;
        posT.setText(pos);
        if (pos.contains("Blue")) {
            posT.setTextColor(Color.BLUE);
        } else {
            posT.setTextColor(Color.RED);
        }
    }

    @Override
    public String getHelpMessage() {
        return HELPMESSAGE;
    }

    @NonNull
    @Override
    public ActivityResultLauncher<Intent> getResultForPrefs() {
        return resultForPrefs;
    }

    private class positionClickListener implements View.OnClickListener {
        public void onClick(View v) {
            MainMenuSelection.openSettings(MatchActivity.this);
        }
    }

    private void loadData() {
        Editable teamE = teamText.getText();
        Editable matchE = matchT.getText();

        boolean loadData = false;
        if (teamE != null && teamE.length() > 0 && matchE != null
            && matchE.length() > 0) {
            String team = teamE.toString();
            String match = matchE.toString();
            teamData = db.getMatchStats(event == null ? Prefs.getEvent(getApplicationContext(), defaultEvent) : event, Integer
                .parseInt(match), Integer.parseInt(team), readOnly ? prac : Prefs.getPracticeMatch(getApplicationContext(), false));
            if (teamData == null)
                teamData = new MatchStatsStruct(Integer.parseInt(team),
                    event == null ? Prefs.getEvent(getApplicationContext(), defaultEvent) : event, Integer.parseInt(match),
                    readOnly ? prac : Prefs.getPracticeMatch(getApplicationContext(), false));
            else
                loadData = true;
        } else
            teamData = new MatchStatsStruct();

        if (loadData && !readOnly) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Data for this match exists.\nLoad old match?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                    (dialog, id) -> dialog.cancel())
                .setNegativeButton("No",
                    (dialog, id) -> {
                        if (teamText.getText().toString().length() > 0
                            && matchT.getText().toString()
                            .length() > 0) {
                            teamData = new MatchStatsStruct(
                                Integer.parseInt(teamText
                                    .getText().toString()),
                                event == null ? Prefs.getEvent(getApplicationContext(), defaultEvent) : event,
                                Integer.parseInt(matchT
                                    .getText().toString()));
                        } else
                            teamData = new MatchStatsStruct();
                        loadAll();
                    });
            builder.show();
        }
        mViewPager.setCurrentItem(0, true);
        loadAll();
        lastB.setText(getString(R.string.match_change_button_cancel));
        nextB.setText(getString(R.string.match_change_button_auto));
    }

    public void pageSelected(int page) {
        switch (mCurrentPage) {
            case PRE_SCREEN:
                savePreMatch();
                break;
            case AUTO_SCREEN:
                saveAuto();
                break;
            case TELE_SCREEN:
                saveTele();
                break;
            case DEF_SCREEN:
                saveDef();
                break;
            case BARGE_SCREEN:
                saveBarge();
                break;
            case END_SCREEN:
                saveEnd();
                break;
        }
        mCurrentPage = page;
        switch (page) {
            case PRE_SCREEN:
                loadPreMatch();
                lastB.setText(getString(R.string.match_change_button_cancel));
                nextB.setText(getString(R.string.match_change_button_auto));
                timer.removeCallbacks(mUpdateTimeTask);
                centerB.setVisibility(View.INVISIBLE);
                centerB.setEnabled(false);
                break;
            case AUTO_SCREEN:
                loadAuto();
                lastB.setText(getString(R.string.match_change_button_prematch));
                nextB.setText(getString(R.string.match_change_button_tele));
                if (!readOnly) {
                    timer.removeCallbacks(mUpdateTimeTask);
                    timer.postDelayed(mUpdateTimeTask, DELAY);
                }
                centerB.setVisibility(View.INVISIBLE);
                centerB.setEnabled(false);
                break;
            case TELE_SCREEN:
                loadTele();
                centerB.setText(getString(R.string.tele_button_defense));
                lastB.setText(getString(R.string.match_change_button_auto));
                nextB.setText(getString(R.string.match_change_button_barge));
                timer.removeCallbacks(mUpdateTimeTask);
                centerB.setVisibility(View.VISIBLE);
                centerB.setEnabled(true);
                break;
            case DEF_SCREEN:
                loadDef();
                centerB.setText(getString(R.string.tele_button_scoring));
                lastB.setText(getString(R.string.match_change_button_auto));
                nextB.setText(getString(R.string.match_change_button_barge));
                timer.removeCallbacks(mUpdateTimeTask);
                centerB.setVisibility(View.VISIBLE);
                centerB.setEnabled(true);
                break;
            case BARGE_SCREEN:
                loadBarge();
                lastB.setText(getString(R.string.match_change_button_tele));
                nextB.setText(getString(R.string.match_change_button_end));
                timer.removeCallbacks(mUpdateTimeTask);
                centerB.setVisibility(View.INVISIBLE);
                centerB.setEnabled(false);
                break;
            case END_SCREEN:
                loadEnd();
                lastB.setText(getString(R.string.match_change_button_barge));
                nextB.setText(readOnly ? getString(R.string.match_change_button_cancel) : getString(R.string.match_change_button_submit));
                timer.removeCallbacks(mUpdateTimeTask);
                centerB.setVisibility(View.INVISIBLE);
                centerB.setEnabled(false);
                break;
            default:
                loadAll();
                lastB.setText(getString(R.string.match_change_button_cancel));
                nextB.setText(getString(R.string.match_change_button_auto));
                timer.removeCallbacks(mUpdateTimeTask);
        }
    }

    public void onBack(View v) {
        if (mCurrentPage == 0 || mCurrentPage >= NUM_SCREENS) {
            if (!readOnly) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(
                        "Cancel Match Entry?\nChanges will not be saved.")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                        (dialog, id) -> MatchActivity.this.finish())
                    .setNegativeButton("No",
                        (dialog, id) -> dialog.cancel());
                builder.show();
            } else
                finish();
        }
        int new_page = mCurrentPage;
        if (mCurrentPage == DEF_SCREEN) {
            new_page = TELE_SCREEN - 1;
        } else {
            new_page -= 1;
            if (new_page == DEF_SCREEN && !defPage) {
                new_page = TELE_SCREEN;
            }
        }
        mViewPager.setCurrentItem(new_page, true);
    }

    public void onNext(View v) {
        if (mCurrentPage < 0 || mCurrentPage == (NUM_SCREENS - 1)) {
            saveAll();
            submit();
            return;
        }
        int new_page = mCurrentPage;
        if (mCurrentPage == TELE_SCREEN) {
            new_page = DEF_SCREEN + 1;
        } else {
            new_page += 1;
            if (new_page == TELE_SCREEN && defPage) {
                new_page = DEF_SCREEN;
            }
        }
        mViewPager.setCurrentItem(new_page, true);
    }

    public void onCenter(View v) {
        if (mCurrentPage == TELE_SCREEN) {
            defPage = true;
            mViewPager.setCurrentItem(DEF_SCREEN);
        } else if (mCurrentPage == DEF_SCREEN) {
            defPage = false;
            mViewPager.setCurrentItem(TELE_SCREEN);
        }
    }

    private class MatchViewAdapter extends FragmentStateAdapter {

        SparseArray<MatchFragment> fragments;

        MatchViewAdapter(FragmentManager fm, Lifecycle lc) {
            super(fm, lc);
            fragments = new SparseArray<>(NUM_SCREENS);
        }

        MatchFragment getMatchFragment(int i) {
            MatchFragment fragment;

            if (fragments.get(i) != null) {
                return fragments.get(i);
            }
            switch (i) {
                case PRE_SCREEN:
                    fragment = PreMatchFragment.newInstance();
                    fragments.put(i, fragment);
                    return fragment;
                case AUTO_SCREEN:
                    fragment = AutoMatchFragment.newInstance();
                    fragments.put(i, fragment);
                    return fragment;
                case TELE_SCREEN:
                    fragment = TeleMatchFragment.newInstance();
                    fragments.put(i, fragment);
                    return fragment;
                case DEF_SCREEN:
                    fragment = DefMatchFragment.newInstance();
                    fragments.put(i, fragment);
                    return fragment;
                case BARGE_SCREEN:
                    fragment = BargeMatchFragment.newInstance();
                    fragments.put(i, fragment);
                    return fragment;
                case END_SCREEN:
                default:
                    fragment = EndMatchFragment.newInstance();
                    fragments.put(i, fragment);
                    return fragment;
            }
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            return getMatchFragment(position);
        }

        @Override
        public int getItemCount() {
            return NUM_SCREENS;
        }
    }

    private void getGUIRefs() {
        teamText = findViewById(R.id.teamNum);

        matchT = findViewById(R.id.matchNum);
        posT = findViewById(R.id.pos);

        lastB = findViewById(R.id.backB);
        nextB = findViewById(R.id.nextB);
        centerB = findViewById(R.id.centerB);
    }

    private void loadPreMatch() {
        mMatchViewAdapter.getMatchFragment(PRE_SCREEN).loadData(teamData);
    }

    private void savePreMatch() {
        saveTeamInfo();
        mMatchViewAdapter.getMatchFragment(PRE_SCREEN).saveData(teamData);
    }

    private void loadAuto() {
        mMatchViewAdapter.getMatchFragment(AUTO_SCREEN).loadData(teamData);
    }

    private void saveAuto() {
        saveTeamInfo();
        mMatchViewAdapter.getMatchFragment(AUTO_SCREEN).saveData(teamData);
    }

    private void loadTele() {
        mMatchViewAdapter.getMatchFragment(TELE_SCREEN).loadData(teamData);
    }

    private void saveTele() {
        saveTeamInfo();
        mMatchViewAdapter.getMatchFragment(TELE_SCREEN).saveData(teamData);
    }

    private void loadDef() {
        mMatchViewAdapter.getMatchFragment(DEF_SCREEN).loadData(teamData);
    }

    private void saveDef() {
        saveTeamInfo();
        mMatchViewAdapter.getMatchFragment(DEF_SCREEN).saveData(teamData);
    }

    private void loadBarge() {
        mMatchViewAdapter.getMatchFragment(BARGE_SCREEN).loadData(teamData);
    }

    private void saveBarge() {
        saveTeamInfo();
        mMatchViewAdapter.getMatchFragment(BARGE_SCREEN).saveData(teamData);
    }

    private void loadEnd() {
        mMatchViewAdapter.getMatchFragment(END_SCREEN).loadData(teamData);
    }

    private void saveEnd() {
        saveTeamInfo();
        mMatchViewAdapter.getMatchFragment(END_SCREEN).saveData(teamData);
    }

    private void loadAll() {
        loadPreMatch();
        loadAuto();
        loadTele();
        loadDef();
        loadBarge();
        loadEnd();
    }

    private void saveAll() {
        savePreMatch();
        saveAuto();
        saveTele();
        saveDef();
        saveBarge();
        saveEnd();
    }

    private void saveTeamInfo() {
        Editable team = teamText.getText();
        if (team != null && team.length() > 0)
            teamData.team_id = Integer.parseInt(team.toString());
        Editable match = matchT.getText();
        if (match != null && match.length() > 0) {
            teamData.match_id = Integer.parseInt(match.toString());
        }
        teamData.position_id = posT.getText().toString();
    }

    private void submit() {
        if (!readOnly) {
            saveEnd();
            if (teamData.match_id > 0 && teamData.team_id > 0) {
                db.submitMatch(teamData);
                nextB.setEnabled(false);
                if (matchT.getText().length() > 0)
                    setResult(Integer.parseInt(matchT.getText().toString()) + 1);
            } else if (teamData.match_id <= 0) {
                Toast.makeText(this, "Please enter a match number", Toast.LENGTH_SHORT).show();
                return;
            } else {
                Toast.makeText(this, "Please enter a team number", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        finish();
    }

    public String getPosition() {
        return position == null ? Prefs.getPosition(getApplicationContext(), "Red 1") : position;
    }


}
