/*
 * Copyright 2016 Daniel Logan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.frc836.yearly;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;

import org.frc836.database.DB;
import org.frc836.database.MatchStatsStruct;
import org.growingstems.scouting.MatchFragment;
import org.growingstems.scouting.MultiSpinner;
import org.growingstems.scouting.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.SortedMap;


public class EndMatchFragment extends MatchFragment {

    private final MatchStatsStruct tempData = new MatchStatsStruct();

    private Spinner commonNotes;

    private Spinner pastNotes;

    private EditText teamNotes;

    private Button cardsB;
    private Button penaltyB;

    private MultiSpinner degradeS;


    private int red;
    private int yellow;
    private int green;

    private final PorterDuff.Mode filterMode = PorterDuff.Mode.MULTIPLY;

    public EndMatchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EndMatch.
     */
    public static EndMatchFragment newInstance() {
        return new EndMatchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_match_end, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        setDisplayed(true);
        red = ResourcesCompat.getColor(getResources(), R.color.halfred, null);
        yellow = ResourcesCompat.getColor(getResources(), R.color.halfyellow, null);
        green = ResourcesCompat.getColor(getResources(), R.color.halfgreen, null);
        commonNotes = view.findViewById(R.id.commonNotes);
        teamNotes = view.findViewById(R.id.notes);
        pastNotes = view.findViewById(R.id.previousNotes);
        commonNotes.setOnItemSelectedListener(new NotesSelectedListener());
        pastNotes.setOnItemSelectedListener(new NotesSelectedListener());

        cardsB = view.findViewById(R.id.button_end_cards);
        cardsB.setOnClickListener(new CardsClickListener());

        penaltyB = view.findViewById(R.id.button_end_penalties);
        penaltyB.setOnClickListener(v -> {
            tempData.causes_penalties = !tempData.causes_penalties;
            setPenalty(tempData);
        });

        degradeS = view.findViewById(R.id.spinner_degrade_state);
        degradeS.setHint("Degrade States");
        degradeS.setTitle("Select reasons for degraded state");
        degradeS.setSelectAllEnabled(false);

    }


    public void onResume() {
        super.onResume();

        Activity a = getActivity();

        if (a instanceof MatchActivity match) {
            List<String> options = match.getNotesOptions();
            List<String> teamOptions = match.getTeamNotes();
            SortedMap<Integer, String> degradeMap = DB.getDegradeList();

            if (options == null)
                options = new ArrayList<>(1);

            if (teamOptions == null)
                teamOptions = new ArrayList<>(1);

            options.add(0, commonNotes.getItemAtPosition(0).toString());
            teamOptions.add(0, pastNotes.getItemAtPosition(0).toString());

            ArrayAdapter<String> adapter = new ArrayAdapter<>(match,
                android.R.layout.simple_spinner_item, options);

            ArrayAdapter<String> adapterTeam = new ArrayAdapter<>(match,
                android.R.layout.simple_spinner_item, teamOptions);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            adapterTeam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            commonNotes.setAdapter(adapter);
            pastNotes.setAdapter(adapterTeam);

            assert degradeMap != null;
            degradeS.setItems(degradeMap.values());
        }

        loadData(tempData);
    }

    public void onPause() {
        super.onPause();

        saveData(tempData);
    }

    @Override
    public void saveData(@NonNull MatchStatsStruct data) {
        if (getView() == null || !getDisplayed())
            return;
        data.notes = ((EditText) getView().findViewById(R.id.notes)).getText().toString();
        data.red_card = tempData.red_card;
        data.yellow_card = tempData.yellow_card;
        switch (((Spinner) getView().findViewById(R.id.spinner_end_show)).getSelectedItemPosition()) {
            case 1:
                data.no_show = false;
                data.no_show_w_rep = true;
                break;
            case 2:
                data.no_show = true;
                data.no_show_w_rep = true;
                break;
            case 0:
            default:
                data.no_show = false;
                data.no_show_w_rep = false;
                break;
        }

        data.degrade_causes_id = getDegradeInt();
        data.degraded_performance = data.degrade_causes_id != 0;
        data.causes_penalties = tempData.causes_penalties;
    }

    @Override
    public void loadData(@NonNull MatchStatsStruct data) {
        tempData.copyFrom(data);

        if (getView() == null || !getDisplayed())
            return;

        //// which side are we using
        //boolean redLeft = Prefs.getRedLeft(getActivity(), true);

        //Activity act = getActivity();
        //String pos;
        //if (act instanceof MatchActivity)
        //    pos = ((MatchActivity) act).getPosition();
        //else
        //    pos = Prefs.getPosition(getActivity(), "Red 1");

        //boolean blue = pos.contains("Blue");

        tempData.notes = data.notes;
        ((EditText) getView().findViewById(R.id.notes)).setText(data.notes);

        tempData.yellow_card = data.yellow_card;
        tempData.red_card = data.red_card;
        setFouls(data);

        tempData.no_show = data.no_show;
        tempData.no_show_w_rep = data.no_show_w_rep;

        setNoShow(data);
        tempData.causes_penalties = data.causes_penalties;
        setPenalty(data);
        //TODO load degrade

    }

    private void setFouls(@NonNull MatchStatsStruct data) {
        if (data.red_card) {
            cardsB.setText(R.string.redcard);
            cardsB.getBackground().setColorFilter(red, filterMode);
        } else if (data.yellow_card) {
            cardsB.setText(R.string.yellowcard);
            cardsB.getBackground().setColorFilter(yellow, filterMode);
        } else {
            cardsB.setText(R.string.cards);
            cardsB.getBackground().clearColorFilter();
        }
    }

    private void setNoShow(@NonNull MatchStatsStruct data) {
        Spinner noshow = requireView().findViewById(R.id.spinner_end_show);
        if (data.no_show) {
            noshow.setSelection(2);
        } else if (data.no_show_w_rep) {
            noshow.setSelection(1);
        } else {
            noshow.setSelection(0);
        }
    }

    private void setPenalty(@NonNull MatchStatsStruct data) {
        if (data.causes_penalties) {
            penaltyB.getBackground().setColorFilter(red, filterMode);
        } else {
            penaltyB.getBackground().clearColorFilter();
        }
    }

    public class NotesSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View v, int position,
                                   long id) {
            if (position == 0 || !(parent instanceof Spinner par))
                return;
            String note = teamNotes.getText().toString();
            if (!note.contains(par.getItemAtPosition(position).toString())) {
                if (!note.trim().isEmpty())
                    note = note + "; ";
                note = note + par.getItemAtPosition(position);
                teamNotes.setText(note);
            }
            par.setSelection(0);

        }

        public void onNothingSelected(AdapterView<?> parent) {
        }

    }

    private class CardsClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (tempData.red_card) {
                tempData.red_card = false;
                tempData.yellow_card = false;
            } else if (tempData.yellow_card) {
                tempData.yellow_card = false;
                tempData.red_card = true;
            } else {
                tempData.yellow_card = true;
            }

            setFouls(tempData);
        }
    }

    private int getDegradeInt() {
        return 0; //TODO
    }

}
