/*
 * Copyright 2022 Daniel Logan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.frc836.yearly

import org.frc836.database.MatchStatsStruct

object MatchStatsYearly {
    const val NUM_GRAPHS = 4
    private const val TOTAL_SCORE = 0
    private const val AUTO = 1
    private const val TELEOP = 2
    private const val BARGE = 3

    @JvmStatic
    fun getTotalScore(stats: MatchStatsStruct): Int {
        return getAutoScore(stats) + getBargeScore(stats) + getTeleScore(stats)
    }

    @JvmStatic
    fun getBargeScore(stats: MatchStatsStruct): Int {
        return (if (stats.deep_climb_success) 12 else if (stats.shallow_climb_success) 6
        else if (stats.barge_park) 2 else 0);
    }

    @JvmStatic
    fun getTeleScore(stats: MatchStatsStruct): Int {
        return ((4 * stats.net_score)
            + (6 * stats.processor_score)

            + (5 * (stats.far_left_l4_score + stats.far_right_l4_score
            + stats.far_center_l4_score + stats.player_left_l4_score + stats.player_right_l4_score
            + stats.player_center_l4_score))

            + (4 * (stats.far_left_l3_score + stats.far_right_l3_score
            + stats.far_center_l3_score + stats.player_left_l3_score + stats.player_right_l3_score
            + stats.player_center_l3_score))

            + (3 * (stats.far_left_l2_score + stats.far_right_l2_score
            + stats.far_center_l2_score + stats.player_left_l2_score + stats.player_right_l2_score
            + stats.player_center_l2_score))

            + (2 * (stats.far_left_l1_score + stats.far_right_l1_score
            + stats.far_center_l1_score + stats.player_left_l1_score + stats.player_right_l1_score
            + stats.player_center_l1_score)))
    }

    @JvmStatic
    fun getAutoScore(stats: MatchStatsStruct): Int {
        return ((if (stats.auto_leave) 3 else 0)
            + (4 * stats.auto_net_score)
            + (6 * stats.auto_processor_score)

            + (7 * (stats.auto_far_left_l4_score + stats.auto_far_right_l4_score
            + stats.auto_far_center_l4_score + stats.auto_player_left_l4_score + stats.auto_player_right_l4_score
            + stats.auto_player_center_l4_score))

            + (6 * (stats.auto_far_left_l3_score + stats.auto_far_right_l3_score
            + stats.auto_far_center_l3_score + stats.auto_player_left_l3_score + stats.auto_player_right_l3_score
            + stats.auto_player_center_l3_score))

            + (4 * (stats.auto_far_left_l2_score + stats.auto_far_right_l2_score
            + stats.auto_far_center_l2_score + stats.auto_player_left_l2_score + stats.auto_player_right_l2_score
            + stats.auto_player_center_l2_score))

            + (3 * (stats.auto_far_left_l1_score + stats.auto_far_right_l1_score
            + stats.auto_far_center_l1_score + stats.auto_player_left_l1_score + stats.auto_player_right_l1_score
            + stats.auto_player_center_l1_score)))
    }

    @JvmStatic
    val graphNames: List<String> by lazy {
        val ret: MutableList<String> = ArrayList(NUM_GRAPHS)
        ret.add(TOTAL_SCORE, "Total Score")
        ret.add(AUTO, "Total Autonomous Score")
        ret.add(TELEOP, "Total Tele-Op score")
        ret.add(BARGE, "Total Barge Score")
        ret
    }

    @JvmStatic
    val graphShortNames: List<String> by lazy {
        val ret: MutableList<String> = ArrayList(NUM_GRAPHS)
        ret.add(TOTAL_SCORE, "TS")
        ret.add(AUTO, "AS")
        ret.add(TELEOP, "TE")
        ret.add(BARGE, "BG")
        ret
    }

    @JvmStatic
    fun getStat(statNum: Int, stats: MatchStatsStruct): Int {
        return when (statNum) {
            TOTAL_SCORE -> getTotalScore(stats)
            AUTO -> getAutoScore(stats)
            TELEOP -> getTeleScore(stats)
            BARGE -> getBargeScore(stats)
            else -> getTotalScore(stats)
        }
    }

    @JvmStatic
    fun clearAuto(stats: MatchStatsStruct) {
        stats.auto_stop = false
        stats.auto_leave = false

        stats.auto_net_score = 0
        stats.auto_net_miss = 0
        stats.auto_processor_miss = 0
        stats.auto_processor_score = 0

        stats.auto_far_center_l1_miss = 0
        stats.auto_far_center_l1_score = 0
        stats.auto_far_center_l2_miss = 0
        stats.auto_far_center_l2_score = 0
        stats.auto_far_center_l3_miss = 0
        stats.auto_far_center_l3_score = 0
        stats.auto_far_center_l4_miss = 0
        stats.auto_far_center_l4_score = 0
        stats.auto_far_left_l1_miss = 0
        stats.auto_far_left_l1_score = 0
        stats.auto_far_left_l2_miss = 0
        stats.auto_far_left_l2_score = 0
        stats.auto_far_left_l3_miss = 0
        stats.auto_far_left_l3_score = 0
        stats.auto_far_left_l4_miss = 0
        stats.auto_far_left_l4_score = 0
        stats.auto_far_right_l1_miss = 0
        stats.auto_far_right_l1_score = 0
        stats.auto_far_right_l2_miss = 0
        stats.auto_far_right_l2_score = 0
        stats.auto_far_right_l3_miss = 0
        stats.auto_far_right_l3_score = 0
        stats.auto_far_right_l4_miss = 0
        stats.auto_far_right_l4_score = 0

        stats.auto_player_center_l1_miss = 0
        stats.auto_player_center_l1_score = 0
        stats.auto_player_center_l2_miss = 0
        stats.auto_player_center_l2_score = 0
        stats.auto_player_center_l3_miss = 0
        stats.auto_player_center_l3_score = 0
        stats.auto_player_center_l4_miss = 0
        stats.auto_player_center_l4_score = 0
        stats.auto_player_left_l1_miss = 0
        stats.auto_player_left_l1_score = 0
        stats.auto_player_left_l2_miss = 0
        stats.auto_player_left_l2_score = 0
        stats.auto_player_left_l3_miss = 0
        stats.auto_player_left_l3_score = 0
        stats.auto_player_left_l4_miss = 0
        stats.auto_player_left_l4_score = 0
        stats.auto_player_right_l1_miss = 0
        stats.auto_player_right_l1_score = 0
        stats.auto_player_right_l2_miss = 0
        stats.auto_player_right_l2_score = 0
        stats.auto_player_right_l3_miss = 0
        stats.auto_player_right_l3_score = 0
        stats.auto_player_right_l4_miss = 0
        stats.auto_player_right_l4_score = 0

        stats.auto_l2_algae_removed = 0
        stats.auto_l3_algae_removed = 0

        stats.auto_algae_discarded = false
        stats.auto_algae_intake_ground_center = false
        stats.auto_algae_intake_ground_left = false
        stats.auto_algae_intake_ground_right = false
        stats.auto_coral_intake_ground_center = false
        stats.auto_coral_intake_ground_left = false
        stats.auto_coral_intake_ground_right = false

        stats.auto_barge_zone_id = ""
        stats.auto_feeder_station_id = ""
        stats.auto_first_face_id = ""
        stats.auto_ground_face_id = ""
    }

    @JvmStatic
    fun copyAuto(from: MatchStatsStruct, to: MatchStatsStruct) {
        to.auto_stop = from.auto_stop
        to.auto_leave = from.auto_leave

        to.auto_net_score = from.auto_net_score
        to.auto_net_miss = from.auto_net_miss
        to.auto_processor_miss = from.auto_processor_miss
        to.auto_processor_score = from.auto_processor_score

        to.auto_far_center_l1_miss = from.auto_far_center_l1_miss
        to.auto_far_center_l1_score = from.auto_far_center_l1_score
        to.auto_far_center_l2_miss = from.auto_far_center_l2_miss
        to.auto_far_center_l2_score = from.auto_far_center_l2_score
        to.auto_far_center_l3_miss = from.auto_far_center_l3_miss
        to.auto_far_center_l3_score = from.auto_far_center_l3_score
        to.auto_far_center_l4_miss = from.auto_far_center_l4_miss
        to.auto_far_center_l4_score = from.auto_far_center_l4_score
        to.auto_far_left_l1_miss = from.auto_far_left_l1_miss
        to.auto_far_left_l1_score = from.auto_far_left_l1_score
        to.auto_far_left_l2_miss = from.auto_far_left_l2_miss
        to.auto_far_left_l2_score = from.auto_far_left_l2_score
        to.auto_far_left_l3_miss = from.auto_far_left_l3_miss
        to.auto_far_left_l3_score = from.auto_far_left_l3_score
        to.auto_far_left_l4_miss = from.auto_far_left_l4_miss
        to.auto_far_left_l4_score = from.auto_far_left_l4_score
        to.auto_far_right_l1_miss = from.auto_far_right_l1_miss
        to.auto_far_right_l1_score = from.auto_far_right_l1_score
        to.auto_far_right_l2_miss = from.auto_far_right_l2_miss
        to.auto_far_right_l2_score = from.auto_far_right_l2_score
        to.auto_far_right_l3_miss = from.auto_far_right_l3_miss
        to.auto_far_right_l3_score = from.auto_far_right_l3_score
        to.auto_far_right_l4_miss = from.auto_far_right_l4_miss
        to.auto_far_right_l4_score = from.auto_far_right_l4_score

        to.auto_player_center_l1_miss = from.auto_player_center_l1_miss
        to.auto_player_center_l1_score = from.auto_player_center_l1_score
        to.auto_player_center_l2_miss = from.auto_player_center_l2_miss
        to.auto_player_center_l2_score = from.auto_player_center_l2_score
        to.auto_player_center_l3_miss = from.auto_player_center_l3_miss
        to.auto_player_center_l3_score = from.auto_player_center_l3_score
        to.auto_player_center_l4_miss = from.auto_player_center_l4_miss
        to.auto_player_center_l4_score = from.auto_player_center_l4_score
        to.auto_player_left_l1_miss = from.auto_player_left_l1_miss
        to.auto_player_left_l1_score = from.auto_player_left_l1_score
        to.auto_player_left_l2_miss = from.auto_player_left_l2_miss
        to.auto_player_left_l2_score = from.auto_player_left_l2_score
        to.auto_player_left_l3_miss = from.auto_player_left_l3_miss
        to.auto_player_left_l3_score = from.auto_player_left_l3_score
        to.auto_player_left_l4_miss = from.auto_player_left_l4_miss
        to.auto_player_left_l4_score = from.auto_player_left_l4_score
        to.auto_player_right_l1_miss = from.auto_player_right_l1_miss
        to.auto_player_right_l1_score = from.auto_player_right_l1_score
        to.auto_player_right_l2_miss = from.auto_player_right_l2_miss
        to.auto_player_right_l2_score = from.auto_player_right_l2_score
        to.auto_player_right_l3_miss = from.auto_player_right_l3_miss
        to.auto_player_right_l3_score = from.auto_player_right_l3_score
        to.auto_player_right_l4_miss = from.auto_player_right_l4_miss
        to.auto_player_right_l4_score = from.auto_player_right_l4_score

        to.auto_l2_algae_removed = from.auto_l2_algae_removed
        to.auto_l3_algae_removed = from.auto_l3_algae_removed

        to.auto_algae_discarded = from.auto_algae_discarded
        to.auto_algae_intake_ground_center = from.auto_algae_intake_ground_center
        to.auto_algae_intake_ground_left = from.auto_algae_intake_ground_left
        to.auto_algae_intake_ground_right = from.auto_algae_intake_ground_right
        to.auto_coral_intake_ground_center = from.auto_coral_intake_ground_center
        to.auto_coral_intake_ground_left = from.auto_coral_intake_ground_left
        to.auto_coral_intake_ground_right = from.auto_coral_intake_ground_right

        to.auto_first_face_id = from.auto_first_face_id
        to.auto_ground_face_id = from.auto_ground_face_id
        to.auto_feeder_station_id = from.auto_feeder_station_id
        to.auto_barge_zone_id = from.auto_barge_zone_id
    }

    @JvmStatic
    fun clearTele(stats: MatchStatsStruct) {
        stats.net_miss = 0
        stats.net_score = 0
        stats.processor_miss = 0
        stats.processor_score = 0
        stats.l2_algae_removed = 0
        stats.l3_algae_removed = 0
        stats.algae_stolen_by_opp = false
        stats.controlled_algae_removal = false

        stats.far_center_l1_miss = 0
        stats.far_center_l1_score = 0
        stats.far_center_l2_miss = 0
        stats.far_center_l2_score = 0
        stats.far_center_l3_miss = 0
        stats.far_center_l3_score = 0
        stats.far_center_l4_miss = 0
        stats.far_center_l4_score = 0
        stats.far_left_l1_miss = 0
        stats.far_left_l1_score = 0
        stats.far_left_l2_miss = 0
        stats.far_left_l2_score = 0
        stats.far_left_l3_miss = 0
        stats.far_left_l3_score = 0
        stats.far_left_l4_miss = 0
        stats.far_left_l4_score = 0
        stats.far_right_l1_miss = 0
        stats.far_right_l1_score = 0
        stats.far_right_l2_miss = 0
        stats.far_right_l2_score = 0
        stats.far_right_l3_miss = 0
        stats.far_right_l3_score = 0
        stats.far_right_l4_miss = 0
        stats.far_right_l4_score = 0

        stats.player_center_l1_miss = 0
        stats.player_center_l1_score = 0
        stats.player_center_l2_miss = 0
        stats.player_center_l2_score = 0
        stats.player_center_l3_miss = 0
        stats.player_center_l3_score = 0
        stats.player_center_l4_miss = 0
        stats.player_center_l4_score = 0
        stats.player_left_l1_miss = 0
        stats.player_left_l1_score = 0
        stats.player_left_l2_miss = 0
        stats.player_left_l2_score = 0
        stats.player_left_l3_miss = 0
        stats.player_left_l3_score = 0
        stats.player_left_l4_miss = 0
        stats.player_left_l4_score = 0
        stats.player_right_l1_miss = 0
        stats.player_right_l1_score = 0
        stats.player_right_l2_miss = 0
        stats.player_right_l2_score = 0
        stats.player_right_l3_miss = 0
        stats.player_right_l3_score = 0
        stats.player_right_l4_miss = 0
        stats.player_right_l4_score = 0
    }

    @JvmStatic
    fun copyTele(from: MatchStatsStruct, to: MatchStatsStruct) {
        to.net_miss = from.net_miss
        to.net_score = from.net_score
        to.processor_miss = from.processor_miss
        to.processor_score = from.processor_score
        to.l2_algae_removed = from.l2_algae_removed
        to.l3_algae_removed = from.l3_algae_removed
        to.algae_stolen_by_opp = from.algae_stolen_by_opp
        to.controlled_algae_removal = from.controlled_algae_removal

        to.far_center_l1_miss = from.far_center_l1_miss
        to.far_center_l1_score = from.far_center_l1_score
        to.far_center_l2_miss = from.far_center_l2_miss
        to.far_center_l2_score = from.far_center_l2_score
        to.far_center_l3_miss = from.far_center_l3_miss
        to.far_center_l3_score = from.far_center_l3_score
        to.far_center_l4_miss = from.far_center_l4_miss
        to.far_center_l4_score = from.far_center_l4_score
        to.far_left_l1_miss = from.far_left_l1_miss
        to.far_left_l1_score = from.far_left_l1_score
        to.far_left_l2_miss = from.far_left_l2_miss
        to.far_left_l2_score = from.far_left_l2_score
        to.far_left_l3_miss = from.far_left_l3_miss
        to.far_left_l3_score = from.far_left_l3_score
        to.far_left_l4_miss = from.far_left_l4_miss
        to.far_left_l4_score = from.far_left_l4_score
        to.far_right_l1_miss = from.far_right_l1_miss
        to.far_right_l1_score = from.far_right_l1_score
        to.far_right_l2_miss = from.far_right_l2_miss
        to.far_right_l2_score = from.far_right_l2_score
        to.far_right_l3_miss = from.far_right_l3_miss
        to.far_right_l3_score = from.far_right_l3_score
        to.far_right_l4_miss = from.far_right_l4_miss
        to.far_right_l4_score = from.far_right_l4_score

        to.player_center_l1_miss = from.player_center_l1_miss
        to.player_center_l1_score = from.player_center_l1_score
        to.player_center_l2_miss = from.player_center_l2_miss
        to.player_center_l2_score = from.player_center_l2_score
        to.player_center_l3_miss = from.player_center_l3_miss
        to.player_center_l3_score = from.player_center_l3_score
        to.player_center_l4_miss = from.player_center_l4_miss
        to.player_center_l4_score = from.player_center_l4_score
        to.player_left_l1_miss = from.player_left_l1_miss
        to.player_left_l1_score = from.player_left_l1_score
        to.player_left_l2_miss = from.player_left_l2_miss
        to.player_left_l2_score = from.player_left_l2_score
        to.player_left_l3_miss = from.player_left_l3_miss
        to.player_left_l3_score = from.player_left_l3_score
        to.player_left_l4_miss = from.player_left_l4_miss
        to.player_left_l4_score = from.player_left_l4_score
        to.player_right_l1_miss = from.player_right_l1_miss
        to.player_right_l1_score = from.player_right_l1_score
        to.player_right_l2_miss = from.player_right_l2_miss
        to.player_right_l2_score = from.player_right_l2_score
        to.player_right_l3_miss = from.player_right_l3_miss
        to.player_right_l3_score = from.player_right_l3_score
        to.player_right_l4_miss = from.player_right_l4_miss
        to.player_right_l4_score = from.player_right_l4_score
    }

    @JvmStatic
    fun clearEndgame(stats: MatchStatsStruct) {
        stats.causes_penalties = false

        stats.red_card = false
        stats.degraded_performance = false
        stats.no_show = false
        stats.no_show_w_rep = false
        stats.notes = ""
        stats.yellow_card = false
    }

    @JvmStatic
    fun copyEndgame(from: MatchStatsStruct, to: MatchStatsStruct) {
        to.causes_penalties = from.causes_penalties

        to.red_card = from.red_card
        to.degraded_performance = from.degraded_performance
        to.no_show = from.no_show
        to.no_show_w_rep = from.no_show_w_rep
        to.notes = from.notes
        to.yellow_card = from.yellow_card
    }

    @JvmStatic
    fun clearBarge(stats: MatchStatsStruct) {
        stats.deep_climb_unsuccessful_attempt = false
        stats.deep_climb_success = false
        stats.shallow_climb_unsuccessful_attempt = false
        stats.shallow_climb_success = false
        stats.buddy_climb = false
        stats.barge_park = false
    }

    @JvmStatic
    fun copyBarge(from: MatchStatsStruct, to: MatchStatsStruct) {
        to.deep_climb_unsuccessful_attempt = from.deep_climb_unsuccessful_attempt
        to.deep_climb_success = from.deep_climb_success
        to.shallow_climb_unsuccessful_attempt = from.shallow_climb_unsuccessful_attempt
        to.shallow_climb_success = from.shallow_climb_success
        to.buddy_climb = from.buddy_climb
        to.barge_park = from.barge_park
    }

    @JvmStatic
    fun clearDef(stats: MatchStatsStruct) {
        stats.can_propel_algae_through_barge = false
        stats.can_launch_algae_over_barge = false
        stats.can_place_algae_precisely = false
        stats.scores_allowed_while_def = 0
        stats.algae_stolen_from_opp = 0
    }

    @JvmStatic
    fun copyDef(from: MatchStatsStruct, to: MatchStatsStruct) {
        to.can_propel_algae_through_barge = from.can_propel_algae_through_barge
        to.can_launch_algae_over_barge = from.can_launch_algae_over_barge
        to.can_place_algae_precisely = from.can_place_algae_precisely
        to.scores_allowed_while_def = from.scores_allowed_while_def
        to.algae_stolen_from_opp = from.algae_stolen_from_opp
    }
}
