/*
 * Copyright 2016 Daniel Logan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.frc836.yearly;


import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;

import org.frc836.database.MatchStatsStruct;
import org.growingstems.scouting.MatchFragment;
import org.growingstems.scouting.OnIncrementListener;
import org.growingstems.scouting.Prefs;
import org.growingstems.scouting.R;

import java.util.HashMap;


public class AutoMatchFragment extends MatchFragment {

    private final MatchStatsStruct tempData = new MatchStatsStruct();

    private Drawable checkmarks;

    /*
    TODO: Still missing:
    auto_leave
    auto_net_miss
    auto_processor_miss
    auto_l2_algae_removed
    auto_l3_algae_removed
    auto_algae_discarded
    auto_first_face_id
    auto_ground_face_id
     */

    private ImageView reef_player_center;
    private ImageView reef_player_left;
    private ImageView reef_player_right;
    private ImageView reef_far_left;
    private ImageView reef_far_center;
    private ImageView reef_far_right;

    private final ReefStats reef_player_center_stats = new ReefStats();
    private final ReefStats reef_player_left_stats = new ReefStats();
    private final ReefStats reef_player_right_stats = new ReefStats();
    private final ReefStats reef_far_left_stats = new ReefStats();
    private final ReefStats reef_far_center_stats = new ReefStats();
    private final ReefStats reef_far_right_stats = new ReefStats();

    private Button proc_inc;
    private Spinner proc_score;
    private Button net_inc;
    private Spinner net_score;

    private Button auto_stop;
    private Button left_station;
    private Button right_station;

    private ImageView coral_left;
    private ImageView coral_mid;
    private ImageView coral_right;
    private ImageView algae_left;
    private ImageView algae_mid;
    private ImageView algae_right;

    private final HashMap<ReefStats.Face, Integer> deselect_faces = new HashMap<>(6);
    private final HashMap<ReefStats.Face, Integer> data_faces = new HashMap<>(6);

    private static final String dialog_transaction_name = "AUTO_REEF";

    public AutoMatchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PreMatch.
     */
    public static AutoMatchFragment newInstance() {
        return new AutoMatchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_match_auto, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        getBaseGUIRefs(view);
        setDisplayed(true);
        //Don't call setListeners here, as it needs to be called after the side is determined
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData(tempData);
    }

    public void onPause() {
        super.onPause();
        saveData(tempData);
    }

    @Override
    public void saveData(@NonNull MatchStatsStruct data) {
        if (getView() == null || !getDisplayed()) return;


        tempData.auto_processor_score = proc_score.getSelectedItemPosition();
        tempData.auto_net_score = net_score.getSelectedItemPosition();
        reef_player_center_stats.saveToMatchStats(tempData, ReefStats.Face.PLAYER_CENTER, true);
        reef_player_left_stats.saveToMatchStats(tempData, ReefStats.Face.PLAYER_LEFT, true);
        reef_player_right_stats.saveToMatchStats(tempData, ReefStats.Face.PLAYER_RIGHT, true);
        reef_far_center_stats.saveToMatchStats(tempData, ReefStats.Face.FAR_CENTER, true);
        reef_far_left_stats.saveToMatchStats(tempData, ReefStats.Face.FAR_LEFT, true);
        reef_far_right_stats.saveToMatchStats(tempData, ReefStats.Face.FAR_RIGHT, true);
        MatchStatsYearly.copyAuto(tempData, data);
    }

    @Override
    public void loadData(@NonNull MatchStatsStruct data) {
        tempData.copyFrom(data);
        View view = getView();
        if (view == null || !getDisplayed()) return;

        updateSide(view);
        setListeners();

        Activity act = getActivity();
        if (act == null) return;


        reef_player_center_stats.loadFromMatchStats(tempData, ReefStats.Face.PLAYER_CENTER, true);
        reef_player_left_stats.loadFromMatchStats(tempData, ReefStats.Face.PLAYER_LEFT, true);
        reef_player_right_stats.loadFromMatchStats(tempData, ReefStats.Face.PLAYER_RIGHT, true);
        reef_far_center_stats.loadFromMatchStats(tempData, ReefStats.Face.FAR_CENTER, true);
        reef_far_left_stats.loadFromMatchStats(tempData, ReefStats.Face.FAR_LEFT, true);
        reef_far_right_stats.loadFromMatchStats(tempData, ReefStats.Face.FAR_RIGHT, true);

        switch (tempData.auto_feeder_station_id) {
            case "sta_none" -> {
                left_station.setForeground(null);
                right_station.setForeground(null);
            }
            case "sta_right" -> {
                left_station.setForeground(null);
                right_station.setForeground(checkmarks);
            }
            case "sta_left" -> {
                left_station.setForeground(checkmarks);
                right_station.setForeground(null);
            }
            case "sta_both" -> {
                left_station.setForeground(checkmarks);
                right_station.setForeground(checkmarks);
            }
            default -> {
                tempData.auto_feeder_station_id = "sta_none";
                left_station.setForeground(null);
                right_station.setForeground(null);
            }
        }

        proc_score.setSelection(tempData.auto_processor_score);
        net_score.setSelection(tempData.auto_net_score);
        auto_stop.setForeground(tempData.auto_stop ? checkmarks : null);
        algae_left.setForeground(tempData.auto_algae_intake_ground_left ? checkmarks : null);
        algae_mid.setForeground(tempData.auto_algae_intake_ground_center ? checkmarks : null);
        algae_right.setForeground(tempData.auto_algae_intake_ground_right ? checkmarks : null);
        coral_left.setForeground(tempData.auto_coral_intake_ground_left ? checkmarks : null);
        coral_mid.setForeground(tempData.auto_coral_intake_ground_center ? checkmarks : null);
        coral_right.setForeground(tempData.auto_coral_intake_ground_right ? checkmarks : null);
        setReefFaces();
    }

    private void updateSide(View view) {
        Activity act = getActivity();
        boolean redLeft = Prefs.getRedLeft(act, true);
        String pos;
        if (act instanceof MatchActivity) pos = ((MatchActivity) act).getPosition();
        else pos = Prefs.getPosition(act, "Red 1");

        if ((pos.contains("Blue") && !redLeft) || ((!pos.contains("Blue") && redLeft))) {
            //Left side layout
            coral_left = view.findViewById(R.id.image_auto_coral_top);
            coral_right = view.findViewById(R.id.image_auto_coral_bot);
            algae_left = view.findViewById(R.id.image_auto_algae_top);
            algae_right = view.findViewById(R.id.image_auto_algae_bot);

            reef_player_center = view.findViewById(R.id.auto_reef_left);
            reef_player_left = view.findViewById(R.id.auto_reef_top_left);
            reef_player_right = view.findViewById(R.id.auto_reef_bot_left);
            reef_far_center = view.findViewById(R.id.auto_reef_right);
            reef_far_left = view.findViewById(R.id.auto_reef_top_right);
            reef_far_right = view.findViewById(R.id.auto_reef_bot_right);

            deselect_faces.put(ReefStats.Face.PLAYER_CENTER, R.drawable.left_deselect);
            deselect_faces.put(ReefStats.Face.PLAYER_LEFT, R.drawable.top_left_deselect);
            deselect_faces.put(ReefStats.Face.PLAYER_RIGHT, R.drawable.bot_left_deselect);
            deselect_faces.put(ReefStats.Face.FAR_CENTER, R.drawable.right_deselect);
            deselect_faces.put(ReefStats.Face.FAR_LEFT, R.drawable.top_right_deselect);
            deselect_faces.put(ReefStats.Face.FAR_RIGHT, R.drawable.bot_right_deselect);
            data_faces.put(ReefStats.Face.PLAYER_CENTER, R.drawable.left_data);
            data_faces.put(ReefStats.Face.PLAYER_LEFT, R.drawable.top_left_data);
            data_faces.put(ReefStats.Face.PLAYER_RIGHT, R.drawable.bot_left_data);
            data_faces.put(ReefStats.Face.FAR_CENTER, R.drawable.right_data);
            data_faces.put(ReefStats.Face.FAR_LEFT, R.drawable.top_right_data);
            data_faces.put(ReefStats.Face.FAR_RIGHT, R.drawable.bot_right_data);

        } else {
            //Right side layout
            coral_left = view.findViewById(R.id.image_auto_coral_bot);
            coral_right = view.findViewById(R.id.image_auto_coral_top);
            algae_left = view.findViewById(R.id.image_auto_algae_bot);
            algae_right = view.findViewById(R.id.image_auto_algae_top);

            reef_player_center = view.findViewById(R.id.auto_reef_right);
            reef_player_left = view.findViewById(R.id.auto_reef_bot_right);
            reef_player_right = view.findViewById(R.id.auto_reef_top_right);
            reef_far_center = view.findViewById(R.id.auto_reef_left);
            reef_far_left = view.findViewById(R.id.auto_reef_bot_left);
            reef_far_right = view.findViewById(R.id.auto_reef_top_left);

            deselect_faces.put(ReefStats.Face.PLAYER_CENTER, R.drawable.right_deselect);
            deselect_faces.put(ReefStats.Face.PLAYER_LEFT, R.drawable.bot_right_deselect);
            deselect_faces.put(ReefStats.Face.PLAYER_RIGHT, R.drawable.top_right_deselect);
            deselect_faces.put(ReefStats.Face.FAR_CENTER, R.drawable.left_deselect);
            deselect_faces.put(ReefStats.Face.FAR_LEFT, R.drawable.bot_left_deselect);
            deselect_faces.put(ReefStats.Face.FAR_RIGHT, R.drawable.top_left_deselect);
            data_faces.put(ReefStats.Face.PLAYER_CENTER, R.drawable.right_data);
            data_faces.put(ReefStats.Face.PLAYER_LEFT, R.drawable.bot_right_data);
            data_faces.put(ReefStats.Face.PLAYER_RIGHT, R.drawable.top_right_data);
            data_faces.put(ReefStats.Face.FAR_CENTER, R.drawable.left_data);
            data_faces.put(ReefStats.Face.FAR_LEFT, R.drawable.bot_left_data);
            data_faces.put(ReefStats.Face.FAR_RIGHT, R.drawable.top_left_data);
        }
    }

    private void getBaseGUIRefs(View view) {
        checkmarks = getActivity() == null ? null : AppCompatResources.getDrawable(getActivity(), R.drawable.icon_awesome_check_double);

        proc_inc = view.findViewById(R.id.button_auto_processor_score_increment);
        proc_score = view.findViewById(R.id.spinner_auto_processor_score);

        net_inc = view.findViewById(R.id.button_auto_net_score_increment);
        net_score = view.findViewById(R.id.spinner_auto_net_score);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(), R.array.numbers, R.layout.spinner_center_item);
        // set whatever dropdown resource you want
        adapter.setDropDownViewResource(R.layout.spinner_center_item);
        proc_score.setAdapter(adapter);
        net_score.setAdapter(adapter);

        auto_stop = view.findViewById(R.id.button_auto_astop);
        left_station = view.findViewById(R.id.button_auto_left_feeder);
        right_station = view.findViewById(R.id.button_auto_right_feeder);

        coral_mid = view.findViewById(R.id.image_auto_coral_mid);
        algae_mid = view.findViewById(R.id.image_auto_algae_mid);

        //All other UI elements are mapped to local variables based on which side we're on
    }

    private void setListeners() {

        Activity act = getActivity();
        if (act == null) return;

        proc_inc.setOnClickListener(new OnIncrementListener(proc_score, 1));
        net_inc.setOnClickListener(new OnIncrementListener(net_score, 1));

        auto_stop.setOnClickListener(v -> {
            tempData.auto_stop = !tempData.auto_stop;
            auto_stop.setForeground(tempData.auto_stop ? checkmarks : null);
        });

        left_station.setOnClickListener(v -> {
            tempData.auto_feeder_station_id = switch (tempData.auto_feeder_station_id) {
                case "sta_none" -> "sta_left";
                case "sta_left" -> "sta_none";
                case "sta_right" -> "sta_both";
                case "sta_both" -> "sta_right";
                default -> "sta_none";
            };
            switch (tempData.auto_feeder_station_id) {
                case "sta_none", "sta_right" -> left_station.setForeground(null);
                case "sta_left", "sta_both" -> left_station.setForeground(checkmarks);
                default -> left_station.setForeground(null);
            }
        });

        right_station.setOnClickListener(v -> {
            tempData.auto_feeder_station_id = switch (tempData.auto_feeder_station_id) {
                case "sta_none" -> "sta_right";
                case "sta_left" -> "sta_both";
                case "sta_right" -> "sta_none";
                case "sta_both" -> "sta_left";
                default -> "sta_none";
            };
            switch (tempData.auto_feeder_station_id) {
                case "sta_both", "sta_right" -> right_station.setForeground(checkmarks);
                case "sta_left", "sta_none" -> right_station.setForeground(null);
                default -> right_station.setForeground(null);
            }
        });

        algae_left.setOnClickListener(v -> {
            tempData.auto_algae_intake_ground_left = !tempData.auto_algae_intake_ground_left;
            algae_left.setForeground(tempData.auto_algae_intake_ground_left ? checkmarks : null);
        });
        algae_mid.setOnClickListener(v -> {
            tempData.auto_algae_intake_ground_center = !tempData.auto_algae_intake_ground_center;
            algae_mid.setForeground(tempData.auto_algae_intake_ground_center ? checkmarks : null);
        });
        algae_right.setOnClickListener(v -> {
            tempData.auto_algae_intake_ground_right = !tempData.auto_algae_intake_ground_right;
            algae_right.setForeground(tempData.auto_algae_intake_ground_right ? checkmarks : null);
        });

        coral_left.setOnClickListener(v -> {
            tempData.auto_coral_intake_ground_left = !tempData.auto_coral_intake_ground_left;
            coral_left.setForeground(tempData.auto_coral_intake_ground_left ? checkmarks : null);
        });
        coral_mid.setOnClickListener(v -> {
            tempData.auto_coral_intake_ground_center = !tempData.auto_coral_intake_ground_center;
            coral_mid.setForeground(tempData.auto_coral_intake_ground_center ? checkmarks : null);
        });
        coral_right.setOnClickListener(v -> {
            tempData.auto_coral_intake_ground_right = !tempData.auto_coral_intake_ground_right;
            coral_right.setForeground(tempData.auto_coral_intake_ground_right ? checkmarks : null);
        });

        reef_far_left.setOnClickListener(v -> {
            new ReefDialogFragment(reef_far_left_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
        reef_far_center.setOnClickListener(v -> {
            new ReefDialogFragment(reef_far_center_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
        reef_far_right.setOnClickListener(v -> {
            new ReefDialogFragment(reef_far_right_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
        reef_player_left.setOnClickListener(v -> {
            new ReefDialogFragment(reef_player_left_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
        reef_player_center.setOnClickListener(v -> {
            new ReefDialogFragment(reef_player_center_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
        reef_player_right.setOnClickListener(v -> {
            new ReefDialogFragment(reef_player_right_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
    }

    private void setReefFaces() {
        reef_player_center.setBackgroundResource(reef_player_center_stats.containsData() ? data_faces.get(ReefStats.Face.PLAYER_CENTER) : deselect_faces.get(ReefStats.Face.PLAYER_CENTER));
        reef_player_left.setBackgroundResource(reef_player_left_stats.containsData() ? data_faces.get(ReefStats.Face.PLAYER_LEFT) : deselect_faces.get(ReefStats.Face.PLAYER_LEFT));
        reef_player_right.setBackgroundResource(reef_player_right_stats.containsData() ? data_faces.get(ReefStats.Face.PLAYER_RIGHT) : deselect_faces.get(ReefStats.Face.PLAYER_RIGHT));
        reef_far_center.setBackgroundResource(reef_far_center_stats.containsData() ? data_faces.get(ReefStats.Face.FAR_CENTER) : deselect_faces.get(ReefStats.Face.FAR_CENTER));
        reef_far_left.setBackgroundResource(reef_far_left_stats.containsData() ? data_faces.get(ReefStats.Face.FAR_LEFT) : deselect_faces.get(ReefStats.Face.FAR_LEFT));
        reef_far_right.setBackgroundResource(reef_far_right_stats.containsData() ? data_faces.get(ReefStats.Face.FAR_RIGHT) : deselect_faces.get(ReefStats.Face.FAR_RIGHT));
    }
}
