package org.frc836.yearly

import android.app.Activity
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import org.frc836.database.MatchStatsStruct
import org.growingstems.scouting.MatchFragment
import org.growingstems.scouting.Prefs
import org.growingstems.scouting.R

class PreMatchFragment : MatchFragment() {

    private val tempData: MatchStatsStruct = MatchStatsStruct()

    private var checkmarks: Drawable? = null

    /**2025 Constraints */
    private var mainLayout: ConstraintLayout? = null

    private var blueTeamRedLeftC: ConstraintSet? = null
    private var blueTeamRedRightC: ConstraintSet? = null
    private var redTeamRedLeftC: ConstraintSet? = null
    private var redTeamRedRightC: ConstraintSet? = null

    // Pre-match
    private var posTopB: Button? = null
    private var posMidTopB: Button? = null
    private var posMidB: Button? = null
    private var posMidBotB: Button? = null
    private var posBotB: Button? = null

    private var preMatchBargeTopI: ImageView? = null
    private var preMatchBargeBotI: ImageView? = null

    private var leftLayout: Boolean = false;

    companion object {
        @JvmStatic
        fun newInstance(): PreMatchFragment {
            return PreMatchFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_match_pre, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getBaseGuiRefs()
        displayed = true
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        loadData(tempData)
    }

    override fun onPause() {
        super.onPause()
        saveData(tempData)
    }

    private fun getBaseGuiRefs() {
        val act: Activity? = activity
        checkmarks = if (activity == null) null else AppCompatResources.getDrawable(
            act!!,
            R.drawable.icon_awesome_check_double
        )

        mainLayout = requireView().findViewById(R.id.preMatchLayout)

        posTopB = requireView().findViewById(R.id.position_top)
        posMidTopB = requireView().findViewById(R.id.position_mid_top)
        posMidB = requireView().findViewById(R.id.position_mid)
        posMidBotB = requireView().findViewById(R.id.position_mid_bot)
        posBotB = requireView().findViewById(R.id.position_bot)

        preMatchBargeTopI = requireView().findViewById(R.id.preMatchBargeTop)
        preMatchBargeBotI = requireView().findViewById(R.id.preMatchBargeBot)
    }

    override fun saveData(data: MatchStatsStruct) {
        if (!displayed) return

        data.auto_barge_zone_id = tempData.auto_barge_zone_id
    }

    override fun loadData(data: MatchStatsStruct) {
        tempData.copyFrom(data)

        if (!displayed) return

        tempData.auto_barge_zone_id = data.auto_barge_zone_id

        updateSide()
        setListeners()

        when (tempData.auto_barge_zone_id) {
            "barge_l" -> {
                if (leftLayout) {
                    posTopB!!.foreground = checkmarks
                } else {
                    posBotB!!.foreground = checkmarks
                }
            }
            "barge_cl" -> {
                if (leftLayout) {
                    posMidTopB!!.foreground = checkmarks
                } else {
                    posMidBotB!!.foreground = checkmarks
                }
            }
            "barge_c" -> {
                posMidB!!.foreground = checkmarks
            }
            "barge_cr" -> {
                if (leftLayout) {
                    posMidBotB!!.foreground = checkmarks
                } else {
                    posMidTopB!!.foreground = checkmarks
                }
            }
            "barge_r" -> {
                if (leftLayout) {
                    posBotB!!.foreground = checkmarks
                } else {
                    posTopB!!.foreground = checkmarks
                }
            }
        }

        activity ?: return
    }

    private fun setListeners() {
        val act: Activity? = activity

        posTopB!!.setOnClickListener {
            if (leftLayout) {
                tempData.auto_barge_zone_id = "barge_l"
            } else {
                tempData.auto_barge_zone_id = "barge_r"
            }
            posTopB!!.foreground = checkmarks
            posMidTopB!!.foreground = null
            posMidB!!.foreground = null
            posMidBotB!!.foreground = null
            posBotB!!.foreground = null
        }

        posMidTopB!!.setOnClickListener {
            if (leftLayout) {
                tempData.auto_barge_zone_id = "barge_cl"
            } else {
                tempData.auto_barge_zone_id = "barge_cr"
            }
            posTopB!!.foreground = null
            posMidTopB!!.foreground = checkmarks
            posMidB!!.foreground = null
            posMidBotB!!.foreground = null
            posBotB!!.foreground = null
        }

        posMidB!!.setOnClickListener {
            tempData.auto_barge_zone_id = "barge_c"

            posTopB!!.foreground = null
            posMidTopB!!.foreground = null
            posMidB!!.foreground = checkmarks
            posMidBotB!!.foreground = null
            posBotB!!.foreground = null
        }

        posMidBotB!!.setOnClickListener {
            if (leftLayout) {
                tempData.auto_barge_zone_id = "barge_cr"
            } else {
                tempData.auto_barge_zone_id = "barge_cl"
            }
            posTopB!!.foreground = null
            posMidTopB!!.foreground = null
            posMidB!!.foreground = null
            posMidBotB!!.foreground = checkmarks
            posBotB!!.foreground = null
        }

        posBotB!!.setOnClickListener {
            if (leftLayout) {
                tempData.auto_barge_zone_id = "barge_r"
            } else {
                tempData.auto_barge_zone_id = "barge_l"
            }
            posTopB!!.foreground = null
            posMidTopB!!.foreground = null
            posMidB!!.foreground = null
            posMidBotB!!.foreground = null
            posBotB!!.foreground = checkmarks
        }
    }

    private fun updateSide() {
        val act: Activity? = activity
        val redLeft = Prefs.getRedLeft(act, true)
        val pos = if (act is MatchActivity) (act as MatchActivity).position
        else Prefs.getPosition(act, "Red 1")

        if (pos.contains("Blue") && redLeft) {
            leftLayout = false
            // team is Blue, Red's field to the left
            if (blueTeamRedLeftC == null) {
                configureBlueLeftConstraints()
            }
            blueTeamRedLeftC!!.applyTo(mainLayout)

        } else if (pos.contains("Blue") && !redLeft) {
            leftLayout = true
            // team is Blue, Red's field to the right (default)
            if (blueTeamRedRightC == null) {
                configureBlueRightConstraints()
            }
            blueTeamRedRightC!!.applyTo(mainLayout)

        } else if (!pos.contains("Blue") && redLeft) {
            leftLayout = true
            // team is Red, Red's field to the left
            if (redTeamRedLeftC == null) {
                configureRedLeftConstraints()
            }
            redTeamRedLeftC!!.applyTo(mainLayout)

        } else {
            leftLayout = false
            // team is Red, Red's field to the right
            if (redTeamRedRightC == null) {
                configureRedRightConstraints()
            }
            redTeamRedRightC!!.applyTo(mainLayout)

        }

        if (!redLeft) {
            preMatchBargeTopI!!.setImageResource(R.drawable.pre_rectangle_blue)
            preMatchBargeBotI!!.setImageResource(R.drawable.pre_rectangle_red)
        } else {
            preMatchBargeTopI!!.setImageResource(R.drawable.pre_rectangle_red)
            preMatchBargeBotI!!.setImageResource(R.drawable.pre_rectangle_blue)
        }
    }

    private fun configureBlueLeftConstraints() {
        blueTeamRedLeftC = ConstraintSet()
        blueTeamRedLeftC!!.clone(mainLayout)

        // top
        blueTeamRedLeftC!!.connect(R.id.position_top, ConstraintSet.LEFT, R.id.guide_pre_mid_left, ConstraintSet.RIGHT)
        blueTeamRedLeftC!!.connect(R.id.position_top, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
        // mid top
        blueTeamRedLeftC!!.connect(R.id.position_mid_top, ConstraintSet.LEFT, R.id.guide_pre_mid_left, ConstraintSet.RIGHT)
        blueTeamRedLeftC!!.connect(R.id.position_mid_top, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
        // mid
        blueTeamRedLeftC!!.connect(R.id.position_mid, ConstraintSet.LEFT, R.id.guide_pre_mid_left, ConstraintSet.RIGHT)
        blueTeamRedLeftC!!.connect(R.id.position_mid, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
        // mid bot
        blueTeamRedLeftC!!.connect(R.id.position_mid_bot, ConstraintSet.LEFT, R.id.guide_pre_mid_left, ConstraintSet.RIGHT)
        blueTeamRedLeftC!!.connect(R.id.position_mid_bot, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
        // bot
        blueTeamRedLeftC!!.connect(R.id.position_bot, ConstraintSet.LEFT, R.id.guide_pre_mid_left, ConstraintSet.RIGHT)
        blueTeamRedLeftC!!.connect(R.id.position_bot, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)


        // match barge top
        blueTeamRedLeftC!!.connect(R.id.preMatchBargeTop, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        blueTeamRedLeftC!!.connect(R.id.preMatchBargeTop, ConstraintSet.RIGHT, R.id.guide_pre_mid_left, ConstraintSet.LEFT)
        // match barge bottom
        blueTeamRedLeftC!!.connect(R.id.preMatchBargeBot, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        blueTeamRedLeftC!!.connect(R.id.preMatchBargeBot, ConstraintSet.RIGHT, R.id.guide_pre_mid_left, ConstraintSet.LEFT)
    }

    private fun configureBlueRightConstraints() {
        blueTeamRedRightC = ConstraintSet()
        blueTeamRedRightC!!.clone(mainLayout)

        // top
        blueTeamRedRightC!!.connect(R.id.position_top, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        blueTeamRedRightC!!.connect(R.id.position_top, ConstraintSet.RIGHT, R.id.guide_pre_mid_right, ConstraintSet.LEFT)
        // mid top
        blueTeamRedRightC!!.connect(R.id.position_mid_top, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        blueTeamRedRightC!!.connect(R.id.position_mid_top, ConstraintSet.RIGHT, R.id.guide_pre_mid_right, ConstraintSet.LEFT)
        // mid
        blueTeamRedRightC!!.connect(R.id.position_mid, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        blueTeamRedRightC!!.connect(R.id.position_mid, ConstraintSet.RIGHT, R.id.guide_pre_mid_right, ConstraintSet.LEFT)
        // mid bot
        blueTeamRedRightC!!.connect(R.id.position_mid_bot, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        blueTeamRedRightC!!.connect(R.id.position_mid_bot, ConstraintSet.RIGHT, R.id.guide_pre_mid_right, ConstraintSet.LEFT)
        // bot
        blueTeamRedRightC!!.connect(R.id.position_bot, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        blueTeamRedRightC!!.connect(R.id.position_bot, ConstraintSet.RIGHT, R.id.guide_pre_mid_right, ConstraintSet.LEFT)

        // match barge top
        blueTeamRedRightC!!.connect(R.id.preMatchBargeTop, ConstraintSet.LEFT, R.id.guide_pre_mid_right, ConstraintSet.RIGHT)
        blueTeamRedRightC!!.connect(R.id.preMatchBargeTop, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
        // match barge bottom
        blueTeamRedRightC!!.connect(R.id.preMatchBargeBot, ConstraintSet.LEFT, R.id.guide_pre_mid_right, ConstraintSet.RIGHT)
        blueTeamRedRightC!!.connect(R.id.preMatchBargeBot, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
    }

    private fun configureRedLeftConstraints() {
        redTeamRedLeftC = ConstraintSet()
        redTeamRedLeftC!!.clone(mainLayout)

        // top
        redTeamRedLeftC!!.connect(R.id.position_top, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        redTeamRedLeftC!!.connect(R.id.position_top, ConstraintSet.RIGHT, R.id.guide_pre_mid_right, ConstraintSet.LEFT)
        // mid top
        redTeamRedLeftC!!.connect(R.id.position_mid_top, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        redTeamRedLeftC!!.connect(R.id.position_mid_top, ConstraintSet.RIGHT, R.id.guide_pre_mid_right, ConstraintSet.LEFT)
        // mid
        redTeamRedLeftC!!.connect(R.id.position_mid, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        redTeamRedLeftC!!.connect(R.id.position_mid, ConstraintSet.RIGHT, R.id.guide_pre_mid_right, ConstraintSet.LEFT)
        // mid bot
        redTeamRedLeftC!!.connect(R.id.position_mid_bot, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        redTeamRedLeftC!!.connect(R.id.position_mid_bot, ConstraintSet.RIGHT, R.id.guide_pre_mid_right, ConstraintSet.LEFT)
        // bot
        redTeamRedLeftC!!.connect(R.id.position_bot, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        redTeamRedLeftC!!.connect(R.id.position_bot, ConstraintSet.RIGHT, R.id.guide_pre_mid_right, ConstraintSet.LEFT)

        // match barge top
        redTeamRedLeftC!!.connect(R.id.preMatchBargeTop, ConstraintSet.LEFT, R.id.guide_pre_mid_right, ConstraintSet.RIGHT)
        redTeamRedLeftC!!.connect(R.id.preMatchBargeTop, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
        // match barge bottom
        redTeamRedLeftC!!.connect(R.id.preMatchBargeBot, ConstraintSet.LEFT, R.id.guide_pre_mid_right, ConstraintSet.RIGHT)
        redTeamRedLeftC!!.connect(R.id.preMatchBargeBot, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
    }

    private fun configureRedRightConstraints() {
        redTeamRedRightC = ConstraintSet()
        redTeamRedRightC!!.clone(mainLayout)

        // top
        redTeamRedRightC!!.connect(R.id.position_top, ConstraintSet.LEFT, R.id.guide_pre_mid_left, ConstraintSet.RIGHT)
        redTeamRedRightC!!.connect(R.id.position_top, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
        // mid top
        redTeamRedRightC!!.connect(R.id.position_mid_top, ConstraintSet.LEFT, R.id.guide_pre_mid_left, ConstraintSet.RIGHT)
        redTeamRedRightC!!.connect(R.id.position_mid_top, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
        // mid
        redTeamRedRightC!!.connect(R.id.position_mid, ConstraintSet.LEFT, R.id.guide_pre_mid_left, ConstraintSet.RIGHT)
        redTeamRedRightC!!.connect(R.id.position_mid, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
        // mid bot
        redTeamRedRightC!!.connect(R.id.position_mid_bot, ConstraintSet.LEFT, R.id.guide_pre_mid_left, ConstraintSet.RIGHT)
        redTeamRedRightC!!.connect(R.id.position_mid_bot, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)
        // bot
        redTeamRedRightC!!.connect(R.id.position_bot, ConstraintSet.LEFT, R.id.guide_pre_mid_left, ConstraintSet.RIGHT)
        redTeamRedRightC!!.connect(R.id.position_bot, ConstraintSet.RIGHT, R.id.guide_pre_right, ConstraintSet.LEFT)

        // match barge top
        redTeamRedRightC!!.connect(R.id.preMatchBargeTop, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        redTeamRedRightC!!.connect(R.id.preMatchBargeTop, ConstraintSet.RIGHT, R.id.guide_pre_mid_left, ConstraintSet.LEFT)
        // match barge bottom
        redTeamRedRightC!!.connect(R.id.preMatchBargeBot, ConstraintSet.LEFT, R.id.guide_pre_left, ConstraintSet.RIGHT)
        redTeamRedRightC!!.connect(R.id.preMatchBargeBot, ConstraintSet.RIGHT, R.id.guide_pre_mid_left, ConstraintSet.LEFT)
    }
}
