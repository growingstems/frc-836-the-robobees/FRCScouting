package org.frc836.yearly

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.core.content.res.ResourcesCompat
import org.frc836.database.MatchStatsStruct
import org.growingstems.scouting.MatchFragment
import org.growingstems.scouting.OnIncrementListener
import org.growingstems.scouting.R

class DefMatchFragment : MatchFragment() {

    private val tempData: MatchStatsStruct = MatchStatsStruct()

    private val green: Int by lazy { ResourcesCompat.getColor(resources, R.color.halfgreen, null)}
    private val filterMode = PorterDuff.Mode.MULTIPLY

    private val oppInc : Button? by lazy { view?.findViewById(R.id.button_def_opponentscoring) }
    private val oppScore : Spinner? by lazy { view?.findViewById(R.id.spinner_def_opponentscoring) }
    private val stealInc : Button? by lazy { view?.findViewById(R.id.button_def_algaestealing) }
    private val stealCount: Spinner? by lazy { view?.findViewById(R.id.spinner_def_algaestealing) }

    private val overBarge: Button? by lazy { view?.findViewById(R.id.button_def_launch_over_barge) }
    private val throughBarge: Button? by lazy { view?.findViewById(R.id.button_def_launch_through_barge) }
    private val stayInBarge: Button? by lazy { view?.findViewById(R.id.button_def_algae_stays_in_barge) }

    companion object {
        @JvmStatic
        fun newInstance(): DefMatchFragment {
            return DefMatchFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_match_def, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getBaseGuiRefs(view)
        displayed = true
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        loadData(tempData)
        setListeners()
    }

    override fun onPause() {
        super.onPause()
        saveData(tempData)
    }

    private fun getBaseGuiRefs(view: View) {
        val adapter = ArrayAdapter.createFromResource(view.context, R.array.numbers, R.layout.spinner_center_item)
        adapter.setDropDownViewResource(R.layout.spinner_center_item)

        oppScore?.adapter = adapter
        stealCount?.adapter = adapter

    }

    private fun setListeners() {
        if (activity == null) return

        oppScore?.let {oppInc?.setOnClickListener(OnIncrementListener(it, 1))}
        stealCount?.let {stealInc?.setOnClickListener(OnIncrementListener(it, 1))}

        overBarge?.setOnClickListener { _ ->
            tempData.can_launch_algae_over_barge = !tempData.can_launch_algae_over_barge
            setDefButtons(tempData)
        }
        throughBarge?.setOnClickListener { _ ->
            tempData.can_propel_algae_through_barge = !tempData.can_propel_algae_through_barge
            setDefButtons(tempData)
        }
        stayInBarge?.setOnClickListener { _ ->
            tempData.can_place_algae_precisely = !tempData.can_place_algae_precisely
            setDefButtons(tempData)
        }
    }

    override fun saveData(data: MatchStatsStruct) {
        if (view == null || !displayed) return

        tempData.scores_allowed_while_def = oppScore?.selectedItemPosition!!
        tempData.algae_stolen_from_opp = stealCount?.selectedItemPosition!!

        MatchStatsYearly.copyDef(tempData, data)
    }

    override fun loadData(data: MatchStatsStruct) {
        tempData.copyFrom(data)
        if (view == null || !displayed || activity == null) return

        oppScore?.setSelection(tempData.scores_allowed_while_def)
        stealCount?.setSelection(tempData.algae_stolen_from_opp)

        setDefButtons(data)
    }

    private fun setDefButtons(data: MatchStatsStruct) {
        if (data.can_launch_algae_over_barge) {
            overBarge?.background?.setColorFilter(green, filterMode)
        } else {
            overBarge?.background?.clearColorFilter()
        }

        if (data.can_propel_algae_through_barge) {
            throughBarge?.background?.setColorFilter(green, filterMode)
        } else {
            throughBarge?.background?.clearColorFilter()
        }

        if (data.can_place_algae_precisely) {
            stayInBarge?.background?.setColorFilter(green, filterMode)
        } else {
            stayInBarge?.background?.clearColorFilter()
        }
    }
}
