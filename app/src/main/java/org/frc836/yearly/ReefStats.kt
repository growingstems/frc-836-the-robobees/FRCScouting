package org.frc836.yearly

import org.frc836.database.MatchStatsStruct

class ReefStats {
    var l4_score: Int = 0
    var l4_miss: Int = 0
    var l3_score: Int = 0
    var l3_miss: Int = 0
    var l2_score: Int = 0
    var l2_miss: Int = 0
    var l1_score: Int = 0
    var l1_miss: Int = 0

    enum class Face {
        PLAYER_CENTER,
        PLAYER_RIGHT,
        PLAYER_LEFT,
        FAR_CENTER,
        FAR_RIGHT,
        FAR_LEFT
    }

    fun loadFromMatchStats(stats: MatchStatsStruct, face: Face, auto: Boolean) {
        if (auto) {
            when (face) {
                Face.PLAYER_CENTER -> {
                    l4_score = stats.auto_player_center_l4_score
                    l4_miss = stats.auto_player_center_l4_miss
                    l3_score = stats.auto_player_center_l3_score
                    l3_miss = stats.auto_player_center_l3_miss
                    l2_score = stats.auto_player_center_l2_score
                    l2_miss = stats.auto_player_center_l2_miss
                    l1_score = stats.auto_player_center_l1_score
                    l1_miss = stats.auto_player_center_l1_miss
                }
                Face.PLAYER_RIGHT -> {
                    l4_score = stats.auto_player_right_l4_score
                    l4_miss = stats.auto_player_right_l4_miss
                    l3_score = stats.auto_player_right_l3_score
                    l3_miss = stats.auto_player_right_l3_miss
                    l2_score = stats.auto_player_right_l2_score
                    l2_miss = stats.auto_player_right_l2_miss
                    l1_score = stats.auto_player_right_l1_score
                    l1_miss = stats.auto_player_right_l1_miss
                }
                Face.PLAYER_LEFT -> {
                    l4_score = stats.auto_player_left_l4_score
                    l4_miss = stats.auto_player_left_l4_miss
                    l3_score = stats.auto_player_left_l3_score
                    l3_miss = stats.auto_player_left_l3_miss
                    l2_score = stats.auto_player_left_l2_score
                    l2_miss = stats.auto_player_left_l2_miss
                    l1_score = stats.auto_player_left_l1_score
                    l1_miss = stats.auto_player_left_l1_miss
                }
                Face.FAR_CENTER -> {
                    l4_score = stats.auto_far_center_l4_score
                    l4_miss = stats.auto_far_center_l4_miss
                    l3_score = stats.auto_far_center_l3_score
                    l3_miss = stats.auto_far_center_l3_miss
                    l2_score = stats.auto_far_center_l2_score
                    l2_miss = stats.auto_far_center_l2_miss
                    l1_score = stats.auto_far_center_l1_score
                    l1_miss = stats.auto_far_center_l1_miss
                }
                Face.FAR_RIGHT -> {
                    l4_score = stats.auto_far_right_l4_score
                    l4_miss = stats.auto_far_right_l4_miss
                    l3_score = stats.auto_far_right_l3_score
                    l3_miss = stats.auto_far_right_l3_miss
                    l2_score = stats.auto_far_right_l2_score
                    l2_miss = stats.auto_far_right_l2_miss
                    l1_score = stats.auto_far_right_l1_score
                    l1_miss = stats.auto_far_right_l1_miss
                }
                Face.FAR_LEFT -> {
                    l4_score = stats.auto_far_left_l4_score
                    l4_miss = stats.auto_far_left_l4_miss
                    l3_score = stats.auto_far_left_l3_score
                    l3_miss = stats.auto_far_left_l3_miss
                    l2_score = stats.auto_far_left_l2_score
                    l2_miss = stats.auto_far_left_l2_miss
                    l1_score = stats.auto_far_left_l1_score
                    l1_miss = stats.auto_far_left_l1_miss
                }
            }
        } else {
            when (face) {
                Face.PLAYER_CENTER -> {
                    l4_score = stats.player_center_l4_score
                    l4_miss = stats.player_center_l4_miss
                    l3_score = stats.player_center_l3_score
                    l3_miss = stats.player_center_l3_miss
                    l2_score = stats.player_center_l2_score
                    l2_miss = stats.player_center_l2_miss
                    l1_score = stats.player_center_l1_score
                    l1_miss = stats.player_center_l1_miss
                }
                Face.PLAYER_RIGHT -> {
                    l4_score = stats.player_right_l4_score
                    l4_miss = stats.player_right_l4_miss
                    l3_score = stats.player_right_l3_score
                    l3_miss = stats.player_right_l3_miss
                    l2_score = stats.player_right_l2_score
                    l2_miss = stats.player_right_l2_miss
                    l1_score = stats.player_right_l1_score
                    l1_miss = stats.player_right_l1_miss
                }
                Face.PLAYER_LEFT -> {
                    l4_score = stats.player_left_l4_score
                    l4_miss = stats.player_left_l4_miss
                    l3_score = stats.player_left_l3_score
                    l3_miss = stats.player_left_l3_miss
                    l2_score = stats.player_left_l2_score
                    l2_miss = stats.player_left_l2_miss
                    l1_score = stats.player_left_l1_score
                    l1_miss = stats.player_left_l1_miss
                }
                Face.FAR_CENTER -> {
                    l4_score = stats.far_center_l4_score
                    l4_miss = stats.far_center_l4_miss
                    l3_score = stats.far_center_l3_score
                    l3_miss = stats.far_center_l3_miss
                    l2_score = stats.far_center_l2_score
                    l2_miss = stats.far_center_l2_miss
                    l1_score = stats.far_center_l1_score
                    l1_miss = stats.far_center_l1_miss
                }
                Face.FAR_RIGHT -> {
                    l4_score = stats.far_right_l4_score
                    l4_miss = stats.far_right_l4_miss
                    l3_score = stats.far_right_l3_score
                    l3_miss = stats.far_right_l3_miss
                    l2_score = stats.far_right_l2_score
                    l2_miss = stats.far_right_l2_miss
                    l1_score = stats.far_right_l1_score
                    l1_miss = stats.far_right_l1_miss
                }
                Face.FAR_LEFT -> {
                    l4_score = stats.far_left_l4_score
                    l4_miss = stats.far_left_l4_miss
                    l3_score = stats.far_left_l3_score
                    l3_miss = stats.far_left_l3_miss
                    l2_score = stats.far_left_l2_score
                    l2_miss = stats.far_left_l2_miss
                    l1_score = stats.far_left_l1_score
                    l1_miss = stats.far_left_l1_miss
                }
            }
        }
    }

    fun saveToMatchStats(stats: MatchStatsStruct, face: Face, auto: Boolean) {
        if (auto) {
            when (face) {
                Face.PLAYER_CENTER -> {
                    stats.auto_player_center_l4_score = l4_score
                    stats.auto_player_center_l4_miss = l4_miss
                    stats.auto_player_center_l3_score = l3_score
                    stats.auto_player_center_l3_miss = l3_miss
                    stats.auto_player_center_l2_score = l2_score
                    stats.auto_player_center_l2_miss = l2_miss
                    stats.auto_player_center_l1_score = l1_score
                    stats.auto_player_center_l1_miss = l1_miss
                }
                Face.PLAYER_RIGHT -> {
                    stats.auto_player_right_l4_score = l4_score
                    stats.auto_player_right_l4_miss = l4_miss
                    stats.auto_player_right_l3_score = l3_score
                    stats.auto_player_right_l3_miss = l3_miss
                    stats.auto_player_right_l2_score = l2_score
                    stats.auto_player_right_l2_miss = l2_miss
                    stats.auto_player_right_l1_score = l1_score
                    stats.auto_player_right_l1_miss = l1_miss
                }
                Face.PLAYER_LEFT -> {
                    stats.auto_player_left_l4_score = l4_score
                    stats.auto_player_left_l4_miss = l4_miss
                    stats.auto_player_left_l3_score = l3_score
                    stats.auto_player_left_l3_miss = l3_miss
                    stats.auto_player_left_l2_score = l2_score
                    stats.auto_player_left_l2_miss = l2_miss
                    stats.auto_player_left_l1_score = l1_score
                    stats.auto_player_left_l1_miss = l1_miss
                }
                Face.FAR_CENTER -> {
                    stats.auto_far_center_l4_score = l4_score
                    stats.auto_far_center_l4_miss = l4_miss
                    stats.auto_far_center_l3_score = l3_score
                    stats.auto_far_center_l3_miss = l3_miss
                    stats.auto_far_center_l2_score = l2_score
                    stats.auto_far_center_l2_miss = l2_miss
                    stats.auto_far_center_l1_score = l1_score
                    stats.auto_far_center_l1_miss = l1_miss
                }
                Face.FAR_RIGHT -> {
                    stats.auto_far_right_l4_score = l4_score
                    stats.auto_far_right_l4_miss = l4_miss
                    stats.auto_far_right_l3_score = l3_score
                    stats.auto_far_right_l3_miss = l3_miss
                    stats.auto_far_right_l2_score = l2_score
                    stats.auto_far_right_l2_miss = l2_miss
                    stats.auto_far_right_l1_score = l1_score
                    stats.auto_far_right_l1_miss = l1_miss
                }
                Face.FAR_LEFT -> {
                    stats.auto_far_left_l4_score = l4_score
                    stats.auto_far_left_l4_miss = l4_miss
                    stats.auto_far_left_l3_score = l3_score
                    stats.auto_far_left_l3_miss = l3_miss
                    stats.auto_far_left_l2_score = l2_score
                    stats.auto_far_left_l2_miss = l2_miss
                    stats.auto_far_left_l1_score = l1_score
                    stats.auto_far_left_l1_miss = l1_miss
                }
            }
        } else {
            when (face) {
                Face.PLAYER_CENTER -> {
                    stats.player_center_l4_score = l4_score
                    stats.player_center_l4_miss = l4_miss
                    stats.player_center_l3_score = l3_score
                    stats.player_center_l3_miss = l3_miss
                    stats.player_center_l2_score = l2_score
                    stats.player_center_l2_miss = l2_miss
                    stats.player_center_l1_score = l1_score
                    stats.player_center_l1_miss = l1_miss
                }
                Face.PLAYER_RIGHT -> {
                    stats.player_right_l4_score = l4_score
                    stats.player_right_l4_miss = l4_miss
                    stats.player_right_l3_score = l3_score
                    stats.player_right_l3_miss = l3_miss
                    stats.player_right_l2_score = l2_score
                    stats.player_right_l2_miss = l2_miss
                    stats.player_right_l1_score = l1_score
                    stats.player_right_l1_miss = l1_miss
                }
                Face.PLAYER_LEFT -> {
                    stats.player_left_l4_score = l4_score
                    stats.player_left_l4_miss = l4_miss
                    stats.player_left_l3_score = l3_score
                    stats.player_left_l3_miss = l3_miss
                    stats.player_left_l2_score = l2_score
                    stats.player_left_l2_miss = l2_miss
                    stats.player_left_l1_score = l1_score
                    stats.player_left_l1_miss = l1_miss
                }
                Face.FAR_CENTER -> {
                    stats.far_center_l4_score = l4_score
                    stats.far_center_l4_miss = l4_miss
                    stats.far_center_l3_score = l3_score
                    stats.far_center_l3_miss = l3_miss
                    stats.far_center_l2_score = l2_score
                    stats.far_center_l2_miss = l2_miss
                    stats.far_center_l1_score = l1_score
                    stats.far_center_l1_miss = l1_miss
                }
                Face.FAR_RIGHT -> {
                    stats.far_right_l4_score = l4_score
                    stats.far_right_l4_miss = l4_miss
                    stats.far_right_l3_score = l3_score
                    stats.far_right_l3_miss = l3_miss
                    stats.far_right_l2_score = l2_score
                    stats.far_right_l2_miss = l2_miss
                    stats.far_right_l1_score = l1_score
                    stats.far_right_l1_miss = l1_miss
                }
                Face.FAR_LEFT -> {
                    stats.far_left_l4_score = l4_score
                    stats.far_left_l4_miss = l4_miss
                    stats.far_left_l3_score = l3_score
                    stats.far_left_l3_miss = l3_miss
                    stats.far_left_l2_score = l2_score
                    stats.far_left_l2_miss = l2_miss
                    stats.far_left_l1_score = l1_score
                    stats.far_left_l1_miss = l1_miss
                }
            }
        }
    }

    fun containsData() : Boolean {
        return l4_score > 0
            || l4_miss > 0
            || l3_score > 0
            || l3_miss > 0
            || l2_score > 0
            || l2_miss > 0
            || l1_score > 0
            || l1_miss > 0
    }
}
