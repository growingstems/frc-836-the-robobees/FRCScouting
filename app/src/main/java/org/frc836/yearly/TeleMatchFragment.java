/*
 * Copyright 2016 Daniel Logan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.frc836.yearly;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.res.ResourcesCompat;

import org.frc836.database.MatchStatsStruct;
import org.growingstems.scouting.MatchFragment;
import org.growingstems.scouting.OnIncrementListener;
import org.growingstems.scouting.Prefs;
import org.growingstems.scouting.R;

import java.util.HashMap;


public class TeleMatchFragment extends MatchFragment {

    private final MatchStatsStruct tempData = new MatchStatsStruct();

    private int red;
    private int green;
    private final PorterDuff.Mode filterMode = PorterDuff.Mode.MULTIPLY;

    /*
    TODO: Still missing
    processor_miss
    net_miss
     */

    private ImageView reef_player_center;
    private ImageView reef_player_left;
    private ImageView reef_player_right;
    private ImageView reef_far_left;
    private ImageView reef_far_center;
    private ImageView reef_far_right;

    private final ReefStats reef_player_center_stats = new ReefStats();
    private final ReefStats reef_player_left_stats = new ReefStats();
    private final ReefStats reef_player_right_stats = new ReefStats();
    private final ReefStats reef_far_center_stats = new ReefStats();
    private final ReefStats reef_far_left_stats = new ReefStats();
    private final ReefStats reef_far_right_stats = new ReefStats();

    private Button proc_inc;
    private Spinner proc_score;
    private Button net_inc;
    private Spinner net_score;

    private Button l2_algae_inc;
    private Spinner l2_algae_count;
    private Button l3_algae_inc;
    private Spinner l3_algae_count;

    private Button algae_control;
    private Button algae_stolen;

    private final HashMap<ReefStats.Face, Integer> deselect_faces = new HashMap<>(6);
    private final HashMap<ReefStats.Face, Integer> data_faces = new HashMap<>(6);

    private static final String dialog_transaction_name = "TELE_REEF";

    public TeleMatchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PreMatch.
     */
    public static TeleMatchFragment newInstance() {
        return new TeleMatchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_match_tele, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        getBaseGUIRefs(view);
        setDisplayed(true);
        //Don't call setListeners here, as it needs to be called after the side is determined
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData(tempData);
    }

    public void onPause() {
        super.onPause();
        saveData(tempData);
    }

    @Override
    public void saveData(@NonNull MatchStatsStruct data) {
        if (getView() == null || !getDisplayed()) return;

        tempData.processor_score = proc_score.getSelectedItemPosition();
        tempData.net_score = net_score.getSelectedItemPosition();
        tempData.l2_algae_removed = l2_algae_count.getSelectedItemPosition();
        tempData.l3_algae_removed = l3_algae_count.getSelectedItemPosition();

        reef_player_center_stats.saveToMatchStats(tempData, ReefStats.Face.PLAYER_CENTER, false);
        reef_player_left_stats.saveToMatchStats(tempData, ReefStats.Face.PLAYER_LEFT, false);
        reef_player_right_stats.saveToMatchStats(tempData, ReefStats.Face.PLAYER_RIGHT, false);
        reef_far_center_stats.saveToMatchStats(tempData, ReefStats.Face.FAR_CENTER, false);
        reef_far_left_stats.saveToMatchStats(tempData, ReefStats.Face.FAR_LEFT, false);
        reef_far_right_stats.saveToMatchStats(tempData, ReefStats.Face.FAR_RIGHT, false);

        MatchStatsYearly.copyTele(tempData, data);
    }

    @Override
    public void loadData(@NonNull MatchStatsStruct data) {
        tempData.copyFrom(data);
        View view = getView();
        if (view == null || !getDisplayed()) return;
        updateSide(view);
        setListeners();

        Activity act = getActivity();
        if (act == null) return;


        reef_player_center_stats.loadFromMatchStats(tempData, ReefStats.Face.PLAYER_CENTER, false);
        reef_player_left_stats.loadFromMatchStats(tempData, ReefStats.Face.PLAYER_LEFT, false);
        reef_player_right_stats.loadFromMatchStats(tempData, ReefStats.Face.PLAYER_RIGHT, false);
        reef_far_center_stats.loadFromMatchStats(tempData, ReefStats.Face.FAR_CENTER, false);
        reef_far_left_stats.loadFromMatchStats(tempData, ReefStats.Face.FAR_LEFT, false);
        reef_far_right_stats.loadFromMatchStats(tempData, ReefStats.Face.FAR_RIGHT, false);

        proc_score.setSelection(tempData.processor_score);
        net_score.setSelection(tempData.net_score);
        l2_algae_count.setSelection(tempData.l2_algae_removed);
        l3_algae_count.setSelection(tempData.l3_algae_removed);
        setReefFaces();

        setAlgaeButtons(data);
    }

    private void updateSide(View view) {
        Activity act = getActivity();
        boolean redLeft = Prefs.getRedLeft(act, true);
        String pos;
        if (act instanceof MatchActivity) pos = ((MatchActivity) act).getPosition();
        else pos = Prefs.getPosition(act, "Red 1");


        if ((pos.contains("Blue") && !redLeft) || ((!pos.contains("Blue") && redLeft))) {
            //Left side layout

            reef_player_center = view.findViewById(R.id.tele_reef_left);
            reef_player_left = view.findViewById(R.id.tele_reef_top_left);
            reef_player_right = view.findViewById(R.id.tele_reef_bot_left);
            reef_far_center = view.findViewById(R.id.tele_reef_right);
            reef_far_left = view.findViewById(R.id.tele_reef_top_right);
            reef_far_right = view.findViewById(R.id.tele_reef_bot_right);

            deselect_faces.put(ReefStats.Face.PLAYER_CENTER, R.drawable.left_deselect);
            deselect_faces.put(ReefStats.Face.PLAYER_LEFT, R.drawable.top_left_deselect);
            deselect_faces.put(ReefStats.Face.PLAYER_RIGHT, R.drawable.bot_left_deselect);
            deselect_faces.put(ReefStats.Face.FAR_CENTER, R.drawable.right_deselect);
            deselect_faces.put(ReefStats.Face.FAR_LEFT, R.drawable.top_right_deselect);
            deselect_faces.put(ReefStats.Face.FAR_RIGHT, R.drawable.bot_right_deselect);
            data_faces.put(ReefStats.Face.PLAYER_CENTER, R.drawable.left_data);
            data_faces.put(ReefStats.Face.PLAYER_LEFT, R.drawable.top_left_data);
            data_faces.put(ReefStats.Face.PLAYER_RIGHT, R.drawable.bot_left_data);
            data_faces.put(ReefStats.Face.FAR_CENTER, R.drawable.right_data);
            data_faces.put(ReefStats.Face.FAR_LEFT, R.drawable.top_right_data);
            data_faces.put(ReefStats.Face.FAR_RIGHT, R.drawable.bot_right_data);
        } else {
            //Right side layout

            reef_player_center = view.findViewById(R.id.tele_reef_right);
            reef_player_left = view.findViewById(R.id.tele_reef_bot_right);
            reef_player_right = view.findViewById(R.id.tele_reef_top_right);
            reef_far_center = view.findViewById(R.id.tele_reef_left);
            reef_far_left = view.findViewById(R.id.tele_reef_bot_left);
            reef_far_right = view.findViewById(R.id.tele_reef_top_left);

            deselect_faces.put(ReefStats.Face.PLAYER_CENTER, R.drawable.right_deselect);
            deselect_faces.put(ReefStats.Face.PLAYER_LEFT, R.drawable.bot_right_deselect);
            deselect_faces.put(ReefStats.Face.PLAYER_RIGHT, R.drawable.top_right_deselect);
            deselect_faces.put(ReefStats.Face.FAR_CENTER, R.drawable.left_deselect);
            deselect_faces.put(ReefStats.Face.FAR_LEFT, R.drawable.bot_left_deselect);
            deselect_faces.put(ReefStats.Face.FAR_RIGHT, R.drawable.top_left_deselect);
            data_faces.put(ReefStats.Face.PLAYER_CENTER, R.drawable.right_data);
            data_faces.put(ReefStats.Face.PLAYER_LEFT, R.drawable.bot_right_data);
            data_faces.put(ReefStats.Face.PLAYER_RIGHT, R.drawable.top_right_data);
            data_faces.put(ReefStats.Face.FAR_CENTER, R.drawable.left_data);
            data_faces.put(ReefStats.Face.FAR_LEFT, R.drawable.bot_left_data);
            data_faces.put(ReefStats.Face.FAR_RIGHT, R.drawable.top_left_data);
        }
    }

    private void getBaseGUIRefs(View view) {
        green = ResourcesCompat.getColor(getResources(), R.color.halfgreen, null);
        red = ResourcesCompat.getColor(getResources(), R.color.halfred, null);

        proc_inc = view.findViewById(R.id.button_tele_processor_score_increment);
        proc_score = view.findViewById(R.id.spinner_tele_processor_score);
        net_inc = view.findViewById(R.id.button_tele_net_score_increment);
        net_score = view.findViewById(R.id.spinner_tele_net_score);

        l2_algae_inc = view.findViewById(R.id.button_tele_algae_removal_L2_increment);
        l2_algae_count = view.findViewById(R.id.spinner_tele_algae_removal_L2);
        l3_algae_inc = view.findViewById(R.id.button_tele_algae_removal_L3_increment);
        l3_algae_count = view.findViewById(R.id.spinner_tele_algae_removal_L3);

        algae_control = view.findViewById(R.id.button_tele_algae_control);
        algae_stolen = view.findViewById(R.id.button_tele_algae_stolen);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(), R.array.numbers, R.layout.spinner_center_item);
        // set whatever dropdown resource you want
        adapter.setDropDownViewResource(R.layout.spinner_center_item);
        proc_score.setAdapter(adapter);
        net_score.setAdapter(adapter);
        l2_algae_count.setAdapter(adapter);
        l3_algae_count.setAdapter(adapter);
    }

    private void setListeners() {

        Activity act = getActivity();
        if (act == null) return;

        proc_inc.setOnClickListener(new OnIncrementListener(proc_score, 1));
        net_inc.setOnClickListener(new OnIncrementListener(net_score, 1));
        l2_algae_inc.setOnClickListener(new OnIncrementListener(l2_algae_count, 1));
        l3_algae_inc.setOnClickListener(new OnIncrementListener(l3_algae_count, 1));

        algae_control.setOnClickListener(v -> {
            tempData.controlled_algae_removal = !tempData.controlled_algae_removal;
            setAlgaeButtons(tempData);
        });
        algae_stolen.setOnClickListener(v -> {
            tempData.algae_stolen_by_opp = !tempData.algae_stolen_by_opp;
            setAlgaeButtons(tempData);
        });

        reef_far_left.setOnClickListener(v -> {
            new ReefDialogFragment(reef_far_left_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
        reef_far_center.setOnClickListener(v -> {
            new ReefDialogFragment(reef_far_center_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
        reef_far_right.setOnClickListener(v -> {
            new ReefDialogFragment(reef_far_right_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
        reef_player_left.setOnClickListener(v -> {
            new ReefDialogFragment(reef_player_left_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
        reef_player_center.setOnClickListener(v -> {
            new ReefDialogFragment(reef_player_center_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
        reef_player_right.setOnClickListener(v -> {
            new ReefDialogFragment(reef_player_right_stats, this::setReefFaces).show(getChildFragmentManager(), dialog_transaction_name);
        });
    }

    private void setReefFaces() {
        reef_player_center.setBackgroundResource(reef_player_center_stats.containsData() ? data_faces.get(ReefStats.Face.PLAYER_CENTER) : deselect_faces.get(ReefStats.Face.PLAYER_CENTER));
        reef_player_left.setBackgroundResource(reef_player_left_stats.containsData() ? data_faces.get(ReefStats.Face.PLAYER_LEFT) : deselect_faces.get(ReefStats.Face.PLAYER_LEFT));
        reef_player_right.setBackgroundResource(reef_player_right_stats.containsData() ? data_faces.get(ReefStats.Face.PLAYER_RIGHT) : deselect_faces.get(ReefStats.Face.PLAYER_RIGHT));
        reef_far_center.setBackgroundResource(reef_far_center_stats.containsData() ? data_faces.get(ReefStats.Face.FAR_CENTER) : deselect_faces.get(ReefStats.Face.FAR_CENTER));
        reef_far_left.setBackgroundResource(reef_far_left_stats.containsData() ? data_faces.get(ReefStats.Face.FAR_LEFT) : deselect_faces.get(ReefStats.Face.FAR_LEFT));
        reef_far_right.setBackgroundResource(reef_far_right_stats.containsData() ? data_faces.get(ReefStats.Face.FAR_RIGHT) : deselect_faces.get(ReefStats.Face.FAR_RIGHT));

    }

    private void setAlgaeButtons(MatchStatsStruct stats) {
        if (stats.controlled_algae_removal) {
            algae_control.getBackground().setColorFilter(green, filterMode);
        } else {
            algae_control.getBackground().clearColorFilter();
        }

        if (stats.algae_stolen_by_opp) {
            algae_stolen.getBackground().setColorFilter(red, filterMode);
        } else {
            algae_stolen.getBackground().clearColorFilter();
        }
    }
}
