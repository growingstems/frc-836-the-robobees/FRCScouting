package org.frc836.yearly

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Spinner
import androidx.fragment.app.DialogFragment
import org.growingstems.scouting.OnIncrementListener
import org.growingstems.scouting.R

class ReefDialogFragment(val stats: ReefStats, val callback: Runnable?) : DialogFragment() {

    private var view: View? = null
    private val l4_score: Spinner? by lazy { view?.findViewById(R.id.spinner_reef_L4_score) }
    private val l4_miss: Spinner? by lazy { view?.findViewById(R.id.spinner_reef_L4_miss) }
    private val l3_score: Spinner? by lazy { view?.findViewById(R.id.spinner_reef_L3_score) }
    private val l3_miss: Spinner? by lazy { view?.findViewById(R.id.spinner_reef_L3_miss) }
    private val l2_score: Spinner? by lazy { view?.findViewById(R.id.spinner_reef_L2_score) }
    private val l2_miss: Spinner? by lazy { view?.findViewById(R.id.spinner_reef_L2_miss) }
    private val l1_score: Spinner? by lazy { view?.findViewById(R.id.spinner_reef_L1_score) }
    private val l1_miss: Spinner? by lazy { view?.findViewById(R.id.spinner_reef_L1_miss) }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater;
            view = inflater.inflate(R.layout.fragment_match_reef, null)
            builder.setView(view)
                .setPositiveButton(R.string.ok) { _: DialogInterface, _: Int ->
                    stats.l4_score = l4_score?.selectedItemPosition!!
                    stats.l4_miss = l4_miss?.selectedItemPosition!!
                    stats.l3_score = l3_score?.selectedItemPosition!!
                    stats.l3_miss = l3_miss?.selectedItemPosition!!
                    stats.l2_score = l2_score?.selectedItemPosition!!
                    stats.l2_miss = l2_miss?.selectedItemPosition!!
                    stats.l1_score = l1_score?.selectedItemPosition!!
                    stats.l1_miss = l1_miss?.selectedItemPosition!!
                    callback?.run()
                }
                .setNegativeButton(R.string.cancel) { _: DialogInterface, _: Int -> callback?.run() }

            l4_score?.let { it1 ->
                view?.findViewById<Button>(R.id.button_reef_L4_score_increment)?.setOnClickListener(
                    OnIncrementListener(it1, 1)
                )
            }
            l4_miss?.let { it1 ->
                view?.findViewById<Button>(R.id.button_reef_L4_miss_increment)?.setOnClickListener(
                    OnIncrementListener(it1, 1)
                )
            }
            l3_score?.let { it1 ->
                view?.findViewById<Button>(R.id.button_reef_L3_score_increment)?.setOnClickListener(
                    OnIncrementListener(it1, 1)
                )
            }
            l3_miss?.let { it1 ->
                view?.findViewById<Button>(R.id.button_reef_L3_miss_increment)?.setOnClickListener(
                    OnIncrementListener(it1, 1)
                )
            }
            l2_score?.let { it1 ->
                view?.findViewById<Button>(R.id.button_reef_L2_score_increment)?.setOnClickListener(
                    OnIncrementListener(it1, 1)
                )
            }
            l2_miss?.let { it1 ->
                view?.findViewById<Button>(R.id.button_reef_L2_miss_increment)?.setOnClickListener(
                    OnIncrementListener(it1, 1)
                )
            }
            l1_score?.let { it1 ->
                view?.findViewById<Button>(R.id.button_reef_L1_score_increment)?.setOnClickListener(
                    OnIncrementListener(it1, 1)
                )
            }
            l1_miss?.let { it1 ->
                view?.findViewById<Button>(R.id.button_reef_L1_miss_increment)?.setOnClickListener(
                    OnIncrementListener(it1, 1)
                )
            }

            l4_score?.setSelection(stats.l4_score)
            l4_miss?.setSelection(stats.l4_miss)
            l3_score?.setSelection(stats.l3_score)
            l3_miss?.setSelection(stats.l3_miss)
            l2_score?.setSelection(stats.l2_score)
            l2_miss?.setSelection(stats.l2_miss)
            l1_score?.setSelection(stats.l1_score)
            l1_miss?.setSelection(stats.l1_miss)

            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}
