/*
 * Copyright 2016 Daniel Logan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.frc836.database;

import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaScannerConnection;
import android.os.AsyncTask;
import android.util.Pair;
import android.util.SparseArray;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.frc836.database.DBSyncService.LocalBinder;
import org.frc836.samsung.fileselector.FileOperation;
import org.frc836.samsung.fileselector.FileSelector;
import org.frc836.samsung.fileselector.OnHandleFileListener;
import org.growingstems.scouting.Prefs;
import org.growingstems.scouting.R;
import org.growingstems.scouting.ScoutingMenuActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class DB {

    public static final boolean debug = false;
    // kinda dangerous, but we are assuming that the timestamp field will always
    // have the same name for all tables
    public static final String COLUMN_NAME_TIMESTAMP = PitStats.COLUMN_NAME_TIMESTAMP;

    private RequestQueue reqQueue = null;
    private String password;
    private Context context;
    private LocalBinder binder;

    public static final SimpleDateFormat dateParser = new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss.sss", Locale.US);

    public enum RequestType {None, Matches}

    @SuppressWarnings("unused")
    private DB() {
    }

    public DB(Context context, String pass, LocalBinder binder) {
        password = pass;
        this.context = context;
        reqQueue = Volley.newRequestQueue(context);
        reqQueue.start();
        //initialize the database if it isn't created already
        SQLiteDatabase tmp = ScoutingDBHelper.getInstance(context.getApplicationContext()).getReadableDatabase();
        tmp.close();
        this.binder = binder;
    }

    public DB(Context context, LocalBinder binder) {
        this.context = context;
        reqQueue = Volley.newRequestQueue(context);
        reqQueue.start();
        password = Prefs.getSavedPassword(context);
        //initialize the database if it isn't created already
        SQLiteDatabase tmp = ScoutingDBHelper.getInstance(context.getApplicationContext()).getReadableDatabase();
        tmp.close();
        this.binder = binder;
    }

    public void setBinder(LocalBinder binder) {
        this.binder = binder;
    }

    protected static Map<String, String> getPostData(ContentValues values) {
        Map<String, String> data = new HashMap<String, String>();
        for (String key : values.keySet()) {
            data.put(key, values.getAsString(key));
        }
        return data;
    }

    public void startSync() {
        startSync(null);
    }

    public void startSync(SyncCallback callback) {
        if (binder != null) {
            binder.setCallback(callback);
            binder.setPassword(password);
            binder.startSync();
        }
    }

    private void insertOrUpdate(String table, String nullColumnHack,
                                ContentValues values, String idColumnName, String whereClause,
                                String[] whereArgs) {
        synchronized (ScoutingDBHelper.lock) {
            SQLiteDatabase db = ScoutingDBHelper.getInstance()
                .getWritableDatabase();

            String[] projection = {idColumnName};

            try (Cursor c = db.query(table, projection, whereClause, whereArgs,
                null, null, null, "0,1")) {
                if (c.moveToFirst()) {
                    String[] id = {c.getString(c
                        .getColumnIndexOrThrow(idColumnName))};
                    values.put(COLUMN_NAME_TIMESTAMP,
                        dateParser.format(new Date()));
                    db.update(table, values, idColumnName + "=?", id);
                } else {
                    db.insert(table, nullColumnHack, values);
                }
            } catch (Exception e) {
                //Do nothing
            } finally {
                ScoutingDBHelper.getInstance().close();
            }
        }
    }

    public boolean submitMatch(MatchStatsStruct teamData) {
        try {
            String where = MatchStatsStruct.COLUMN_NAME_EVENT_ID + "=? AND "
                + MatchStatsStruct.COLUMN_NAME_MATCH_ID + "=? AND "
                + MatchStatsStruct.COLUMN_NAME_TEAM_ID + "=? AND "
                + MatchStatsStruct.COLUMN_NAME_PRACTICE_MATCH + "=?";
            ContentValues values;
            synchronized (ScoutingDBHelper.lock) {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                values = teamData.getValues(db);

                ScoutingDBHelper.getInstance().close();
            }
            String[] whereArgs = {
                values.getAsString(MatchStatsStruct.COLUMN_NAME_EVENT_ID),
                values.getAsString(MatchStatsStruct.COLUMN_NAME_MATCH_ID),
                values.getAsString(MatchStatsStruct.COLUMN_NAME_TEAM_ID),
                values.getAsString(MatchStatsStruct.COLUMN_NAME_PRACTICE_MATCH)};

            insertOrUpdate(MatchStatsStruct.TABLE_NAME, null, values,
                MatchStatsStruct.COLUMN_NAME_ID, where, whereArgs);

            startSync();

            return true;

        } catch (Exception e) {
            return false;
        }

    }

    public boolean submitSuperScout(SuperScoutStats team1Data, SuperScoutStats team2Data, SuperScoutStats team3Data) {
        if (submitIndividualSuperScout(team1Data) && submitIndividualSuperScout(team2Data) && submitIndividualSuperScout(team3Data)) {
            startSync();
            return true;
        }
        return false;
    }

    private boolean submitIndividualSuperScout(SuperScoutStats teamData) {
        try {
            String where = SuperScoutStats.COLUMN_NAME_EVENT_ID + "=? AND "
                + SuperScoutStats.COLUMN_NAME_MATCH_ID + "=? AND "
                + SuperScoutStats.COLUMN_NAME_TEAM_ID + "=? AND "
                + SuperScoutStats.COLUMN_NAME_PRACTICE_MATCH + "=?";
            ContentValues values;
            synchronized (ScoutingDBHelper.lock) {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                values = teamData.getValues(db);

                ScoutingDBHelper.getInstance().close();
            }
            String[] whereArgs = {
                values.getAsString(SuperScoutStats.COLUMN_NAME_EVENT_ID),
                values.getAsString(SuperScoutStats.COLUMN_NAME_MATCH_ID),
                values.getAsString(SuperScoutStats.COLUMN_NAME_TEAM_ID),
                values.getAsString(SuperScoutStats.COLUMN_NAME_PRACTICE_MATCH)};

            insertOrUpdate(SuperScoutStats.TABLE_NAME, null, values,
                SuperScoutStats.COLUMN_NAME_ID, where, whereArgs);

            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public boolean submitPits(PitStats stats) {
        try {
            String where = PitStats.COLUMN_NAME_TEAM_ID + "=? AND "
                + PitStats.COLUMN_NAME_EVENT_ID + "=? AND "
                + PitStats.COLUMN_NAME_PASS_NUMBER + "=?";
            ContentValues values;
            synchronized (ScoutingDBHelper.lock) {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getWritableDatabase();
                values = stats.getValues(db);
                ScoutingDBHelper.getInstance().close();
            }

            String[] whereArgs = {values.getAsString(PitStats.COLUMN_NAME_TEAM_ID),
                values.getAsString(PitStats.COLUMN_NAME_EVENT_ID),
                values.getAsString(PitStats.COLUMN_NAME_PASS_NUMBER)};

            insertOrUpdate(PitStats.TABLE_NAME, null, values,
                PitStats.COLUMN_NAME_ID, where, whereArgs);

            startSync();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void setPass(String pass) {
        password = pass;
    }

    public void checkPass(String pass, HttpCallback callback) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("password", pass);
        params.put("type", "passConfirm");

        String url = Prefs.getScoutingURLNoDefault(context);

        if (url.length() > 1) {
            reqQueue.add(new StringRequest(Request.Method.POST, Prefs.getScoutingURLNoDefault(context), callback::onResponse, callback::onError) {
                @Override
                protected Map<String, String> getParams() {
                    return params;
                }
            });
        } else {
            callback.onError(new VolleyError("Invalid URL"));
        }
    }

    public void checkVersion(HttpCallback callback) {
        Map<String, String> args = new HashMap<String, String>();
        args.put("type", "versioncheck");

        String url = Prefs.getScoutingURLNoDefault(context);
        if (url.length() > 1) {
            reqQueue.add(new StringRequest(Request.Method.POST, Prefs.getScoutingURLNoDefault(context), callback::onResponse, callback::onError) {
                @Override
                protected Map<String, String> getParams() {
                    return args;
                }
            });
        } else {
            callback.onError(new VolleyError("Invalid URL"));
        }
    }

    public Pair<String, PitStats> getTeamPitInfo(String teamNum, String eventName, int passNum) {

        synchronized (ScoutingDBHelper.lock) {
            try {

                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();
                String date = "";

                String[] temp = PitStats.getProjection();
                String[] projection = Arrays.copyOf(temp, temp.length + 1);
                projection[temp.length] = PitStats.COLUMN_NAME_TIMESTAMP;
                String[] where = {teamNum, String.valueOf(getEventIdFromName(eventName, db)), String.valueOf(passNum)};

                PitStats stats = new PitStats();

                try (Cursor c = db.query(PitStats.TABLE_NAME, // from the
                    // scout_pit_data
                    // table
                    projection, // select
                    PitStats.COLUMN_NAME_TEAM_ID + "=? AND "
                    + PitStats.COLUMN_NAME_EVENT_ID + "=? AND "
                    + PitStats.COLUMN_NAME_PASS_NUMBER + "=?", // where
                    where,
                    null, // don't group
                    null, // don't filter
                    null, // don't order
                    "0,1") // limit to 1
                ) {

                    stats.fromCursor(c, this, db);

                    date = c.getString(c
                        .getColumnIndexOrThrow(PitStats.COLUMN_NAME_TIMESTAMP));
                } catch (Exception e) {
                    //Do nothing
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }
                return new Pair<>(date, stats);

            } catch (Exception e) {
                return null;
            }
        }
    }

    public List<String> getEventList() {

        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_NAME};
                List<String> ret;

                try (Cursor c = db.query(FRCScoutingContract.EVENT_LU_Entry.TABLE_NAME, projection,
                    null, null, null, null, FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_ID)) {

                    ret = new ArrayList<String>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            ret.add(c.getString(c
                                .getColumnIndexOrThrow(FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_NAME)));
                        } while (c.moveToNext());
                    else
                        ret = null;
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }

                return ret;
            } catch (Exception e) {
                return null;
            }
        }
    }
    /**
    public List<String> getProgrammingList() {

        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {FRCScoutingContract.PROGRAMMING_LU_Entry.COLUMN_NAME_LANGUAGE_NAME};

                List<String> ret;

                try (Cursor c = db.query(FRCScoutingContract.PROGRAMMING_LU_Entry.TABLE_NAME, projection,
                    null, null, null, null,
                    FRCScoutingContract.PROGRAMMING_LU_Entry.COLUMN_NAME_ID)) {

                    ret = new ArrayList<String>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            ret.add(c.getString(c
                                .getColumnIndexOrThrow(FRCScoutingContract.PROGRAMMING_LU_Entry.COLUMN_NAME_LANGUAGE_NAME)));
                        } while (c.moveToNext());
                    else
                        ret = null;
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }

                return ret;
            } catch (Exception e) {
                return null;
            }
        }

    }
    */

    public static SortedMap<Integer, String> getDegradeList() {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance().getReadableDatabase();

                String[] projection = {FRCScoutingContract.DEGRADE_LU_Entry.COLUMN_NAME_INTVALUE,
                    FRCScoutingContract.DEGRADE_LU_Entry.COLUMN_NAME_STRINGVAL};
                SortedMap<Integer, String> ret;

                try (Cursor c = db.query(FRCScoutingContract.DEGRADE_LU_Entry.TABLE_NAME, projection,
                    null, null, null, null,
                    FRCScoutingContract.DEGRADE_LU_Entry.COLUMN_NAME_ID)) {

                    ret = new TreeMap<>();

                    if (c.moveToFirst()) {
                        do {
                            ret.put(c.getInt(c.getColumnIndexOrThrow(FRCScoutingContract.DEGRADE_LU_Entry.COLUMN_NAME_INTVALUE)),
                                c.getString(c.getColumnIndexOrThrow(FRCScoutingContract.DEGRADE_LU_Entry.COLUMN_NAME_STRINGVAL)));
                        } while (c.moveToNext());
                    } else {
                        ret = null;
                    }
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }
                return ret;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static List<String> getDriveList() {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance().getReadableDatabase();

                String[] projection = {FRCScoutingContract.DRIVE_LU_Entry.COLUMN_NAME_DRIVE_NAME};

                List<String> ret;

                try (Cursor c = db.query(FRCScoutingContract.DRIVE_LU_Entry.TABLE_NAME, projection,
                    null, null, null, null,
                    FRCScoutingContract.DRIVE_LU_Entry.COLUMN_NAME_ID)) {

                    ret = new ArrayList<>(c.getCount());

                    if (c.moveToFirst()) {
                        do {
                            ret.add(c.getString(c.getColumnIndexOrThrow(FRCScoutingContract.DRIVE_LU_Entry.COLUMN_NAME_DRIVE_NAME)));
                        } while (c.moveToNext());
                    } else {
                        ret = null;
                    }
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }
                return ret;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static List<String> getCgList() {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance().getReadableDatabase();

                String[] projection = {FRCScoutingContract.ROBOT_CG_LU_Entry.COLUMN_NAME_CG_TYPE};

                List<String> ret;

                try(Cursor c = db.query(FRCScoutingContract.ROBOT_CG_LU_Entry.TABLE_NAME, projection,
                    null, null, null, null,
                    FRCScoutingContract.ROBOT_CG_LU_Entry.COLUMN_NAME_ID)) {
                    ret = new ArrayList<>(c.getCount());
                    if (c.moveToFirst()) {
                        do {
                            ret.add(c.getString(c.getColumnIndexOrThrow(FRCScoutingContract.ROBOT_CG_LU_Entry.COLUMN_NAME_CG_TYPE)));
                        } while (c.moveToNext());
                    } else {
                        ret = null;
                    }
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }
                return ret;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static String getCodeFromEventName(String eventName) {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();
                String[] projection = {FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_CODE};
                String[] where = {eventName};
                String ret = "";
                try (Cursor c = db.query(FRCScoutingContract.EVENT_LU_Entry.TABLE_NAME, // from the
                    // event_lu
                    // table
                    projection, // select
                    FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_NAME + " LIKE ?", // where
                    // event_name
                    // ==
                    where, // EventName
                    null, // don't group
                    null, // don't filter
                    null, // don't order
                    "0,1") // limit to 1
                ) {
                    c.moveToFirst();
                    ret = c.getString(c
                        .getColumnIndexOrThrow(FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_CODE));
                } catch (Exception e) {
                    //Do nothing
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }
                return ret;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static List<String> getNotesOptions() {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {FRCScoutingContract.NOTES_OPTIONS_Entry.COLUMN_NAME_OPTION_TEXT};

                List<String> ret;
                try (Cursor c = db.query(FRCScoutingContract.NOTES_OPTIONS_Entry.TABLE_NAME, projection,
                    null, null, null, null,
                    FRCScoutingContract.NOTES_OPTIONS_Entry.COLUMN_NAME_ID)) {

                    ret = new ArrayList<String>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            ret.add(c.getString(c
                                .getColumnIndexOrThrow(FRCScoutingContract.NOTES_OPTIONS_Entry.COLUMN_NAME_OPTION_TEXT)));
                        } while (c.moveToNext());
                    else
                        ret = null;
                }  catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }

                return ret;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static List<String> getNotesForTeam(int team_id) {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {MatchStatsStruct.COLUMN_NAME_NOTES};
                String selection = MatchStatsStruct.COLUMN_NAME_TEAM_ID + "=?";
                String[] selectionArgs = {String.valueOf(team_id)};

                List<String> ret;
                try (Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    selection, selectionArgs, null, null, MatchStatsStruct.COLUMN_NAME_ID)) {
                    ret = new ArrayList<String>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            String[] notes = c.getString(c.getColumnIndexOrThrow(MatchStatsStruct.COLUMN_NAME_NOTES)).split(";");
                            for (String note : notes) {
                                if (!note.isEmpty() && !ret.contains(note))
                                    ret.add(note);
                            }
                        } while (c.moveToNext());
                    else
                        ret = null;
                }  catch (Exception e) {
                    ret = null;
                }finally {
                    ScoutingDBHelper.getInstance().close();
                }

                return ret;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static List<String> getEventsWithData() {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {
                    MatchStatsStruct.COLUMN_NAME_EVENT_ID,
                    "MAX(" + MatchStatsStruct.COLUMN_NAME_TIMESTAMP
                        + ") AS time"};

                List<String> ret;
                try (Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    null, null, MatchStatsStruct.COLUMN_NAME_EVENT_ID,
                    null, "time")) {

                    ret = new ArrayList<String>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            ret.add(getEventNameFromId(
                                c.getInt(c
                                    .getColumnIndexOrThrow(MatchStatsStruct.COLUMN_NAME_EVENT_ID)),
                                db));
                        } while (c.moveToNext());
                    else
                        ret = null;
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }

                return ret;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static List<String> getTeamsWithData() {
        return getTeamsWithData(null);
    }

    public static List<String> getTeamsWithData(String eventName) {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {MatchStatsStruct.COLUMN_NAME_TEAM_ID};

                String selection = null;
                String[] selectionArgs = null;

                if (eventName != null) {
                    selection = MatchStatsStruct.COLUMN_NAME_EVENT_ID + "=?";
                    selectionArgs = new String[1];
                    selectionArgs[0] = String.valueOf(getEventIdFromName(
                        eventName, db));
                }

                Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    selection, selectionArgs,
                    MatchStatsStruct.COLUMN_NAME_TEAM_ID, null,
                    MatchStatsStruct.COLUMN_NAME_TEAM_ID);
                List<Integer> teams;
                try {

                    teams = new ArrayList<Integer>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            teams.add(c.getInt(c
                                .getColumnIndexOrThrow(MatchStatsStruct.COLUMN_NAME_TEAM_ID)));
                        } while (c.moveToNext());

                    projection[0] = PitStats.COLUMN_NAME_TEAM_ID;

                    c.close();

                    if (eventName == null) {

                        c = db.query(PitStats.TABLE_NAME, projection, null,
                            null, null, null, PitStats.COLUMN_NAME_TEAM_ID);
                        if (c.moveToFirst()) {
                            do {
                                int team = c
                                    .getInt(c
                                        .getColumnIndexOrThrow(PitStats.COLUMN_NAME_TEAM_ID));
                                if (!teams.contains(team)) {
                                    teams.add(team);
                                }
                            } while (c.moveToNext());
                        }
                    }

                    if (teams.isEmpty())
                        teams = null;

                } catch (Exception e) {
                    teams = null;
                } finally {
                    c.close();
                    ScoutingDBHelper.getInstance().close();
                }

                if (teams != null) {
                    Collections.sort(teams);
                    List<String> ret = new ArrayList<String>(teams.size());
                    for (Integer team : teams) {
                        ret.add(team.toString());
                    }
                    return ret;
                } else
                    return null;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static List<Long> getEventIdsForTeam(int teamNum) {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();
                String[] projection = {
                    MatchStatsStruct.COLUMN_NAME_EVENT_ID,
                    "MAX(" + MatchStatsStruct.COLUMN_NAME_TIMESTAMP
                        + ") AS time"};

                String selection = MatchStatsStruct.COLUMN_NAME_TEAM_ID + "=?";
                String[] selectionArgs = {String.valueOf(teamNum)};

                List<Long> ret;

                try (Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    selection, selectionArgs,
                    MatchStatsStruct.COLUMN_NAME_EVENT_ID, null, "time")) {
                    ret = new ArrayList<Long>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            ret.add(c.getLong(c.getColumnIndexOrThrow(MatchStatsStruct.COLUMN_NAME_EVENT_ID)));
                        } while (c.moveToNext());
                    else
                        ret = null;
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }
                return ret;

            } catch (Exception e) {
                return null;
            }
        }
    }

    public static List<String> getEventsForTeam(int teamNum) {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();
                String[] projection = {
                    MatchStatsStruct.COLUMN_NAME_EVENT_ID,
                    "MAX(" + MatchStatsStruct.COLUMN_NAME_TIMESTAMP
                        + ") AS time"};

                String selection = MatchStatsStruct.COLUMN_NAME_TEAM_ID + "=?";
                String[] selectionArgs = {String.valueOf(teamNum)};

                List<String> ret;

                try (Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    selection, selectionArgs,
                    MatchStatsStruct.COLUMN_NAME_EVENT_ID, null, "time")) {
                    ret = new ArrayList<String>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            ret.add(getEventNameFromId(
                                c.getInt(c
                                    .getColumnIndexOrThrow(MatchStatsStruct.COLUMN_NAME_EVENT_ID)),
                                db));
                        } while (c.moveToNext());
                    else
                        ret = null;
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }
                return ret;

            } catch (Exception e) {
                return null;
            }
        }
    }

    public static List<String> getMatchesWithData(String eventName, boolean practice,
                                           int teamNum) {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {MatchStatsStruct.COLUMN_NAME_MATCH_ID};

                List<String> args = new ArrayList<String>(3);

                String selection = "";
                String[] selectionArgs = new String[1];

                if (eventName != null) {
                    selection += MatchStatsStruct.COLUMN_NAME_EVENT_ID
                        + "=? AND ";
                    args.add(String.valueOf(getEventIdFromName(eventName, db)));
                }
                if (teamNum > 0) {
                    selection += MatchStatsStruct.COLUMN_NAME_TEAM_ID
                        + "=? AND ";
                    args.add(String.valueOf(teamNum));
                }

                selection += MatchStatsStruct.COLUMN_NAME_PRACTICE_MATCH + "=?";
                args.add(practice ? "1" : "0");
                selectionArgs = args.toArray(selectionArgs);

                List<String> ret;
                try (Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    selection, selectionArgs,
                    MatchStatsStruct.COLUMN_NAME_MATCH_ID, null,
                    MatchStatsStruct.COLUMN_NAME_MATCH_ID)) {

                    ret = new ArrayList<String>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            ret.add(c.getString(c
                                .getColumnIndexOrThrow(MatchStatsStruct.COLUMN_NAME_MATCH_ID)));
                        } while (c.moveToNext());
                    else
                        ret = null;
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }

                return ret;
            } catch (Exception e) {
                return null;
            }
        }
    }

    private static List<Integer> getMatchesWithData(long event_id, boolean practice,
                                             int teamNum) {
        synchronized (ScoutingDBHelper.lock) {
            try {
                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {MatchStatsStruct.COLUMN_NAME_MATCH_ID};

                List<String> args = new ArrayList<String>(3);

                String selection = "";
                String[] selectionArgs = new String[1];

                if (event_id > 0) {
                    selection += MatchStatsStruct.COLUMN_NAME_EVENT_ID
                        + "=? AND ";
                    args.add(String.valueOf(event_id));
                }
                if (teamNum > 0) {
                    selection += MatchStatsStruct.COLUMN_NAME_TEAM_ID
                        + "=? AND ";
                    args.add(String.valueOf(teamNum));
                }

                selection += MatchStatsStruct.COLUMN_NAME_PRACTICE_MATCH + "=?";
                args.add(practice ? "1" : "0");
                selectionArgs = args.toArray(selectionArgs);

                List<Integer> ret;
                try (Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    selection, selectionArgs,
                    MatchStatsStruct.COLUMN_NAME_MATCH_ID, null,
                    MatchStatsStruct.COLUMN_NAME_MATCH_ID)) {

                    ret = new ArrayList<Integer>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            ret.add(c.getInt(c
                                .getColumnIndexOrThrow(MatchStatsStruct.COLUMN_NAME_MATCH_ID)));
                        } while (c.moveToNext());
                    else
                        ret = null;
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }

                return ret;
            } catch (Exception e) {
                return null;
            }
        }
    }


    public static String getPictureURL(int teamNum) {
        synchronized (ScoutingDBHelper.lock) {
            String ret = "";
            try {

                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {FRCScoutingContract.ROBOT_LU_Entry.COLUMN_NAME_ROBOT_PHOTO};
                String[] where = {String.valueOf(teamNum)};
                try (Cursor c = db.query(FRCScoutingContract.ROBOT_LU_Entry.TABLE_NAME, // from the
                    // robot_lu
                    // table
                    projection, // select
                    FRCScoutingContract.ROBOT_LU_Entry.COLUMN_NAME_TEAM_ID + "=?", // where
                    // team_id
                    // ==
                    where, // teamNum
                    null, // don't group
                    null, // don't filter
                    null, // don't order
                    "0,1") // limit to 1
                ) {

                    if (c.moveToFirst()) {
                        ret = c.getString(c
                            .getColumnIndexOrThrow(FRCScoutingContract.ROBOT_LU_Entry.COLUMN_NAME_ROBOT_PHOTO));
                    }

                } catch (Exception e) {
                    //Do nothing
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }

                return ret;
            } catch (Exception e) {
                return null;
            }

        }
    }

    public PitStats getTeamPitStats(int teamNum) {
        //TODO get all pit stats collected for a team
        synchronized (ScoutingDBHelper.lock) {

            try {
                PitStats stats = new PitStats();

                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = PitStats.getProjection();
                String[] where = {String.valueOf(teamNum)};
                try (Cursor c = db.query(PitStats.TABLE_NAME, // from the
                    // scout_pit_data
                    // table
                    projection, // select
                    PitStats.COLUMN_NAME_TEAM_ID + "=?", // where team_id ==
                    where, // teamNum
                    null, // don't group
                    null, // don't filter
                    null, // don't order
                    "0,1")) // limit to 1
                {
                    if (c.getCount() > 0)
                        stats.fromCursor(c, this, db);

                } finally {
                    ScoutingDBHelper.getInstance().close();
                }

                return stats;
            } catch (Exception e) {
                return null;
            }

        }
    }

    public @Nullable SuperScoutStats getSuperScoutStats(String eventName, int match,
                                        int team, boolean practice) {
        synchronized (ScoutingDBHelper.lock) {

            try {
                SuperScoutStats stats = new SuperScoutStats();

                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = SuperScoutStats.getProjection();
                String[] where = {String.valueOf(match),
                    String.valueOf(getEventIdFromName(eventName, db)),
                    String.valueOf(team), practice ? "1" : "0"};

                try (Cursor c = db.query(SuperScoutStats.TABLE_NAME, projection,
                    SuperScoutStats.COLUMN_NAME_MATCH_ID + "=? AND "
                        + SuperScoutStats.COLUMN_NAME_EVENT_ID
                        + "=? AND "
                        + SuperScoutStats.COLUMN_NAME_TEAM_ID
                        + "=? AND "
                        + SuperScoutStats.COLUMN_NAME_PRACTICE_MATCH
                        + "=?", where, null, null, null, "0,1")) {

                    stats.fromCursor(c, this, db);
                }
                ScoutingDBHelper.getInstance().close();

                return stats;

            } catch (Exception e) {
                return null;
            }

        }
    }

    public MatchStatsStruct getMatchStats(String eventName, int match,
                                          int team, boolean practice) {
        synchronized (ScoutingDBHelper.lock) {

            try {
                MatchStatsStruct stats = new MatchStatsStruct();

                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = MatchStatsStruct.getProjection();
                String[] where = {String.valueOf(match),
                    String.valueOf(getEventIdFromName(eventName, db)),
                    String.valueOf(team), practice ? "1" : "0"};

                try (Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    MatchStatsStruct.COLUMN_NAME_MATCH_ID + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_EVENT_ID
                        + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_TEAM_ID
                        + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_PRACTICE_MATCH
                        + "=?", where, null, null, null, "0,1")) {

                    stats.fromCursor(c, this, db);
                }
                ScoutingDBHelper.getInstance().close();

                return stats;

            } catch (Exception e) {
                return null;
            }

        }
    }

    public MatchStatsStruct getMatchStats(long event_id, int match,
                                          int team, boolean practice) {
        synchronized (ScoutingDBHelper.lock) {

            try {
                MatchStatsStruct stats = new MatchStatsStruct();

                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = MatchStatsStruct.getProjection();
                String[] where = {String.valueOf(match),
                    String.valueOf(event_id),
                    String.valueOf(team), practice ? "1" : "0"};

                try (Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    MatchStatsStruct.COLUMN_NAME_MATCH_ID + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_EVENT_ID
                        + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_TEAM_ID
                        + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_PRACTICE_MATCH
                        + "=?", where, null, null, null, "0,1")) {

                    stats.fromCursor(c, this, db);
                }
                ScoutingDBHelper.getInstance().close();

                return stats;

            } catch (Exception e) {
                return null;
            }

        }
    }

    public List<String> getTeamsForMatch(String eventName, int match,
                                         boolean practice) {
        synchronized (ScoutingDBHelper.lock) {

            try {

                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {MatchStatsStruct.COLUMN_NAME_TEAM_ID};
                String[] where = {String.valueOf(match),
                    String.valueOf(getEventIdFromName(eventName, db)),
                    practice ? "1" : "0"};

                List<String> ret;
                try (Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    MatchStatsStruct.COLUMN_NAME_MATCH_ID + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_EVENT_ID
                        + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_PRACTICE_MATCH
                        + "=?", where, null, null, MatchStatsStruct.COLUMN_NAME_POSITION_ID)) {

                    ret = new ArrayList<String>(c.getCount());

                    if (c.moveToFirst())
                        do {
                            ret.add(c.getString(c
                                .getColumnIndexOrThrow(MatchStatsStruct.COLUMN_NAME_TEAM_ID)));
                        } while (c.moveToNext());
                    else
                        ret = null;
                } catch (Exception e) {
                    ret = null;
                } finally
                {
                    ScoutingDBHelper.getInstance().close();
                }

                return ret;

            } catch (Exception e) {
                return null;
            }

        }
    }

    public String getPosition(String eventName, int match,
                              boolean practice, int team) {
        synchronized (ScoutingDBHelper.lock) {

            try {

                SQLiteDatabase db = ScoutingDBHelper.getInstance()
                    .getReadableDatabase();

                String[] projection = {MatchStatsStruct.COLUMN_NAME_POSITION_ID};
                String[] where = {String.valueOf(match),
                    String.valueOf(getEventIdFromName(eventName, db)),
                    practice ? "1" : "0",
                    String.valueOf(team)};

                String ret;
                try (Cursor c = db.query(MatchStatsStruct.TABLE_NAME, projection,
                    MatchStatsStruct.COLUMN_NAME_MATCH_ID + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_EVENT_ID
                        + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_PRACTICE_MATCH
                        + "=? AND "
                        + MatchStatsStruct.COLUMN_NAME_TEAM_ID
                        + "=?", where, null, null, null, "0,1")) {

                    if (c.moveToFirst())
                        ret = getPosNameFromId(c.getInt(c.getColumnIndexOrThrow(MatchStatsStruct.COLUMN_NAME_POSITION_ID)), db);
                    else
                        ret = null;
                } catch (Exception e) {
                    ret = null;
                } finally {
                    ScoutingDBHelper.getInstance().close();
                }

                return ret;

            } catch (Exception e) {
                return null;
            }

        }
    }

    public static long getEventIdFromName(String eventName, SQLiteDatabase db) {

        String[] projection = {FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_ID};
        String[] where = {eventName};

        long ret = -1;
        try (Cursor c = db.query(FRCScoutingContract.EVENT_LU_Entry.TABLE_NAME, // from the event_lu
            // table
            projection, // select
            FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_NAME + " LIKE ?", // where
            // event_name
            // ==
            where, // EventName
            null, // don't group
            null, // don't filter
            null, // don't order
            "0,1")) // limit to 1
        {
            c.moveToFirst();
            ret = c.getLong(c
                .getColumnIndexOrThrow(FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_ID));
        } catch (Exception e) {
            //Do nothing
        }

        return ret;
    }

    public static long getPosIdFromName(String position, SQLiteDatabase db) {

        String[] projection = {FRCScoutingContract.POSITION_LU_Entry.COLUMN_NAME_ID};
        String[] where = {position};

        long ret = -1;
        try (Cursor c = db.query(FRCScoutingContract.POSITION_LU_Entry.TABLE_NAME, // from the event_lu
            // table
            projection, // select
            FRCScoutingContract.POSITION_LU_Entry.COLUMN_NAME_POSITION + " LIKE ?", // where
            // event_name
            // ==
            where, // EventName
            null, // don't group
            null, // don't filter
            null, // don't order
            "0,1")) // limit to 1
        {
            c.moveToFirst();
            ret = c.getLong(c
                .getColumnIndexOrThrow(FRCScoutingContract.POSITION_LU_Entry.COLUMN_NAME_ID));
        } catch (Exception e) {
            //Do nothing
        }

        return ret;
    }

//    public static long getProgrammingIdFromName(String language, SQLiteDatabase db) {
//
//        String[] projection = {FRCScoutingContract.PROGRAMMING_LU_Entry.COLUMN_NAME_ID};
//        String[] where = {language};
//
//        long ret = -1;
//        try (Cursor c = db.query(FRCScoutingContract.PROGRAMMING_LU_Entry.TABLE_NAME,
//            projection, // select
//            FRCScoutingContract.PROGRAMMING_LU_Entry.COLUMN_NAME_LANGUAGE_NAME + " LIKE ?",
//            where, // ID
//            null, // don't group
//            null, // don't filter
//            null, // don't order
//            "0,1")) // limit to 1
//        {
//            c.moveToFirst();
//            ret = c.getLong(c
//                .getColumnIndexOrThrow(FRCScoutingContract.PROGRAMMING_LU_Entry.COLUMN_NAME_ID));
//        } catch (Exception e) {
//            //Do nothing
//        }
//        return ret;
//    }

//    public static String getProgrammingNameFromId(int language, SQLiteDatabase db) {
//
//        String[] projection = {FRCScoutingContract.PROGRAMMING_LU_Entry.COLUMN_NAME_LANGUAGE_NAME};
//        String[] where = {String.valueOf(language)};
//
//        String ret = "";
//        try (Cursor c = db.query(FRCScoutingContract.PROGRAMMING_LU_Entry.TABLE_NAME, projection, // select
//            FRCScoutingContract.PROGRAMMING_LU_Entry.COLUMN_NAME_ID + "= ?", where, // Language
//            null, // don't group
//            null, // don't filter
//            null, // don't order
//            "0,1")) // limit to 1
//        {
//            c.moveToFirst();
//            ret = c.getString(c
//                .getColumnIndexOrThrow(FRCScoutingContract.PROGRAMMING_LU_Entry.COLUMN_NAME_LANGUAGE_NAME));
//        } catch (Exception e) {
//            //Do nothing
//        }
//        return ret;
//    }

    public static long getDriveIdFromName(String drive_name, SQLiteDatabase db) {

        String[] projection = {FRCScoutingContract.DRIVE_LU_Entry.COLUMN_NAME_ID};
        String[] where = {drive_name};

        long ret = -1;
        try (Cursor c = db.query(FRCScoutingContract.DRIVE_LU_Entry.TABLE_NAME, projection,
            FRCScoutingContract.DRIVE_LU_Entry.COLUMN_NAME_DRIVE_NAME + "= ?", where,
            null, null, null, "0,1")) {
            c.moveToFirst();
            ret = c.getLong(c.getColumnIndexOrThrow(FRCScoutingContract.DRIVE_LU_Entry.COLUMN_NAME_ID));
        } catch (Exception e) {
            //Do nothing
        }
        return ret;
    }

    public static String getDriveNameFromId(int drive_id, SQLiteDatabase db) {

        String[] projection = {FRCScoutingContract.DRIVE_LU_Entry.COLUMN_NAME_DRIVE_NAME};
        String[] where = {String.valueOf(drive_id)};

        String ret = "";
        try (Cursor c = db.query(FRCScoutingContract.DRIVE_LU_Entry.TABLE_NAME, projection,
            FRCScoutingContract.DRIVE_LU_Entry.COLUMN_NAME_ID + "= ?", where,
            null, null, null, "0,1")) {
            c.moveToFirst();
            ret = c.getString(c.getColumnIndexOrThrow(FRCScoutingContract.DRIVE_LU_Entry.COLUMN_NAME_DRIVE_NAME));
        } catch (Exception e) {
            //Do nothing
        }
        return ret;
    }

    public static long getCgIdFromName(String cg_name, SQLiteDatabase db) {

        String[] projection = {FRCScoutingContract.ROBOT_CG_LU_Entry.COLUMN_NAME_ID};
        String[] where = {cg_name};

        long ret = -1;
        try (Cursor c = db.query(FRCScoutingContract.ROBOT_CG_LU_Entry.TABLE_NAME, projection,
            FRCScoutingContract.ROBOT_CG_LU_Entry.COLUMN_NAME_CG_TYPE + "= ?", where,
            null, null, null, "0,1")) {
            c.moveToFirst();
            ret = c.getLong(c.getColumnIndexOrThrow(FRCScoutingContract.ROBOT_CG_LU_Entry.COLUMN_NAME_ID));
        } catch (Exception e) {
            //do nothing
        }
        return ret;
    }

    public static String getCgNameFromId(int cg_id, SQLiteDatabase db) {

        String[] projection = {FRCScoutingContract.ROBOT_CG_LU_Entry.COLUMN_NAME_CG_TYPE};
        String[] where = {String.valueOf(cg_id)};

        String ret = "";
        try (Cursor c = db.query(FRCScoutingContract.ROBOT_CG_LU_Entry.TABLE_NAME, projection,
            FRCScoutingContract.ROBOT_CG_LU_Entry.COLUMN_NAME_ID + "= ?", where,
            null, null, null, "0,1")) {
            c.moveToFirst();
            ret = c.getString(c.getColumnIndexOrThrow(FRCScoutingContract.ROBOT_CG_LU_Entry.COLUMN_NAME_CG_TYPE));
        } catch (Exception e) {
            //do nothing
        }
        return ret;
    }

    public static String getEventNameFromId(int eventId, SQLiteDatabase db) {

        String[] projection = {FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_NAME};
        String[] where = {String.valueOf(eventId)};
        String ret = "";

        try (Cursor c = db.query(FRCScoutingContract.EVENT_LU_Entry.TABLE_NAME, projection, // select
            FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_ID + "= ?", where, // EventName
            null, // don't group
            null, // don't filter
            null, // don't order
            "0,1")) // limit to 1
        {
            c.moveToFirst();
            ret = c.getString(c
                .getColumnIndexOrThrow(FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_NAME));
        } catch (Exception e) {
            //Do nothing
        }
        return ret;
    }

    public static String getEventCodeFromId(int eventId, SQLiteDatabase db) {

        String[] projection = {FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_CODE};
        String[] where = {String.valueOf(eventId)};
        String ret = "";

        try (Cursor c = db.query(FRCScoutingContract.EVENT_LU_Entry.TABLE_NAME, projection, // select
            FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_ID + "= ?", where, // EventName
            null, // don't group
            null, // don't filter
            null, // don't order
            "0,1")) // limit to 1
        {
            c.moveToFirst();
            ret = c.getString(c
                .getColumnIndexOrThrow(FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_CODE));
        } catch (Exception e) {
            //Do nothing
        }
        return ret;
    }

    public static String getPosNameFromId(int posId, SQLiteDatabase db) {

        String[] projection = {FRCScoutingContract.POSITION_LU_Entry.COLUMN_NAME_POSITION};
        String[] where = {String.valueOf(posId)};

        String ret = "";
        try (Cursor c = db.query(FRCScoutingContract.POSITION_LU_Entry.TABLE_NAME, projection, // select
            FRCScoutingContract.POSITION_LU_Entry.COLUMN_NAME_ID + "= ?", where, // EventName
            null, // don't group
            null, // don't filter
            null, // don't order
            "0,1")) // limit to 1
        {
            c.moveToFirst();
            ret = c.getString(c
                .getColumnIndexOrThrow(FRCScoutingContract.POSITION_LU_Entry.COLUMN_NAME_POSITION));
        } catch (Exception e) {
            //Do nothing
        }
        return ret;
    }

    public static int getGameInfoInt(String key, SQLiteDatabase db, int defaultValue) {

        String[] projection = {FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_INTVALUE};
        String[] where = {key};

        int ret = defaultValue;
        try (Cursor c = db.query(FRCScoutingContract.GAME_INFO_Entry.TABLE_NAME, projection, // select
            FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_KEYSTRING + "= ?", where, // Key
            null, // don't group
            null, // don't filter
            null, // don't order
            "0,1")) // limit to 1
        {
            c.moveToFirst();
            ret = c.getInt(c
                .getColumnIndexOrThrow(FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_INTVALUE));
        } catch (Exception e) {
            //Do nothing
        }
        return ret;
    }

    public static String getGameInfoKey(int intVal, SQLiteDatabase db, String defaultValue) {

        String[] projection = {FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_KEYSTRING};
        String[] where = {String.valueOf(intVal)};

        String ret = defaultValue;
        try (Cursor c = db.query(FRCScoutingContract.GAME_INFO_Entry.TABLE_NAME, projection,
            FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_INTVALUE + "= ?", where,
            null, null, null, "0,1")) {
            c.moveToFirst();
            ret = c.getString(c.getColumnIndexOrThrow(FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_KEYSTRING));
        } catch (Exception e) {
            //Do nothing
        }
        return ret;
    }

    public static String getGameInfoString(String key, SQLiteDatabase db, String defaultValue) {

        String[] projection = {FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_STRINGVAL};
        String[] where = {key};

        String ret = defaultValue;
        try (Cursor c = db.query(FRCScoutingContract.GAME_INFO_Entry.TABLE_NAME, projection, // select
            FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_KEYSTRING + "= ?", where, // Key
            null, // don't group
            null, // don't filter
            null, // don't order
            "0,1")) // limit to 1
        {
            c.moveToFirst();
            ret = c.getString(c
                .getColumnIndexOrThrow(FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_STRINGVAL));
        } catch (Exception e) {
            //Do nothing
        }
        return ret;
    }

    public static String getGameInfoString(int intVal, SQLiteDatabase db, String defaultValue) {

        String[] projection = {FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_STRINGVAL};
        String[] where = {String.valueOf(intVal)};

        String ret = defaultValue;
        try (Cursor c = db.query(FRCScoutingContract.GAME_INFO_Entry.TABLE_NAME, projection, // select
            FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_INTVALUE + "= ?", where, // Key
            null, // don't group
            null, // don't filter
            null, // don't order
            "0,1")) // limit to 1
        {
            c.moveToFirst();
            ret = c.getString(c
                .getColumnIndexOrThrow(FRCScoutingContract.GAME_INFO_Entry.COLUMN_NAME_STRINGVAL));
        } catch (Exception e) {
            //Do nothing
        }
        return ret;
    }

    static OnHandleFileListener mDirSelectListener = new OnHandleFileListener() {
        @Override
        public void handleFile(String filePath) {
            cb.filename = filePath;
            CSVExporter export = new CSVExporter();
            export.execute(cb);
        }
    };

    static ExportCallback cb;

    private static final String FILENAME = "ScoutingDefaultExportLocation";

    public static void exportToCSV(ScoutingMenuActivity context) {
        try {
            cb = new ExportCallback();
            String filename;
            try {
                BufferedInputStream bis = new BufferedInputStream(
                    context.openFileInput(FILENAME));
                byte[] buffer = new byte[bis.available()];
                bis.read(buffer, 0, buffer.length);
                filename = new String(buffer);
            } catch (Exception e) {
                filename = null;
            }

            cb.context = context;

            new FileSelector(context, FileOperation.SELECTDIR,
                mDirSelectListener, null, filename).show();

        } catch (Exception e) {
            Toast.makeText(context, "Error exporting Database",
                Toast.LENGTH_LONG).show();
        }
    }

    private static class ExportCallback {
        Context context;
        String filename;

        public void finish(String result) {
            Toast.makeText(context, result, Toast.LENGTH_LONG).show();
        }
    }

    private static class CSVExporter extends
        AsyncTask<ExportCallback, Integer, String> {

        ExportCallback callback;
        private static final int notifyId = 87492;

        @Override
        protected String doInBackground(ExportCallback... params) {
            synchronized (ScoutingDBHelper.lock) {
                try {
                    callback = params[0];
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        callback.context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Exporting Scouting Data")
                        .setContentText("to " + callback.filename)
                        .setProgress(300, 0, false); //2017

                    NotificationManager notManager = ((NotificationManager) callback.context
                        .getSystemService(Context.NOTIFICATION_SERVICE));
                    notManager.notify(notifyId, mBuilder.build());

                    SQLiteDatabase db = ScoutingDBHelper.getInstance()
                        .getReadableDatabase();

                    SparseArray<String> cgs = new SparseArray<String>();
                    SparseArray<String> driveTypes = new SparseArray<String>();
                    SparseArray<String> events = new SparseArray<String>();
                    SparseArray<String> positions = new SparseArray<String>();
                    SparseArray<String> defenses = new SparseArray<String>();
                    SparseArray<String> gameInfos = new SparseArray<>();
                    defenses.put(0, "None");

                    StringBuilder match_data = null, pit_data = null, superscout_data = null;
                    // export matches
                    try (Cursor c = db.rawQuery("SELECT * FROM "
                        + MatchStatsStruct.TABLE_NAME, null)) {
                        // decent estimate for how big the output will be. will
                        // definitely be too small, but will keep it from having
                        // to resize too many times
                        match_data = new StringBuilder(c.getCount()
                            * c.getColumnCount() * 2);

                        for (int i = 0; i < c.getColumnCount(); i++) {
                            if (i > 0)
                                match_data.append(",");
                            if (MatchStatsStruct.COLUMN_NAME_INVALID
                                .equalsIgnoreCase(c.getColumnName(i))
                                && !debug)
                                i++; //skip invalid

                            if (MatchStatsStruct.COLUMN_NAME_EVENT_ID
                                .equalsIgnoreCase(c.getColumnName(i)))
                                match_data
                                    .append(FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_CODE);
                            else if (MatchStatsStruct.COLUMN_NAME_POSITION_ID
                                .equalsIgnoreCase(c.getColumnName(i)))
                                match_data
                                    .append(FRCScoutingContract.POSITION_LU_Entry.COLUMN_NAME_POSITION);
                            else
                                match_data.append(c.getColumnName(i));
                        }
                        match_data.append("\n");
                        int rowCount = c.getCount();
                        int progress = 0;
                        if (c.moveToFirst())
                            do {
                                for (int j = 0; j < c.getColumnCount(); j++) {
                                    if (j > 0)
                                        match_data.append(",");
                                    if (MatchStatsStruct.COLUMN_NAME_INVALID
                                        .equalsIgnoreCase(c
                                            .getColumnName(j))
                                        && !debug)
                                        j++; //skip invalid

                                    if (MatchStatsStruct
                                        .isTextField(c.getColumnName(j)))
                                        match_data.append("\"")
                                            .append(c.getString(j).replace("\"", "'"))
                                            .append("\"");
                                    else if (MatchStatsStruct.COLUMN_NAME_EVENT_ID
                                        .equalsIgnoreCase(c
                                            .getColumnName(j))) {
                                        String event = events.get(c.getInt(j));
                                        if (event == null) {
                                            event = getEventCodeFromId(
                                                c.getInt(j), db).replace("\"", "'");
                                            events.append(c.getInt(j), event);
                                        }
                                        match_data.append(event);
                                    } else if (MatchStatsStruct.COLUMN_NAME_POSITION_ID
                                        .equalsIgnoreCase(c
                                            .getColumnName(j))) {
                                        String position = positions.get(c
                                            .getInt(j));
                                        if (position == null) {
                                            position = getPosNameFromId(
                                                c.getInt(j), db).replace("\"", "'");
                                            positions.append(c.getInt(j),
                                                position);
                                        }
                                        match_data.append(position);
                                    } else if (MatchStatsStruct.isGameInfoLookup(c.getColumnName(j))) {
                                        String gameInfo = gameInfos.get(c.getInt(j));
                                        if (gameInfo == null) {
                                            gameInfo = getGameInfoString(c.getInt(j), db, c.getString(j)).replace("\"", "'");
                                            gameInfos.append(c.getInt(j), gameInfo);
                                        }
                                        match_data.append("\"")
                                            .append(gameInfo)
                                            .append("\"");
                                    }
                                    else
                                        match_data.append(c.getString(j));
                                }
                                match_data.append("\n");
                                progress++;
                                mBuilder.setProgress(300,
                                    (int) (((double) progress)
                                        / ((double) rowCount) * 100), //2017
                                    false);
                                notManager.notify(notifyId, mBuilder.build());

                            } while (c.moveToNext());
                    }

                    // export pits
                    try (Cursor c = db.rawQuery("SELECT * FROM " + PitStats.TABLE_NAME,
                        null)){
                        pit_data = new StringBuilder(c.getCount()
                            * c.getColumnCount() * 2);
                        for (int i = 0; i < c.getColumnCount(); i++) {
                            if (i > 0)
                                pit_data.append(",");
                            if (PitStats.COLUMN_NAME_INVALID.equalsIgnoreCase(c
                                .getColumnName(i)) && !debug)
                                i++; //skip invalid

                            if (PitStats.COLUMN_NAME_EVENT_ID
                                .equalsIgnoreCase(c.getColumnName(i)))
                                pit_data
                                    .append(FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_CODE);
                            else if (PitStats.COLUMN_NAME_DRIVE_ID.equalsIgnoreCase(c.getColumnName(i)))
                                pit_data.append(FRCScoutingContract.DRIVE_LU_Entry.COLUMN_NAME_DRIVE_NAME);
                            else if (PitStats.COLUMN_NAME_ROBOT_CG_ID.equalsIgnoreCase(c.getColumnName(i)))
                                pit_data.append(FRCScoutingContract.ROBOT_CG_LU_Entry.COLUMN_NAME_CG_TYPE);
                            /*else if (PitStats.COLUMN_NAME_PROGRAMMING_ID
                                .equalsIgnoreCase(c.getColumnName(i)))
                                pit_data.append("programming_language"); */
                            else
                                pit_data.append(c.getColumnName(i));
                        }
                        pit_data.append("\n");
                        int rowCount = c.getCount();
                        int progress = 0;
                        if (c.moveToFirst()) {
                            do {
                                for (int j = 0; j < c.getColumnCount(); j++) {
                                    if (j > 0)
                                        pit_data.append(",");
                                    if (PitStats.COLUMN_NAME_INVALID
                                        .equalsIgnoreCase(c
                                            .getColumnName(j))
                                        && !debug)
                                        j++; //skip invalid

                                    if (PitStats.isTextField(
                                        c.getColumnName(j)))
                                        pit_data.append("\"")
                                            .append(c.getString(j).replace("\"", "'"))
                                            .append("\"");
                                        // wanted to encapsulate the following, but
                                        // doing so would slow down the export.
                                    /*else if (PitStats.COLUMN_NAME_PROGRAMMING_ID
                                        .equalsIgnoreCase(c
                                            .getColumnName(j))) {
                                        String config = configs
                                            .get(c.getInt(j));
                                        if (config == null) {
                                            config = getProgrammingNameFromID(
                                                c.getInt(j), db);
                                            configs.append(c.getInt(j), config);
                                        }
                                        pit_data.append(config);
                                    } */
                                    else if (PitStats.COLUMN_NAME_DRIVE_ID.equalsIgnoreCase(c.getColumnName(j))) {
                                        String drive_name = driveTypes.get(c.getInt(j));
                                        if (drive_name == null) {
                                            drive_name = getDriveNameFromId(c.getInt(j), db).replace("\"", "'");
                                            driveTypes.append(c.getInt(j), drive_name);
                                        }
                                        pit_data.append("\"")
                                            .append(drive_name)
                                            .append("\"");
                                    }
                                    else if (PitStats.COLUMN_NAME_ROBOT_CG_ID.equalsIgnoreCase(c.getColumnName(j))) {
                                        String cg_type = cgs.get(c.getInt(j));
                                        if (cg_type == null) {
                                            cg_type = getCgNameFromId(c.getInt(j), db).replace("\"", "'");
                                            cgs.append(c.getInt(j), cg_type);
                                        }
                                        pit_data.append("\"")
                                            .append(cg_type)
                                            .append("\"");
                                    } else if (PitStats.isGameInfoLookup(c.getColumnName(j))) {
                                        String gameInfo = gameInfos.get(c.getInt(j));
                                        if (gameInfo == null) {
                                            gameInfo = getGameInfoString(c.getInt(j), db, c.getString(j)).replace("\"", "'");
                                            gameInfos.append(c.getInt(j), gameInfo);
                                        }
                                        pit_data.append("\"")
                                            .append(gameInfo)
                                            .append("\"");
                                    } else
                                        pit_data.append(c.getString(j));
                                }
                                pit_data.append("\n");
                                progress++;
                                mBuilder.setProgress(
                                    300,
                                    (int) (((double) progress)
                                        / ((double) rowCount) * 100) + 100, //2017
                                    false);
                                notManager.notify(notifyId, mBuilder.build());
                            } while (c.moveToNext());
                        }
                    }

                    // export superscout
                    try (Cursor c = db.rawQuery("SELECT * FROM "
                        + SuperScoutStats.TABLE_NAME, null)) {

                        // decent estimate for how big the output will be. will
                        // definitely be too small, but will keep it from having
                        // to resize too many times
                        superscout_data = new StringBuilder(c.getCount()
                            * c.getColumnCount() * 2);

                        for (int i = 0; i < c.getColumnCount(); i++) {
                            if (i > 0)
                                superscout_data.append(",");
                            if (SuperScoutStats.COLUMN_NAME_INVALID
                                .equalsIgnoreCase(c.getColumnName(i))
                                && !debug)
                                i++; //skip invalid

                            if (SuperScoutStats.COLUMN_NAME_EVENT_ID
                                .equalsIgnoreCase(c.getColumnName(i)))
                                superscout_data
                                    .append(FRCScoutingContract.EVENT_LU_Entry.COLUMN_NAME_EVENT_CODE);
                            else if (SuperScoutStats.COLUMN_NAME_POSITION_ID
                                .equalsIgnoreCase(c.getColumnName(i)))
                                superscout_data
                                    .append(FRCScoutingContract.POSITION_LU_Entry.COLUMN_NAME_POSITION);
                            else
                                superscout_data.append(c.getColumnName(i));
                        }
                        superscout_data.append("\n");
                        int rowCount = c.getCount();
                        int progress = 0;
                        if (c.moveToFirst())
                            do {
                                for (int j = 0; j < c.getColumnCount(); j++) {
                                    if (j > 0)
                                        superscout_data.append(",");
                                    if (SuperScoutStats.COLUMN_NAME_INVALID
                                        .equalsIgnoreCase(c
                                            .getColumnName(j))
                                        && !debug)
                                        j++; //skip invalid

                                    if (SuperScoutStats
                                        .isTextField(c.getColumnName(j)))
                                        superscout_data.append("\"")
                                            .append(c.getString(j).replace("\"", "'"))
                                            .append("\"");
                                    else if (SuperScoutStats.COLUMN_NAME_EVENT_ID
                                        .equalsIgnoreCase(c
                                            .getColumnName(j))) {
                                        String event = events.get(c.getInt(j));
                                        if (event == null) {
                                            event = getEventCodeFromId(
                                                c.getInt(j), db);
                                            events.append(c.getInt(j), event);
                                        }
                                        superscout_data.append(event);
                                    } else if (SuperScoutStats.COLUMN_NAME_POSITION_ID
                                        .equalsIgnoreCase(c
                                            .getColumnName(j))) {
                                        String position = positions.get(c
                                            .getInt(j));
                                        if (position == null) {
                                            position = getPosNameFromId(
                                                c.getInt(j), db);
                                            positions.append(c.getInt(j),
                                                position);
                                        }
                                        superscout_data.append(position);
                                    } else if (SuperScoutStats.isGameInfoLookup(c.getColumnName(j))) {
                                        String gameInfo = gameInfos.get(c.getInt(j));
                                        if (gameInfo == null) {
                                            gameInfo = getGameInfoString(c.getInt(j), db, c.getString(j)).replace("\"", "'");
                                            gameInfos.append(c.getInt(j), gameInfo);
                                        }
                                        superscout_data.append("\"")
                                            .append(gameInfo)
                                            .append("\"");
                                    } else
                                        superscout_data.append(c.getString(j));
                                }
                                superscout_data.append("\n");
                                progress++;
                                mBuilder.setProgress(300,
                                    (int) (((double) progress)
                                        / ((double) rowCount) * 100) + 200, //2017
                                    false);
                                notManager.notify(notifyId, mBuilder.build());

                            } while (c.moveToNext());
                    }


                    File sd = new File(callback.filename);
                    String filename_append = Prefs.getDeviceName(callback.context, "");
                    if (!filename_append.isEmpty())
                        filename_append = "_" + filename_append;
                    File match = new File(sd, "matches" + filename_append + ".csv");
                    File pits = new File(sd, "pits" + filename_append + ".csv");
                    File superscout = new File(sd, "superscout" + filename_append + ".csv");
                    FileOutputStream destination;
                    if (match_data != null) {
                        destination = new FileOutputStream(match);
                        destination.write(match_data.toString().getBytes());
                        destination.close();
                    }
                    if (pit_data != null) {
                        destination = new FileOutputStream(pits);
                        destination.write(pit_data.toString().getBytes());
                        destination.close();
                    }
                    if (superscout_data != null) {
                        destination = new FileOutputStream(superscout);
                        destination.write(superscout_data.toString().getBytes());
                        destination.close();
                    }
                    try {
                        FileOutputStream fos = callback.context.openFileOutput(
                            FILENAME, Context.MODE_PRIVATE);
                        fos.write(callback.filename.getBytes());
                        fos.close();
                    } catch (Exception e) {

                    }
                    try {
                        MediaScannerConnection.scanFile(callback.context, new String[]{match.getPath(), pits.getPath(), superscout.getPath()}, null, null);
                    } catch (Exception e) {

                    }
                    mBuilder.setProgress(0, 0, false)
                        .setContentTitle("Export Complete")
                        .setContentText(callback.filename);
                    notManager.notify(notifyId, mBuilder.build());
                    ScoutingDBHelper.getInstance().close();
                    return "DB exported to " + sd.getAbsolutePath();
                } catch (Exception e) {
                    ScoutingDBHelper.getInstance().close();
                    return "Error during export";
                }
            }
        }

        protected void onPostExecute(String result) {
            callback.finish(result);
            callback = null;
        }

    }

    public interface SyncCallback {
        void onFinish();
    }

    public interface DBCallback {
        void onFinish(DBData data);
    }

    public static class DBData {
        protected RequestType _type = RequestType.None;

        protected DBCallback _callback;

        protected int _teamNum = -1; //Matches
        protected String _eventName = null; //Matches

        protected Map<String, SparseArray<MatchStatsStruct>> _matches; //eventname to matchlist

        public DBData(RequestType type, DBCallback callback) {
            _type = type;
            _callback = callback;
        }

        public int getTeamNum() {
            return _teamNum;
        }

        public String getEventName() {
            return _eventName;
        }

        public Map<String, SparseArray<MatchStatsStruct>> getMatches() {
            return _matches;
        }
    }

    public void getMatchesForTeam(int team, String eventName, DBCallback callback) {
        DBData dat = new DBData(RequestType.Matches, callback);
        dat._teamNum = team;
        dat._eventName = eventName;
        (new MatchesAsync()).execute(dat);
    }

    private class MatchesAsync extends AsyncTask<DBData, Integer, DBData> {


        @Override
        protected DBData doInBackground(DBData... params) {
            if (params[0] == null || params[0]._type != RequestType.Matches)
                return null;
            if (params[0]._teamNum > 0) {
                List<Long> eventList;
                if (params[0]._eventName != null) {
                    synchronized (ScoutingDBHelper.lock) {
                        try {
                            SQLiteDatabase db = ScoutingDBHelper.getInstance().getReadableDatabase();
                            eventList = new ArrayList<Long>(1);
                            eventList.add(getEventIdFromName(params[0]._eventName, db));
                        } finally {
                            ScoutingDBHelper.getInstance().close();
                        }
                    }
                } else {
                    eventList = getEventIdsForTeam(params[0]._teamNum);
                }

                if (eventList == null || eventList.isEmpty()) {
                    return null;
                }

                params[0]._matches = new HashMap<String, SparseArray<MatchStatsStruct>>(eventList.size());

                for (Long id : eventList) {
                    SparseArray<MatchStatsStruct> matches = new SparseArray<MatchStatsStruct>();

                    List<Integer> matchList = getMatchesWithData(id, false, params[0]._teamNum);

                    if (matchList == null)
                        continue;
                    for (Integer match : matchList) {
                        matches.append(match, getMatchStats(id, match, params[0]._teamNum, false));
                    }

                    synchronized (ScoutingDBHelper.lock) {
                        try {
                            SQLiteDatabase db = ScoutingDBHelper.getInstance().getReadableDatabase();
                            params[0]._matches.put(getEventNameFromId(id.intValue(), db), matches);
                        } finally {
                            ScoutingDBHelper.getInstance().close();
                        }
                    }
                }
                return params[0];
            }
            return null;
        }

        protected void onPostExecute(DBData data) {
            if (data != null && data._callback != null)
                data._callback.onFinish(data);
        }
    }

}
