FRCScouting
===========

This is an Android app to be used for scouting at competitions. 

See https://play.google.com/store/apps/details?id=org.growingstems.scouting for more info


Utilizes Event Data provided by FIRST: https://frc-events.firstinspires.org/services/API

